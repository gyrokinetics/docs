%==============================================================================
% AstroGK Overview
%==============================================================================
\chapter{General Overview of the Code}
%==============================================================================

%==============================================================================
\section{Equations Advanced by \agk}

The system for driven gyrokinetics is given by the gyrokinetic equation
\begin{equation}
\frac{\partial h_s}{\partial t} 
+v_\parallel \zhat \cdot \frac{\partial h_s}{\partial \V{R}_s} +
\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right] -
\left\langle \left(\frac{\partial h_s}{\partial t} \right)_{\rm
coll}\right\rangle_{\V{R}_s} = \frac{q_s}{T_{0s}} \frac{\partial \langle
\chi \rangle_{\V{R}_s}}{\partial t} F_{0s}
\end{equation}
and Maxwell's equations, which in the gyrokinetic limit become 
the quasineutrality condition
\begin{equation}
-\frac{1}{4 \pi} \nabla_\perp^2 (\phi + \phi_a) + 
\sum_s \frac{q_s^2 n_{0s}}{T_{0s}} \phi = q_s \int d^3 \V{v}
\langle h_s \rangle_\V{r},
\label{eq:quasi_driven}
\end{equation}
the parallel component of Amp\`ere's law,
\begin{equation}
- \frac{c}{ 4 \pi}\nabla_\perp^2 (A_\parallel +A_{\parallel a})
= \sum_s q_s
\int d^3 \V{v}  v_\parallel \langle h_s \rangle_\V{r},
\end{equation}
and the perpendicular component of Amp\`ere's law,
\begin{equation}
 \frac{c}{ 4 \pi}\nabla_\perp (\delta B_\parallel + \delta B_{\parallel a})
= \sum_s q_s
\int d^3 \V{v} \langle(\zhat \times \V{v}_\perp) h_s \rangle_\V{r},
\end{equation}
where $\phi_a$, $A_{\parallel a}$, and $\delta B_{\parallel a}$ are
antenna driving terms.  The gyrokinetic potential is
\begin{equation}
\chi = \phi - \frac{\V{v}\cdot\V{A}}{c}.
\end{equation}
Note that in the undriven case, the first term of
\eqref{eq:quasi_driven} is dropped; I am not certain if it is
consistent to retain it when attempting to drive the gyrokinetic
system electrostatically.  To drive electrostatically, it may be necessary to 
add the driving term to the right-hand side of the gyrokinetic
equation as a source of the form $q_sF_{0s}/T_s \partial \langle \phi_a \rangle_{\V{R}_s}
/\partial t$.

The gyroaveraged gyrokinetic potential can be written, following the
procedure in \chapref{chap:gk_linear}, as
\begin{equation}
\langle \chi \rangle_{\V{R}_s}= \sum_\V{k}\left[ J_0(\alpha_s) \phi_\V{k} -
J_0(\alpha_s) \frac{v_\parallel}{c}A_{\parallel\V{k}} + 
\frac{T_{0s}}{q_s} \frac{2 v_\perp^2}{v_{ts}^2} \frac{J_1(\alpha_s)}{\alpha_s}
\frac{\delta B_{\parallel \V{k}}}{B_0} \right] e^{i \V{k}\cdot\V{R}_s}
\end{equation}
We can each Fourier mode  in the slightly less cumbersome notation 
\begin{equation}
\langle \chi \rangle_{\V{R}_s \V{k}}= \langle \phi \rangle_{\V{R}_s \V{k}} -
 \frac{v_\parallel}{c} \langle  A_{\parallel}  \rangle_{\V{R}_s \V{k}}
+\frac{T_{0s}}{q_s} \frac{2 v_\perp^2}{v_{ts}^2} 
\frac{\langle \delta B_{\parallel}\rangle_{\V{R}_s \V{k}}}{B_0} 
\end{equation}
using the definitions
\begin{equation}
 \langle \phi \rangle_{\V{R}_s \V{k}} \equiv  J_0(\alpha_s) \phi_\V{k}
\end{equation}
\begin{equation}
\langle  A_{\parallel}  \rangle_{\V{R}_s \V{k}} 
=J_0(\alpha_s) A_{\parallel\V{k}} 
\end{equation}
\begin{equation}
\langle \delta B_{\parallel}\rangle_{\V{R}_s \V{k}}=
\frac{J_1(\alpha_s)}{\alpha_s} \delta B_{\parallel \V{k}}
\end{equation}.

Now, we define two terms to simplify notation
\begin{equation}
\langle \phit \rangle \equiv  \langle \phi \rangle_{\V{R}_s \V{k}} +
\frac{T_{0s}}{q_s} \frac{2 v_\perp^2}{v_{ts}^2} 
\frac{\langle \delta B_{\parallel}\rangle_{\V{R}_s \V{k}}}{B_0}
\end{equation}
and
\begin{equation}
\langle \tilde{A} \rangle\equiv  \frac{v_\parallel}{c} \langle  A_{\parallel}  \rangle_{\V{R}_s \V{k}}
\end{equation}
so that we have 
\begin{equation}
\langle \chi \rangle_{\V{R}_s \V{k}}= \langle \phit \rangle- \langle \tilde{A}\rangle
\end{equation}

In \T{AstroGK}, the distribution function used is $g_s$, and is related to the
distribution function in the gyrokinetic equation above by
\begin{equation}
g_{s\V{k}}= h_{s\V{k}} - 
\frac{q_s \langle \phi \rangle_{\V{R}_s \V{k}} }{T_{0s}}F_{0s}
- \frac{2 v_\perp^2}{v_{ts}^2} 
\frac{\langle \delta B_{\parallel}\rangle_{\V{R}_s \V{k}}}{B_0}F_{0s}, 
\end{equation}
or in the more simplified notation
\begin{equation}
g_{s\V{k}}= h_{s\V{k}} - \frac{q_s \langle \phit \rangle}{T_{0s}}F_{0s}.
\end{equation}
Thus, the gyrokinetic equation becomes
\begin{equation}
\frac{\partial g_{s \V{k}}}{\partial t} 
+v_\parallel \frac{\partial g_{s \V{k}} }{\partial z} +
v_\parallel \frac{\partial \langle \phit \rangle  }{\partial z} +
\frac{c}{B_0} \left[ \langle \phit \rangle - \langle \tilde{A} \rangle ,h_s \right] -
\left\langle \left(\frac{\partial h_s}{\partial t} \right)_{\rm coll}
\right\rangle_{\V{R}_s} = - \frac{q_s}{T_{0s}} \frac{\partial \langle \tilde{A} \rangle  }{\partial t} F_{0s}
% TT>
\label{eq: GK eqn in g}
% <TT
\end{equation}


In terms of $g_{s \V{k}}$ , the Maxwell's equations become
\begin{equation}
-\frac{1}{4 \pi} \nabla_\perp^2 (\phi + \phi_a) + 
\sum_s \frac{q_s^2 n_{0s}}{T_{0s}} (\phi + \langle \langle \phi \rangle \rangle_s) = q_s \int d^3 \V{v}
\langle g_{s \V{k}} \rangle_\V{r},
\label{eq:quasi_driven2}
\end{equation}
the parallel component of Amp\`ere's law,
\begin{equation}
- \frac{c}{ 4 \pi}\nabla_\perp^2 (A_\parallel +A_{\parallel a})
= \sum_s q_s
\int d^3 \V{v}  v_\parallel \langle  g_{s \V{k}}\rangle_\V{r},
\end{equation}
and the perpendicular component of Amp\`ere's law,
\begin{equation}
 \frac{c}{ 4 \pi}\nabla_\perp (\delta B_\parallel + \delta B_{\parallel a})
= \sum_s q_s
\int d^3 \V{v} \langle(\zhat \times \V{v}_\perp) h_s \rangle_\V{r},
\end{equation}
where $\phi_a$, $A_{\parallel a}$, and $\delta B_{\parallel a}$ are
antenna driving terms.  The gyrokinetic potential is
\begin{equation}
\chi = \phi - \frac{\V{v}\cdot\V{A}}{c}.
\end{equation}

\subsection{OLD}

Dropping the species subscript $s$ and equilibrium subscripts $0$, using $\C(h)$ to denote the collision
operator, and substituting for $\langle \chi \rangle$, we get
\begin{equation}
\frac{\partial h}{\partial t} 
+v_\parallel  \frac{\partial h}{\partial z} +
\frac{c}{B} \left[ \phit - A ,h \right] -
\langle \C(h) \rangle 
= \frac{q}{T} \frac{\partial (\phit - A)}{\partial t} F 
\end{equation}
Next, we define an alternative version of the gyrokinetic collision
operator,
\begin{equation}
g \equiv h - \frac{q \phit}{T}F
\label{eq:gdef}
\end{equation}
Thus, the relation between $g$ and $h$ is 
\begin{equation}
g \equiv h - \frac{q \langle \phi \rangle}{T}F - \frac{m v_\perp^2}{T} \frac{ 
\langle \delta B_\parallel \rangle}{B_0} F
\end{equation}




Writing the gyrokinetic equation in terms of $g$ gives
\begin{equation}
\frac{\partial g}{\partial t} 
+v_\parallel  \frac{\partial g}{\partial z} 
+\frac{q}{T} v_\parallel F \frac{\partial \phit}{\partial z} 
+\frac{c}{B} \left[ \phit - A ,h \right] -
\langle \C(h) \rangle 
= -\frac{q}{T}F \frac{\partial A}{\partial t} 
\label{eq:gkg}
\end{equation}

%==============================================================================
\section{Variables}

%------------------------------------------------------------------------------
\subsection{General Variables}
The dimensions of the variables in the code are:
\begin{center}
\begin{tabular}{lc}
Space & $x$, $y$, and $z$ \\
Energy & $E=m_s v^2/2$ \\
Pitch Angle & $\lambda= v_\perp^2/(v^2B_0)$ \\
Time & t 
\end{tabular}
\end{center}

The variables in the code are:
\begin{center}
\begin{tabular}{lc}
Distribution Function & $g(x,y,z,E,\lambda,t)$ \\
Scalar Potential & $\phi(x,y,z,t)$ \\
Parallel Vector Potential & $A_\parallel(x,y,z,t)$ \\
Parallel Magnetic Field Perturbation & $\delta B_\parallel(x,y,z,t)$ \\
\end{tabular}
\end{center}

The treatment of each of the dimensions of these variables are:
\begin{center}
\begin{tabular}{cl}
$x$ & Fourier Spectral \\
$y$ & Fourier Spectral \\
$z$ & Compact Finite Difference \\
$E$ & Spectral Integration by quadrature \\
$\lambda$ & Finite difference and spectral integration \\
$t$ & Linear terms are implicit, nonlinear terms are 3rd order Adams-Bashforth
\end{tabular}
\end{center}


%==============================================================================
\newpage
\section{General Flow of the Code}

\begin{enumerate}
\item Initialization
\item Main Timestep Loop (\T{advance\_implicit} in \T{fields\_implicit.f90})
\begin{enumerate}
\item Antenna Amplitudes
\item $\V{E} \times \V{B}$ shear
\item Advance Distribution Function (Initial)
\begin{enumerate}
\item Nonlinear Terms (3rd order Adams-Bashforth)
\item Invert Matrix
\item Collisions
\end{enumerate}
\item Add antenna driving term to fields
\item Advance Fields (Implicit)
\item Advance Distribution Function (Final---same as above)
\item Save for restart if necessary
\item Loop Diagnostics
\begin{enumerate}
\item Heating
\item Density and Velocity fluctuations
\item External Current
\item Movie
\item Field magnitudes by mode
\item Fluxes
\item Polar spectra (Raw and log-averaged)
\item Antenna frequency sweep
\item Nonlinear heat fluxes
\item Linear output
\end{enumerate}
\item Check timestep Courant condition
\item Check for stop
\end{enumerate}
\item Final Diagnostics
\begin{enumerate}
\item Output Distribution Function (Linear)
\item Final Fields
\item $E_\parallel$
\item Final moments
\item Save for restart
\item Finish NetCDF
\item $k_\parallel$-field line following spectra
\end{enumerate}
\item End
\end{enumerate}


% TT>
\section{Main Algorithm}

The code is designed under a basis of the Beam-Warming algorithm which discretizes the finite difference in $z$ and $t$.
Other component of the code is, in some sense, attached to the main frame in a consistent way.
In this section, we will describe the basic flow of the linear algorithm on the basis of Beam-Warming scheme.
This corresponds to a part of the steps 2 (c), (e), (f) in the flow of the code.
Other details are described separately in the following chapters.

In \T{AstroGK} the GK eqn and the coupled field eqns (we call this system coupled GK eqns) are written in the semi-Fourier space $k_x$-$k_y$-$z$ and real space in $E$ and $\lambda$.
Since the linear terms in the coupled GK eqns do not include any differentiation except for $z$ and $t$, the following procedure may be carried out independently for $k_x$, $k_y$, $\lambda$ and $E$, which enables an efficient parallelization.

Here we consider an electrostatic case for simplicity since the electromagnetic case is merely an extension to a largeer matrix and fields.
We discretize the GK eqn at the middle of the $z$ and $t$ grid points, namely at $i+1/2$ for $z$ and $n+1/2$ for $t$.
For example, $\partial g / \partial t$ term yields
\begin{equation}
  \left( \pDif{g}{t} \right)_{i+1/2}^{n+1/2}
  \sim \frac{g_{i+1/2}^{n+1} - g_{i+1/2}^n}{\Delta t}
  \sim \frac{g_{i+1}^{n+1} + g_i^{n+1} - g_{i+1}^n - g_i^n}{2 \Delta t},
\end{equation}
where we have approximated $g_{i+1/2}$ by the arithmetic mean of neighboring grid points.
In the same way, we may write
\begin{equation}
  \left( \pDif{\phi}{z} \right)_{i+1/2}^{n+1/2}
  \sim \frac{\phi_{i+1}^{n+1/2} - \phi_i^{n+1/2}}{\Delta z}
  \sim \frac{\phi_{i+1}^{n+1} + \phi_{i+1}^n - \phi_i^{n+1} - \phi_i^n}
       {2 \Delta z}.
\end{equation}
Thus we may symbolically write the GK eqn (\ref{eq: GK eqn in g}) as
\begin{equation}
  C_1 g_i^n + C_2 g_{i+1}^n + D_1 g_i^{n+1} + D_2 g_{i+1}^{n+1}
    = F_1 \phi_i^n + F_2 \phi_{i+1}^n + G_1 \phi_i^{n+1} + G_2 \phi_{i+1}^{n+1}
    + \mbox{other terms},
  \label{overview: dist adv}%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{equation}
where `other terms' include nonlinear and various source terms.
In fact nonlinear term is treated by an independent scheme (3rd order Adams-Bashforth scheme) and added on the right hand side as one of the source terms as is explained later.
This implicit scheme is described by Beam-Warming and it is a second order scheme both in space and time.
Notice that the scheme is only applied to linear terms and by this we become free from the Courant condition due to the convection along background field direction.

In order for the implicit scheme to work, we have to obtain the field at the future time step.
For this we use a response matrix scheme developed by Kotschenreuther \cite{Kotschenreuther:1995}.
The scheme first starts from splitting the distribution function into homogeneous and inhomogeneous parts:
\begin{equation}
  g^{n+1} = g_{\rm inh}^{n+1} + g_{\rm h}^{n+1},
\end{equation}
where $g_{\rm inh}$ is the solution of GK eqn with setting $\phi^{n+1}=0$ and $g_{\rm h}$ is that with $g^n=\phi^n=0$.
The $g_{\rm inh}^{n+1}$ is readily obtained as soon as all quantities at time step $n$ is known [Step 2(c)].
Here we assumed that the edge value is know from the boundary conditions.
It is subtle for periodic conditions, but we may make two sweeps with specified values at the edge as is explained later.

The next step is to write $g_{\rm h}^{n+1}$ in terms of $\phi^{n+1}$ using the response matrix.
In the initialization, the GK eqn is solved for each $j$ with
\begin{align}
  \phi_i^{n+1} &= \delta_{ij} \quad (i \in N_z), \nonumber \\
  \phi^n &= 0, \quad g^n = 0,
\end{align}
where $\delta_{ij}$ denotes Kornecker's delta and we write the solution $\delta g_i / \delta \phi_j$.
For this step we have to solve GK eqn $N_z \times N_s$ times for each $j \in N_z$ and each species.
Since the general $\phi^{n+1}$ may be written by the sum of these Kronecker's delta and both the response of the distribution function and the quasi-neutrality condition are linear, we may write
\begin{equation}
  g_{{\rm h},i}^{n+1} = \frac{\delta g_i}{\delta \phi_j} \phi_j^{n+1}
\end{equation}
in the symbolic form of the quasi-neutrality condition (\ref{eq:quasi_driven}):
\begin{equation}
  Q_{\phi} \phi^{n+1} = {\cal Q}_d (g^{n+1}).
\end{equation}
This will yield
\begin{equation}
  Q_{\phi} \phi_i^{n+1} - {\cal Q}_d \left( \frac{\delta g_i}{\delta \phi_j}
    \right) \phi_j^{n+1} = {\cal Q}_d(g_{{\rm inh},i}^{n+1}),
\end{equation}
where the left hand side is merely a matrix of size $N_z \times N_z$ (only for one dimension along the background field line) which may be inverted at each time step [Step 2(e)].
If there is a source term in the quasi-neutrality condition, it is added on the right hand side.

Finally, the GK eqn is solved again with proper future fields and we obtain the distribution function at the future time step [Step 2(f)].


%==============================================================================
\section{Velocity space grids}

\newcommand{\negrid}{\gsvar{negrid}}
\newcommand{\nesub}{\gsvar{nesub}}
\newcommand{\nesuper}{\gsvar{nesuper}}
\newcommand{\ecut}{\gsvar{ecut}}

We basically use Legendre polynomials for both energy and pitch angle grid points, so we first review how the Legendre zeros and weights are obtained here, and then describe about the details for each grids in separate subsections.

The zeros and weights of Legendre polynomials are obtained from the routine \gsvar{gauleg} in Numerical Recipes \cite{Press:1992}.
It obtains the zeros by the Newton scheme from the definition of the Legendre polynomial
\begin{align}
  n P_n(x) &= (2n-1) x P_{n-1}(x) - (n-1) P_{n-2}(x) \\
  \Dif{}{x} P_n(x) &= \frac{1}{x^2 - 1} [x P_n(x) - P_{n-1}(x)].
\end{align}
Weights are obtained from the formula
\begin{equation}
  w_i = \frac{2}{ (1-z_i^2) \left[ P_n(z_i)' \right]^2 }
  \qquad ( i = 1, 2, \dots, N ),
\end{equation}
where $z_i$ are the zeros of $P_N(x)$.

%%%%%%%%%%%%%%%    subsection 1    %%%%%%%%%%%%%%%
\subsection{Energy grids}

This is the documentation of the module $\T{egrid}$.

We define a function
\begin{align}
  x &= \frac{2}{\sqrt{\pi}} \int_0^E dE' \, e^{-E'}\sqrt{E'} \nonumber \\
    &= \frac{2}{\sqrt{\pi}} \Gamma \left( \frac{1}{2}, E \right)
    = \frac{2 e^{-E}}{\sqrt{\pi}}
      \sum_{k=0}^{\infty} \left(E^{k+3/2} \prod_{j=0}^k \frac{1}{j+3/2}\right)
\label{E2x}%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{align}
and convert energy integral with Jacobian $\sqrt{E}$ and weight $e^{-E}$ corresponding to Maxwellian into the one for $x$, where $\Gamma$ denotes the incomplete gamma function and the summation is taken $k \leq 100$ in the code.
This function yields a monotonic transform from $E \in [0,\infty)$ to $x \in [0,1)$.

%Unfortunately, it is not practical to use Legendre polynomial for the whole region of $x$ since for a sufficiently large $E$, $x$ becomes extremely close to 1.
%To avoid numerically unresolvable grid points, we artificially put a cutoff at $E = \T{ecut}$ and use Legendre quadrature in the finite domain.
The maximum value of the energy $\ecut$ corresponds to the value $\T{x0}$ in $x$:
\begin{equation}
  \T{x0} = \frac{2}{\sqrt{\pi}} \int_0^{\ecut} dE' \, e^{-E'}\sqrt{E'}.
\end{equation}
Legendre zeros and weights of $(\negrid-1)$-th order polynomial are used in $x \in (0, \T{x0})$.

The last weight representing the integral for higher energy than $\ecut$ is obtained by
\begin{equation}
  w_{\negrid} = 1 - \T{x0}
\end{equation}
as is suggested by Candy and Waltz \cite{Candy:2003}.

Therefore, the energy grids are the $\negrid-1$ zeros of the Legendre polynomial $P_{\T{negrid} - 1}[x(E)]$, $\{ E_i(z_i)| \, i=1, \dots, \negrid-1 \}$, which are obtained from $\T{gauleg}$, plus $\ecut$.
Legendre zeros are concentrated to both edges in $x$, but in most reasonable cases, it is not in $E$-space.
In other words, you should choose $\ecut$ and $\negrid$ in such a way that the grid doesn't concentrate on the edge in $E$.
$\T{gs2}$ gives a warning message if this is not fulfilled.

The advantage of this scheme is that we can take the weights of the energy integral unity by putting the square root in the variable transformation \eqn{E2x}.
Thus, the exponential accuracy of the quadrature is kept.


%%%%%%%%%%%%%%%    subsection 2    %%%%%%%%%%%%%%%
\subsection{Pitch angle grids}

This algorithm appears in the module \T{le\_grids}.

By defining
\begin{equation}
  \check{\xi} = \sqrt{1 - \check{\lambda}},
\end{equation}
the pitch angle integration for the untrapped particle is converted into
\begin{equation}
  \int_0^1 d\check{\lambda} \, \frac{1}{\sqrt{1-\check{\lambda}}}
    = 2 \int_0^1 d\check{\xi}.
\label{lambda integral in xi}%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{equation}

Legendre zeros and weights are used again to evaluate the integral in $\check{\xi}$.
The factor multiplied to the quadrature weights are, thus, 2.
In the same way as before, numerical values of Legendre zeros and weights are obtained by $\T{gauleg}$ in the code.

%%%%%%%%%%%%%%%    subsection 3    %%%%%%%%%%%%%%%
\subsection{Michael's Advanced Velocity Space Grids}
This implementation uses \T{nesub} and \T{nesuper} as variables for
the energy grid (with \T{negrid}$=$\T{nesub}$+$\T{nesuper}), where
\T{nesub} is the number of points up to the maximum velocity 
\T{vcut} (instead of \T{ecut} in the old implementation).  
To use this improved implementation, choose \T{vgrid}=T.

You may choose to only specify \T{negrid}, and the code will choose default values using
\[
\T{nesuper} = (\T{negrid}/16)+1
\]
up to a maximum value of either 3 or 5 (CHECK), 
and \T{nesub}$=$\T{negrid}$-$\T{nesuper}.

%------------------------------------------------------------------------------
\section{Tomo's Variable Description}

\paragraph{scalars}$\mathstrut$\\
\begin{tabular}{lcrl}
  name & type & default value & description\\ \hline \T{nx} & int & 0
  & number of grid points in $x$ real space\\ \T{ny} & int & 0 &
  number of grid points in $y$ real space\\ \T{nakx} & int &
  \T{2*((nx-1)/3)+1} & number of valid modes in $x$\\ \T{naky} & int &
  \T{(ny-1)/3+1} & half number of valid modes in $y$\\ \T{ntheta} &
  int & 24 & number of grid points in $z$\\ \T{ntgrid} & int &
  $\T{ntheta}/2$ & half number of grid points in $z$\\ \T{negrid} &
  int & 16 & total number of energy grid\\ \T{ngauss} & int & 8 & half
  number of $\lambda$ grid points\\ \T{ng2} & int & $\T{ngauss}*2$ &
  \\ \T{nlambda} & int & \T{ng2} & number of grid points in $\lambda =
  \mu/E$\\ \T{x0} & real & 10.0 & box length in $x$ by multiple of $2
  \pi$\\ \T{y0} & real & 10.0 & box length in $y$ by multiple of $2
  \pi$\\ \T{z0} & real & 1.0 & box length in $z$ by multiple of $2
  \pi$\\ \T{ecut} & real & 6.0 & cutoff of energy grid\\
%  \T{lmax} & int & \twovalue{10.5em}{$\T{nlambda}-1$ ($\T{eps} > 0.$)}{$\T{nlambda}$ (otherwise)} & maximum value of $\lambda$?\\
  \T{igomega} & int & 0 & $\T{ig}$ to output in 2d \\
\end{tabular}

\paragraph{1d arrays}$\mathstrut$\\
\begin{tabular}{lcll}
  name & type & dim & description\\ \hline
  \T{akx} & real & \T{nakx} & $\hat{k}_x$ wavenumbers in $x$
    (reversed in the middle)\\
  \T{aky} & real & \T{naky} & $\hat{k}_y$ wavenumbers in $y$\\
  \T{al} & real &\T{nlambda} & pitch-angle grid $\lambda = \mu/E$ \\
  \T{wl} & real & $\T{nlambda}$ & pitch-angle weights
    (${} = \int_0^1 \frac{1}{\sqrt{1-\lambda}} \cdot \, d\lambda$) \\
  \T{vperp2} & real & $\T{glo}$ &
    $\check{\lambda} \check{E} = \vperpc^2$ \\
  \T{aj0} & real & $\T{glo}$ & Bessel function $J_0(a)$ \\
  \T{aj1} & real & $\T{glo}$ & Bessel function $J_1(a)/a$
\end{tabular}\\
where $a = k_{\perp} v_{\perp} / \Omega$.

\paragraph{2d arrays}$\mathstrut$\\
\begin{tabular}{lcll}
  name & type & dim & description\\ \hline
  \T{e} & real & $\T{negrid} \times \T{nspec}$ & energy grid \\
  \T{w} & real & $\T{negrid} \times \T{nspec}$ &
    energy weights (${} = \frac{1}{2\sqrt{\pi}} \int_0^{\T{ecut}}
    \sqrt{E} e^{-E} \cdot \, dE$) \\
  \T{anon} & real & $\T{negrid} \times \T{nspec}$ &
    equals unity unless $\T{slowing\_down\_species}$ \\
  \T{vpa} & real & $2 \times \T{glo}$ & $\vparc$\\
  \T{vpar} & real & $2 \times \T{glo}$ &
    $\frac{Z}{\sqrt{mT}} \frac{\Delta t}{\Delta z} \vparc
      = \frac{Z}{T} \frac{\Delta t}{\Delta z} \vparh$\\
  \T{gamtot} & real & $\T{nakx} \times \T{naky}$ &
    $\sum_s \frac{q_s^2 n_{0s}}{T_{0s}} [ 1 - \Gamma_0(\alpha_s) ]$ \\
  \T{gamtot1} & real & $\T{nakx} \times \T{naky}$ &
    $\sum_s q_s n_{0s} \Gamma_1(\alpha_s)$ \\
  \T{gamtot2} & real & $\T{nakx} \times \T{naky}$ &
    $\frac{1}{2} \sum_s q_s T_{0s} \Gamma_2(\alpha_s)$ \\
\end{tabular}\\
where $ntg = \T{-ntgrid:ntgrid}$.

\paragraph{3d arrays}$\mathstrut$\\
\begin{tabular}{lcll}
  name & type & dim & description\\ \hline
  \T{phi} & complex & $ntg \times \T{nakx} \times \T{naky}$ &
    electrostatic field\\
  \T{apar} & complex & $ntg \times \T{nakx} \times \T{naky}$ &
    $A_{\parallel}$\\
  \T{bpar} & complex & $ntg \times \T{nakx} \times \T{naky}$ &
    $\delta B_{\parallel \Vec{k}}$\\
  \T{g} & complex & $ntg \times 2 \times \T{glo}$ & distribution function\\
\end{tabular}
% <TT

