%==============================================================================
% Connection of Gyrokinetics to Reduced MHD
%==============================================================================
\chapter{Connection of Gyrokinetics to Reduced MHD}

%==============================================================================
%==============================================================================
\section{Summary}
The key points made in this paper are:
\begin{enumerate}
\item When a hyperviscosity term of the form $\nu_H \nabla^4 h_s$
is added to the gyrokinetic equation, the resulting vorticity equation is the 
reduced MHD limit is given by 
\begin{equation}
\frac{\partial }{\partial t} \nabla_\perp^2 
\Phi - v_A \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
+  \left(1+ \frac{T_{0i}}{T_{0e}}\right) \frac{2 }{\rho_i^2}  
\nu_H \nabla_\perp^4  \Phi=0.
\end{equation}
This does not agree with the form we expect for hyperviscosity in
reduced MHD.
\item If, on the other hand, a hyperviscosity term of the form 
$\nu_H \nabla^4 g_s$ is used, the reduced MHD limit gives
\begin{equation}
\frac{\partial }{\partial t} \nabla_\perp^2 
\Phi - v_A \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
+ \nu_H \nabla_\perp^4  \nabla_\perp^2 \Phi=0,
\end{equation}
precisely the form one desires.  Hence, it appears one needs to use $g_s$ in the 
hyperviscous term to correctly model hyperviscous damping.
\item The magnetic flux equation, in the reduced MHD limit, resulting from the
use of a hyperviscosity term of the form $\nu_H \nabla^4 g_s$, is
\begin{equation}
 \frac{\partial \Psi }{\partial t} -v_A \frac{\partial \Phi }{\partial z}
+ \frac{\partial }{\partial t} \left(\frac{ c^2}{\omega_{pe}^2}\nabla_\perp^2 
\Psi \right)
+  \nu_H \nabla^4 \left(\frac{ c^2}{\omega_{pe}^2} \nabla_\perp^2 \Psi \right)= 0.
\end{equation}
Hence, the hyperdiffusive term on $\Psi$ is of order
\begin{equation}
 k_\perp^2 \frac{ c^2}{\omega_{pe}^2} = \frac{k_\perp^2 \rho_i^2}{\beta_i} 
\frac{m_e}{m_i} \ll 1.
\end{equation}
Thus, the hyperviscous term gives only weak hyperresisitivity. 
\item To achieve a magnetic flux equation with hyperresistivity of the
form 
\begin{equation}
 \frac{\partial \Psi }{\partial t} -v_A \frac{\partial \Phi }{\partial z}
+  \eta_H \nabla^4 \Psi = 0,
\end{equation}
one should add the right-hand side of 
the gyrokinetic equation the term
\begin{equation}
-\frac{q_s}{T_{0s}} \eta_H \nabla^4  
\left\langle \frac{v_\parallel A_\parallel}{c} \right\rangle_{\V{R}_s} F_{0s}.
\end{equation}
\item Unfortunately, when the hyperviscous term of the form 
$\nu_H \nabla^4 g_s$ is used, the hyperviscous heating is expressed
in a non-positive definite form.
\item Finally, numerical tests with \T{GS2} show that when  $\nu_H \nabla^4 h_s$
is the hyperviscous term, the effective damping is of order
$(m_i/m_e)\nu_h k_\perp^4$, a mass ratio higher than expected. When
the term is $\nu_H \nabla^4 g_s$, the damping rate is consistent with
predictions.
\end{enumerate}


%==============================================================================
\section{Gyrokinetic Connection to Reduced MHD with Diffusive terms}
Here I will describe how to derive the RMHD equations as a limit
of the Gyrokinetic equations including hyperdiffusive terms.
%-----------------------------------------------------------------------------
\subsection{The Vorticity Equation}
We begin with the gyrokinetic equation including the hyperviscous term
$\nu_H \nabla^4 h_s$
\begin{equation}
\frac{\partial h_s}{\partial t} 
+v_\parallel \frac{\partial h_s}{\partial z} +
\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right] -
\left\langle \left(\frac{\partial h_s}{\partial t} \right)_{\rm
coll}\right\rangle_{\V{R}_s} +  \nu_H \nabla^4 h_s
= \frac{q_s}{T_{0s}} \frac{\partial \langle
\chi \rangle_{\V{R}_s}}{\partial t} F_{0s}.
\end{equation}
To derive the vorticity equation, we want to use the form of the equation as
implemented in the \T{GS2} using $g_s$ where
\begin{equation}
g_s \equiv h_s - \frac{q_s \langle \phi \rangle}{T_s}F_{0s} 
+  \frac{q_s \langle \V{v}_\perp \cdot \V{A}_\perp \rangle}{c T_s}F_{0s} 
\end{equation}
We define two quantities to simplify this calculation (for the Fourier
components of the gyrokinetic potential)
\begin{equation}
\phit \equiv   J_0(\frac{k_\perp v_\perp}{\Omega_s}) \hat{\phi}  + 
\frac{J_1\left(\frac{k_\perp v_\perp}{\Omega_s}\right)} {\frac{k_\perp
v_\perp}{\Omega_s}}
\frac{m_s v^2_\perp}{q_s} \frac{\delta \hat{B}_\parallel}{B_0}
\end{equation}
and
\begin{equation}
A \equiv   J_0(\frac{k_\perp v_\perp}{\Omega_s}) \frac{v_\parallel A_\parallel }{c}
\end{equation}
so that we have 
\begin{equation}
\langle \chi \rangle= \phit - A
\end{equation}

The gyrokinetic equation written as a function of $g$ (except for the collision
operator) then becomes
\begin{equation}
\frac{\partial g_s}{\partial t} 
+v_\parallel  \frac{\partial g_s}{\partial z} 
+\frac{q_s}{T_s} v_\parallel F_{0s} \frac{\partial \phit}{\partial z} 
+\frac{c}{B} \left[ \phit - A ,h_s \right] -
\langle \C(h_s) \rangle +  \nu_H \nabla^4 g_s 
+ \frac{q_s \nu_H }{T_s}F_{0s} \nabla^4 \phit 
= -\frac{q_s}{T_s}F_{0s} \frac{\partial A}{\partial t} 
\label{eq:gk_hyp}
\end{equation}
%-----------------------------------------------------------------------------
\subsubsection{Sum Velocity Integrated Gyrokinetic Equations}

To derive the vorticity equation, the first step is to multiply the
gyrokinetic equation by $q_s$, ring average at constant position
$\V{r}$, integrate the equation over velocity, and sum over species.
We will consider each term in turn.

To simplify the first term, we use the quasineutrality condition in
terms of $g_s$,
\begin{equation}
\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}} \hat{\phi} = \sum_s  q_s \int_\V{v}
\langle h_s \rangle_\V{r} = \sum_s  q_s \int_\V{v}
\langle g_s \rangle_\V{r} + \sum_s \int_\V{v}  
\frac{q_s^2 }{T_s}J_0^2 \hat{\phi} F_{0s} 
+ \sum_s \int_\V{v} \frac{q_s m_s v^2_\perp}{T_s} \frac{J_0
J_1}{\gamma_s}  F_{0s} \frac{\delta \hat{B}_\parallel}{B_0}
\end{equation}
Performing the integration over velocity for the potential terms and
solving for the integral of $g_s$ gives
\begin{equation}
\sum_s  q_s \int_\V{v} \langle g_s \rangle_\V{r} = 
\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}} (1-\Gamma_{0s}) \hat{\phi} 
-  \sum_s q_s  n_{0s}\Gamma_{1s} \frac{\delta \hat{B}_\parallel}{B_0}
\end{equation}
The first term in the gyrokinetic equation then becomes
\begin{equation}
\frac{\partial }{\partial t} \sum_s \int_\V{v}  q_s \langle g_s \rangle_\V{r} =
\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}} (1-\Gamma_{0s}) \frac{\partial \hat{\phi}}{\partial t} 
-  \sum_s \frac{q_s^2 n_{0s}}{T_{0s}} \Gamma_{1s} \frac{\partial }{\partial t} \left( \frac{T_s}{q_s} 
\frac{\delta \hat{B}_\parallel}{B_0} \right)
\end{equation}

For the second term, we will use the Parallel Ampere's Law in terms of 
$g_s$ given by
\begin{equation}
- \frac{c}{ 4 \pi} \nabla_\perp^2 A_\parallel
= \sum_s \int d^3 \V{v}  q_s v_\parallel \langle h_s \rangle_\V{r}
= \sum_s \int d^3 \V{v}  q_s v_\parallel \langle g_s \rangle_\V{r}
+ \sum_s \int d^3 \V{v}\frac{q_s^2 }{T_{0s}}F_{0s}  v_\parallel 
\left\langle \left\langle \phi - \frac{\V{v}_\perp \cdot \V{A}_\perp}{c} 
\right\rangle_{\V{R}_s} \right\rangle_\V{r}
\end{equation}
The last term on the right had side is zero after velocity integration
because it is odd in $v_\parallel$.
Hence, the second term becomes
\begin{equation}
\frac{\partial }{\partial z} \sum_s \int_\V{v}  q_s v_\parallel 
\langle g_s \rangle_\V{r} 
=- \frac{c}{ 4 \pi} \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
\end{equation}

The third term is 
\begin{equation}
\frac{\partial }{\partial z} \sum_s \int_\V{v}  \frac{q_s^2 F_{0s}}{T_{0s}} 
v_\parallel \left\langle \left\langle \phi - \frac{\V{v}_\perp \cdot \V{A}_\perp}{c} 
\right\rangle_{\V{R}_s} \right\rangle_\V{r}=0
\end{equation}
because the entire term is odd in $v_\parallel$.

We will skip the fourth and fifth terms for the moment, since the nonlinearity 
and collisionality are not essential to understanding the manifestation of 
hyperdiffusivity in the reduced MHD equations.

The sixth term can be treated in much the same way as the the first, using 
quasineutrality to simplify the term
The first term in the gyrokinetic equation then becomes
\begin{equation}
\nu_H \nabla^4  \sum_s \int_\V{v}  q_s \langle g_s \rangle_\V{r} =
\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}} (1-\Gamma_{0s})\nu_H \nabla^4   \hat{\phi}
-  \sum_s \frac{q_s^2 n_{0s}}{T_{0s}} \Gamma_{1s} \nu_H \nabla^4  \left( \frac{T_s}{q_s} 
\frac{\delta \hat{B}_\parallel}{B_0} \right)
\end{equation}

The seventh term of the left-hand side just becomes
\begin{equation}
\nu_H \nabla^4  \sum_s \int_\V{v} \frac{q_s^2 }{T_{0s}} F_{0s}
\langle \phit \rangle_\V{r} =
\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}} \Gamma_{0s}\nu_H \nabla^4   \hat{\phi}
+  \sum_s \frac{q_s^2 n_{0s}}{T_{0s}} \Gamma_{1s} \nu_H \nabla^4  \left( \frac{T_s}{q_s} 
\frac{\delta \hat{B}_\parallel}{B_0} \right)
\end{equation}

The term on the right hand side is
\begin{equation}
\frac{\partial }{\partial t} \sum_s \int_\V{v}  \frac{q_s^2}{T_{0s}} F_{0s}
\frac{v_\parallel}{c} \left\langle \left\langle A_\parallel\right\rangle_{\V{R}_s} 
\right\rangle_\V{r}=0
\end{equation}

Putting all of these terms together, we see that the seventh term cancels the 
terms in the sixth term that have $\Gamma_{ns}$, leaving us with
\begin{equation}
\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}}\left[ (1-\Gamma_{0s}) 
\frac{\partial \hat{\phi}}{\partial t} 
-\Gamma_{1s} \frac{\partial }{\partial t} \left( \frac{T_s}{q_s} 
\frac{\delta \hat{B}_\parallel}{B_0} \right)\right]
- \frac{c}{ 4 \pi} \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
+\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}} \nu_H \nabla^4   \hat{\phi}=0
\end{equation}
%-----------------------------------------------------------------------------
\subsubsection{Take $k_\perp \rho_i \ll 1$ Limit}
In the MHD limit $k_\perp \rho_i \ll 1$, we can expand $\Gamma_{0i} \simeq 1 - 
\frac{k_\perp^2 \rho_i^2}{2}$, $\Gamma_{1i} \simeq 1 - 
\frac{3k_\perp^2 \rho_i^2}{4}$, $ \Gamma_{0e} \simeq 1$, and  $\Gamma_{1e} \simeq 1$.
We will also drop the $\delta \hat{B}_\parallel$ terms because we know that in 
the MHD limit there is not parallel magnetic field perturbation due to 
Alfven waves.  This leaves us the result
\begin{equation}
\frac{q_i^2 n_{0i}}{T_{0i}}\frac{k_\perp^2 \rho_i^2}{2} 
\frac{\partial \hat{\phi}}{\partial t} 
- \frac{c}{ 4 \pi} \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
+\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}} \nu_H \nabla^4  \hat{\phi}=0
\end{equation}
Rearranging the equation and multiplying by $c/B_0$ gives
\begin{equation}
\frac{\partial }{\partial t} k_\perp^2 
\frac{c\hat{\phi}}{B_0} - \frac{2 c^2 T_{0i}}{4 \pi B_0 q_i^2 n_{0i} \rho_i^2}
\frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
+ \left(1+ \frac{T_{0i}}{T_{0e}}\right)\frac{2 }{\rho_i^2}  
\nu_H \nabla^4  \frac{c\hat{\phi}}{B_0}=0
\end{equation}
Noting that 
\begin{equation}
\frac{2 c^2 T_{0i}}{4 \pi B_0 q_i^2 n_{0i} \rho_i^2}
= \frac{B_0}{4 \pi n_{0i} m_i}
\end{equation}
and using the definition of the reduced MHD stream and flux functions
\begin{equation}
\Phi = \frac{c\hat{\phi}}{B_0}
\label{eq:stream}
\end{equation}
\begin{equation}
\Psi = \frac{A_\parallel}{\sqrt{4 \pi n_{0i} m_i}}
\label{eq:flux}
\end{equation}
and converting back from Fourier to real space produces
\begin{equation}
\frac{\partial }{\partial t} \nabla_\perp^2 
\Phi - v_A \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
+  \left(1+ \frac{T_{0i}}{T_{0e}}\right) \frac{2 }{\rho_i^2}  
\nu_H \nabla_\perp^4  \Phi=0
\end{equation}
THIS IS WRONG!
The diffusive term is down by two factors of $k_\perp$, so the hyperviscosity 
implemented in this way is simply wrong.
%-----------------------------------------------------------------------------
\subsubsection{Correcting the Problem}
The key is to implement hyperviscosity in the gyrokinetic equation
using a hyperviscous term of the form $\nu_H \nabla^4 g_s$ instead of 
$\nu_H \nabla^4 h_s$.  In this case, the seventh term in equation~(\ref{eq:gk_hyp})
is missing and so does not cancel with parts of the sixth term.  If we 
follow this path, then the result of the ring average  at constant position
$\V{r}$, integrate the equation over velocity, and sum over species gives
\begin{eqnarray}
& & \sum_s  \frac{q_s^2 n_{0s}}{T_{0s}}\left[ (1-\Gamma_{0s}) 
\frac{\partial \hat{\phi}}{\partial t} 
-\Gamma_{1s} \frac{\partial }{\partial t} \left( \frac{T_s}{q_s} 
\frac{\delta \hat{B}_\parallel}{B_0} \right)\right]
- \frac{c}{ 4 \pi} \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel \\
& & +\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}}\left[ (1-\Gamma_{0s})  \nu_H \nabla^4   
\hat{\phi} - \Gamma_{1s}\nu_H \nabla^4 \left( \frac{T_s}{q_s} 
\frac{\delta \hat{B}_\parallel}{B_0} \right)\right] =0
\end{eqnarray}

Now, taking the MHD limit $k_\perp \rho_i \ll 1$ and dropping the
$\delta \hat{B}_\parallel$ terms
gives, after the same manipulations as above
\begin{equation}
\frac{\partial }{\partial t} \nabla_\perp^2 
\Phi - v_A \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
+ \nu_H \nabla_\perp^4  \nabla_\perp^2 \Phi=0
\end{equation}


%-----------------------------------------------------------------------------
\subsection{The Magnetic Flux Equation}
We begin with the gyrokinetic equation including the hyperviscous term
$\nu_H \nabla^4 g_s$
\begin{equation}
\frac{\partial h_s}{\partial t} 
+v_\parallel \frac{\partial h_s}{\partial z} +
\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right] -
\left\langle \left(\frac{\partial h_s}{\partial t} \right)_{\rm
coll}\right\rangle_{\V{R}_s} +  \nu_H \nabla^4 g_s
= \frac{q_s}{T_{0s}} \frac{\partial \langle
\chi \rangle_{\V{R}_s}}{\partial t} F_{0s}.
\end{equation}
We'll drop the nonlinear and collision terms to simplify matters (since 
the hyperdiffusive term is unaffected by the presence of these terms).

%-----------------------------------------------------------------------------
\subsubsection{Sum Velocity Integrated Gyrokinetic Equations}
To derive the magnetic flux equation, the first step is to mulitply the
gyrokinetic equation by $q_s v_\parallel$, ring average at constant position
$\V{r}$, integrate the equation over velocity, and sum over species.
We will consider each term in turn.

For the first term, we use the parallel Ampere's Law to obtain,
\begin{equation}
\frac{\partial }{\partial t} \sum_s \int_\V{v}  q_s v_\parallel 
\langle h_s \rangle_\V{r} =
- \frac{c}{ 4 \pi} \frac{\partial }{\partial t} \nabla_\perp^2 A_\parallel
\end{equation}

For the second term, we will define the pressure as a moment of the full
distribution function given by
\begin{equation}
P_s\equiv  \int_\V{v} \frac{m_s v^2}{2} f_s
\end{equation}
We can similarly define the parallel pressure $P_{\parallel s}$ as the moment using
$ \frac{m_s v_\parallel^2}{2}$.  Expanding the distribution function into
its consituent pieces.
\begin{equation}
f_s = F_{0s} \left( 1 - \frac{q_s \phi}{T_{0s}} \right) + \langle h_s \rangle_\V{r}
\end{equation}
Noting that
\begin{equation}
\int_\V{v} v_\parallel^2 F_{0s} = \frac{n_{0s}T_{0s}}{m_s},
\end{equation}
we find that the parallel pressure is given by
\begin{equation}
P_{\parallel s} = \frac{1}{2} n_{0s} T_{0s} - \frac{1}{2} q_s \phi n_{0s}
+ \delta P_{\parallel s}
\label{eq:pres}
\end{equation}
where we have defined the non-adiabatic part of the parallel pressure by
\begin{equation}
\delta P_{\parallel s} \equiv \int_\V{v} \frac{m_s v_\parallel^2}{2} 
\langle h_s \rangle_\V{r}
\end{equation}
In terms of this definition, the second term becomes
\begin{equation}
\frac{\partial }{\partial z} \sum_s \int_\V{v}  q_s v_\parallel^2
\langle h_s \rangle_\V{r} 
=\frac{\partial }{\partial z}\sum_s\frac{2 q_s\delta P_{\parallel s}}{m_s}
\end{equation}

The third term is the hyperviscous term $\nu_H \nabla^4 g_s$.  First, we note
that when multiplied by $v_\parallel$ and integrated velocity, the difference
between the $\nu_H \nabla^4 g_s$ and $\nu_H \nabla^4 h_s$ goes away.
\begin{equation}
\int_\V{v} v_\parallel \langle g_s \rangle_\V{r} 
= \int_\V{v} v_\parallel \langle h_s \rangle_\V{r}
\end{equation}
So, using parallel Ampere's Law, we find,
\begin{equation}
\nu_H \nabla^4 \sum_s \int_\V{v}  q_s v_\parallel  \langle g_s \rangle_\V{r} 
=- \frac{c}{ 4 \pi} \nu_H \nabla^4\nabla_\perp^2 A_\parallel
\end{equation}

Finally, the term on the right-hand side gives
\begin{equation}
\frac{\partial }{\partial t} \sum_s \int_\V{v}  \frac{q_s^2}{T_{0s}} F_{0s}
v_\parallel \left\langle \left\langle \phi - \frac{v_\parallel A_\parallel}{c}
- \frac{\V{v}_\perp \cdot \V{A}_\perp}{c}\right\rangle_{\V{R}_s} 
\right\rangle_\V{r}
\end{equation}
The first and third terms are odd in $v_\parallel$ and so yield zero upon
integration over velocity. Manipulating what remains gives
\begin{equation}
-\frac{c}{ 4 \pi} \frac{\partial }{\partial t} \sum_s  
\frac{4 \pi q_s^2 n_{0s}}{m_s c^2} \int_\V{v}\frac{m_s F_{0s}}{ n_{0s} T_{0s}} 
v_\parallel^2 \left\langle \left\langle A_\parallel \right\rangle_{\V{R}_s} 
\right\rangle_\V{r} 
=-\frac{c}{ 4 \pi} \frac{\partial }{\partial t} \sum_s  \frac{\omega_{ps}^2}{ c^2}
 \int_\V{v}\frac{m_s F_{0s}}{ n_{0s} T_{0s}} 
v_\parallel^2 \left\langle \left\langle A_\parallel \right\rangle_{\V{R}_s} 
\right\rangle_\V{r} 
\end{equation}
%-----------------------------------------------------------------------------
\subsubsection{Eliminate $\delta P_{\parallel s}$}
To eliminate the parallel pressure term, we use the fact that in the
limit $k_\perp \rho_s \ll 1$, the lowest order solution $h_s^{(0)}$ admits
solutions in which the velocity dependence can be separated from the
space and time dependence,
\begin{equation}
h_s^{(0)} = H_s(\V{R},t) F_{0s}(v).
\end{equation}
This is examined in detail in Alex's paper on gyrokinetics and Reduced MHD.
Substituting this solution into the quasineutrality condition
\begin{equation}
\sum_s q_s \delta n_s = 
\sum_s q_s \left( - \frac{q_s n_{0s}}{T_{0s}} \phi + \int d^3 \V{v}
\langle h_s \rangle_\V{r} \right)= 0
\end{equation}
gives the solution 
\begin{equation}
\sum_s q_s n_{0s} H_s(\V{R},t) = 
\sum_s q_s  n_{0s} \left( \frac{\delta n_s}{  n_{0s}}+ \frac{q_s \phi}{T_{0s}}  
\right)= 0
\end{equation}
We can then find the solution [NOTE: Can we really remove this from
the sum? I am not happy about this step, but perhaps there is an
unstated ordering assumption in Alex's paper allowing him to take this
step.]
\begin{equation}
h_s^{(0)} = \left( \frac{\delta n_s}{  n_{0s}}+ \frac{q_s \phi}{T_{0s}}  
\right) F_{0s}(v)
\end{equation}
which gives solution for the total distribution function 
\begin{equation}
f_s = \left(1+  \frac{\delta n_s}{  n_{0s}}
\right) F_{0s}(v)
\end{equation}
and the solution for the parallel pressure is then
\begin{equation}
P_{\parallel s} = \frac{1}{2} n_{0s} T_{0s} - \frac{1}{2} \delta n_{s}T_{0s} .
\end{equation}
Hence, using equation~(\ref{eq:pres}), we find
\begin{equation}
\delta P_{\parallel s} = \frac{1}{2} n_{0s} T_{0s} 
 \left( \frac{\delta n_s}{  n_{0s}}+ \frac{q_s \phi}{T_{0s}}  \right)
\end{equation}
Now, we can rewrite the second term in terms of $\phi$ and $\delta n_s$ as
\begin{equation}
\frac{\partial }{\partial z}\sum_s\frac{2 q_s\delta P_{\parallel s}}{m_s}=
\frac{c^2}{ 4 \pi} \sum_s  \frac{\omega_{ps}^2}{ c^2} \frac{\partial }{\partial z}
\left( \phi + \frac{T_{0s}}{q_s} \frac{\delta n_s}{  n_{0s}}  \right)
\end{equation}
%-----------------------------------------------------------------------------
\subsubsection{Putting everything together}
Putting all of these terms together and multiplying by $\frac{ 4 \pi}{c}$gives
\begin{equation}
- \frac{\partial }{\partial t} \nabla_\perp^2 A_\parallel
+c \sum_s  \frac{\omega_{ps}^2}{ c^2} \frac{\partial }{\partial z}
\left( \phi + \frac{T_{0s}}{q_s} \frac{\delta n_s}{  n_{0s}}  \right)
-  \nu_H \nabla^4\nabla_\perp^2 A_\parallel = 
- \frac{\partial }{\partial t} \sum_s  \frac{\omega_{ps}^2}{ c^2}
 \int_\V{v}\frac{m_s F_{0s}}{ n_{0s} T_{0s}} 
v_\parallel^2 \left\langle \left\langle A_\parallel \right\rangle_{\V{R}_s} 
\right\rangle_\V{r} 
\end{equation}
%-----------------------------------------------------------------------------
\subsubsection{Take $k_\perp \rho_i \ll 1$ Limit}
We know that by Fourier analyzing in space will give us the result
\begin{equation}
\int_\V{R}\int_\V{v} \left\langle \left\langle A_\parallel \right\rangle_{\V{R}_s} 
\right\rangle_\V{r} = \int_\V{v} \sum_\V{k} \Gamma_{0s} \hat{A}_\parallel,
\end{equation}
so we will put in the $\Gamma_{0s}$ (with a sign change for
the Fourier transform) to account for the double ring
average even though we are being a bit sloppy with notation.  In the
limit $k_\perp \rho_i \ll 1$, $\Gamma_{0s} \simeq 1$, and we can take
$\sum_s \omega_{ps}^2= \omega_{pe}^2 \left(1+ \frac{m_e}{m_i} \right)=
\omega_{pe}^2 $.  We also neglect the density perturbation $\delta n_s$ in the 
MHD limit since Alfven waves are incompressible.
Hence, we find
\begin{equation}
 \frac{\partial A_\parallel }{\partial t}
-c \frac{\partial \phi }{\partial z}
+ \frac{\partial }{\partial t} \frac{ c^2}{\omega_{pe}^2}\nabla_\perp^2 A_\parallel 
+  \nu_H \nabla^4 \frac{ c^2}{\omega_{pe}^2} \nabla_\perp^2 A_\parallel = 0
\end{equation}
Multiplying by $(4 \pi n_{0i} m_i)^{-1/2}$ and using the definitions of the stream 
and flux functions equations~(\ref{eq:stream})--(\ref{eq:flux}), we find
\begin{equation}
 \frac{\partial \Psi }{\partial t} -v_A \frac{\partial \Phi }{\partial z}
+ \frac{\partial }{\partial t} \left(\frac{ c^2}{\omega_{pe}^2}\nabla_\perp^2 
\Psi \right)
+  \nu_H \nabla^4 \left(\frac{ c^2}{\omega_{pe}^2} \nabla_\perp^2 \Psi \right)= 0
\end{equation}

If we order the damping rate due to hyperdiffusivity $\Order(\nu_H k_\perp^4)
= \Order(\omega)$, as is appropriate at the of the cascade where we want the 
damping to be strong, then the hyperdiffusive term, compared to the 
$\frac{\partial \Psi }{\partial t}$ term, is of order
\begin{equation}
 k_\perp^2 \frac{ c^2}{\omega_{pe}^2} = \frac{k_\perp^2 \rho_i^2}{\beta_i} 
\frac{m_e}{m_i} \ll 1
\end{equation}
Therefore, except for at very low $\beta_i \sim
\Order(\frac{m_e}{m_i})$, the diffusive effect (hyperresistivity) of 
the hyperviscosity term on $ A_\parallel$ from the hyperviscosity term
is negligible.
%-----------------------------------------------------------------------------
\subsubsection{Discussion of Hyperresistivity}
To achieve an effective hyperresistivity, we would like a term that
produces a Reduced MHD flux equation of the form
\begin{equation}
 \frac{\partial \Psi }{\partial t} -v_A \frac{\partial \Phi }{\partial z}
+  \eta_H \nabla^4 \Psi = 0
\end{equation}

To achieve such an effect in the magnetic flux equation, we can put in a 
term on the right-hand side of the form 
\begin{equation}
\frac{q_s}{T_{0s}} \eta_H \nabla^4  \langle \chi \rangle_{\V{R}_s} F_{0s}
\end{equation}
In this case, only the $A_\parallel$ part of the gyrokinetic potential
will affect the final form of the magnetic flux equation because the
$\phi$ and $\delta B_\parallel$ terms are odd when multiplied by
$v_\parallel$. Whether to include these terms or not, it is necessary
to view how this addition affects the vorticity equation. With only
the $A_\parallel$ term, the vorticity equation is unchanged because
the extra term goes away when integrated over velocity. It seems most
likely that you want only to add the terms necessary to achieve this 
hyperresistivity, so the desired term to add to the right-hand side of 
the gyrokinetic equation would be
\begin{equation}
-\frac{q_s}{T_{0s}} \eta_H \nabla^4  
\left\langle \frac{v_\parallel A_\parallel}{c} \right\rangle_{\V{R}_s} F_{0s}
\end{equation}


One could also add a term to the left-hand side, much like the hyperviscosity,
of the form
\begin{equation}
\frac{\omega_{pe}^2}{ c^2} \eta_H \nabla_\perp^2 g_s.
\end{equation}
This will also produce the desired term in the magnetic flux equation,
but it will also have an effect of normal viscosity, with a huge
coefficient out front because of the $\frac{\omega_{pe}^2}{ c^2}$
factor.  Hence, it will likely create a large Laplacian viscosity, and
undesired effect.

%==============================================================================
\newpage
\section{Corrected Hyperviscous Heating}
In this section we will derive the form of the hyperviscous heating for the 
case when the hyperviscous term is $\nu_H \nabla^4 g_s$ instead of 
$\nu_H \nabla^4 h_s$. 
%-----------------------------------------------------------------------------
\subsection{Entropy Equation with Hyperviscosity}
We begin with the gyrokinetic equation including ther hyperviscous term
\begin{equation}
\frac{\partial h_s}{\partial t} 
+v_\parallel \frac{\partial h_s}{\partial z} +
\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right] -
\left\langle \left(\frac{\partial h_s}{\partial t} \right)_{\rm
coll}\right\rangle_{\V{R}_s} +  \nu_H \nabla^4 g_s
= \frac{q_s}{T_{0s}} \frac{\partial \langle
\chi \rangle_{\V{R}_s}}{\partial t} F_{0s}.
\end{equation}
We multiply the gyrokinetic equation by $T_{0s}h_s/F_{0s}$ and
integrate over space and velocity to obtain  the 
{\bf entropy equation}
\begin{eqnarray}
& &  \int\frac{d^{3}{\bf R}}{V}\int d^{3}{\bf v} 
q_s \frac{\partial \left <\chi\right >}{\partial t} h_s - 
\frac{d}{dt}\int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{0s}}{2F_{0s}}h^2_{s} 
+  \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{0s}}{F_{0s}}\left\langle h_sC(h_s) \right\rangle_{\V{R}_s}  \nonumber \\ 
& &- \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v}
\frac{T_{0s}}{F_{0s}}\nu_H  h_s \nabla_\perp^4 g_s
= 0  
\end{eqnarray}
where we have applied the gyrokinetic approximation $k_\| \ll k_\perp$ to the 
hyperviscous operator $\nabla^4 \rightarrow
\nabla_\perp^4$. 
We need to manipulate the last term into a suitable form for calculating the 
energy.

%-----------------------------------------------------------------------------
\subsubsection{Relation between  $g_s$ and $h_s$}
First, note that the definition of $g_s$ in terms of $h_s$ is
\begin{equation}
g_s \equiv h_s - \frac{q_s \langle \phi \rangle_{\V{R}_s}}{T_{0s}}F_{0s}
+ \frac{q_s \langle \V{v}_\perp \cdot \V{A}_\perp \rangle_{\V{R}_s}}{T_{0s}c}F_{0s} 
\end{equation}
When Fourier decomposed, note that we can write each of these pieces as
\begin{equation}
g_s = \sum_\V{k} \hat{g}_{s \V{k}} e^{i \V{k} \cdot \V{R}_s}
\end{equation}
\begin{equation}
h_s = \sum_\V{k} \hat{h}_{s \V{k}} e^{i \V{k} \cdot \V{R}_s}
\label{eq:hsk}
\end{equation}
\begin{equation}
 \langle \phi \rangle_{\V{R}_s} 
= \sum_\V{k} J_0\left( \frac{k_\perp v_\perp}{\Omega_s} \right)
\hat{\phi}_{\V{k}} e^{i \V{k} \cdot \V{R}_s}
\label{eq:phik}
\end{equation}
\begin{equation}
 \left\langle \frac{\V{v}_\perp \cdot \V{A}_\perp}{c}\right\rangle_{\V{R}_s} 
= -\sum_\V{k} \frac{ J_1\left( \frac{k_\perp v_\perp}{\Omega_s} \right)}
{\frac{k_\perp v_\perp}{\Omega_s}} \frac{m_s v_\perp^2}{q_s} 
\frac{\delta \hat{B}_{\parallel \V{k}}}{B_0} e^{i \V{k} \cdot \V{R}_s}
\end{equation}
Therefore, the Fourier components are related by
\begin{equation}
\hat{g}_{s \V{k}}\equiv \hat{h}_{s \V{k}} 
- \frac{q_s  J_{0s} \hat{\phi}_{\V{k}}}{T_{0s}} F_{0s}
+ \frac{ J_{1s}}{\gamma_s}
\frac{m_s v_\perp^2}{T_{0s}} \frac{\delta \hat{B}_{\parallel \V{k}}}{B_0}F_{0s}
\end{equation}
where we have used the notation $\gamma_s = \frac{k_\perp
v_\perp}{\Omega_s} $ and $J_{ns} = J_n\left( \gamma_s\right)$.

%-----------------------------------------------------------------------------
\subsubsection{Manipulating the Hyperviscous Heating Term}
Noting here that 
\begin{equation}
\nabla_\perp^4 
=\frac{\partial^4}{\partial x^4}+2\frac{\partial^4}{\partial x^2 \partial y^2} 
+\frac{\partial^4}{\partial y^4},
\end{equation}
we can perform an integration by parts in space twice on each term to obtain
\begin{eqnarray}
\lefteqn{\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v}
\frac{T_{0s}\nu_H}{F_{0s}}  h_s \nabla_\perp^4 g_s} && \\
&=& \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v}
\frac{T_{0s}\nu_H}{F_{0s}}  h_s \nabla_\perp^4 \left( h_s 
- \frac{q_s \langle \phi \rangle_{\V{R}_s}}{T_{0s}}F_{0s}
+\frac{q_s \langle \V{v}_\perp \cdot \V{A}_\perp \rangle_{\V{R}_s}}{T_{0s}c}F_{0s}
\right)  \nonumber \\
& = &   \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} \frac{T_{0s}\nu_H}{F_{0s}} 
 \left[ \left(\frac{\partial^2 h_s}{\partial^2 x} \right)^2 +
2\left(\frac{\partial^2 h_s}{\partial x \partial y} \right)^2 +
\left(\frac{\partial^2 h_s}{\partial^2 y}\right)^2 \right]  \nonumber\\
&-&  \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} q_s\nu_H
 \left[ \frac{\partial^2 \langle \phi \rangle}{\partial^2 x} 
\frac{\partial^2 h_s}{\partial^2 x} +
2\frac{\partial^2\langle \phi \rangle}{\partial x \partial y} 
\frac{\partial^2 h_s}{\partial x \partial y}
+ \frac{\partial^2 \langle \phi \rangle}{\partial^2 y} 
\frac{\partial^2 h_s}{\partial^2 y} \right]  \nonumber\\
&+ &  \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} \frac{q_s\nu_H}{c} 
 \left[ \frac{\partial^2 \langle \V{v}_\perp \cdot \V{A}_\perp \rangle}{\partial^2 x} 
\frac{\partial^2 h_s}{\partial^2 x} +
2\frac{\partial^2\langle \V{v}_\perp \cdot \V{A}_\perp \rangle}{\partial x \partial y} 
\frac{\partial^2 h_s}{\partial x \partial y}
+ \frac{\partial^2 \langle \V{v}_\perp \cdot \V{A}_\perp \rangle}{\partial^2 y} 
\frac{\partial^2 h_s}{\partial^2 y} \right] \nonumber 
\end{eqnarray}
%-----------------------------------------------------------------------------
\subsubsection{Fourier Decomposition of Heating Term}
Here we will show how this heating term is expressed in the Fourier decomposition.
Using the Fourier series expansions of $h_s^*$ and $\phi$, as given by
equations~(\ref{eq:hsk})--(\ref{eq:phik}), for one of the terms we find
\begin{eqnarray}
&& \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} q_s\nu_H 
\frac{\partial^2 \langle \phi \rangle}{\partial^2 x}
\frac{\partial^2 h_s^*}{\partial^2 x} 
= \int d^{3}{\bf v}\int\frac{d^{3}{\bf R}_s}{V} q_s\nu_H 
 \sum_{\V{k}}\V{k}^2_x J_{0s}\hat{\phi}_{\V{k}} e^{i \V{k} \cdot \V{R}_s}
 \sum_{\V{k}'}\V{k}'^2_x \hat{h}^*_{s \V{k}'} e^{-i \V{k}' \cdot \V{R}_s} \nonumber \\
&=&\int d^{3}{\bf v} \sum_{\V{k}}\sum_{\V{k}'}q_s\nu_H 
\V{k}^2_x \V{k}'^2_x J_{0s}\hat{\phi}_{\V{k}} \hat{h}^*_{s \V{k}'}
\int\frac{d^{3}{\bf R}_s}{V} e^{i (\V{k}-\V{k}') \cdot \V{R}_s}
\end{eqnarray}
Noting that 
\begin{equation}
\int d^{3}{\bf r} e^{i (\V{k}-\V{k}') \cdot \V{r}} = \delta(\V{k}-\V{k}'),
\end{equation}
we can use the delta function to eliminate one of the sums to find
\begin{equation}
\frac{1}{V}\sum_{\V{k}}\int d^{3}{\bf v} q_s\nu_H 
\V{k}^4_x  J_{0s}\hat{\phi}_{\V{k}} \hat{h}^*_{s \V{k}}.
\end{equation}

Putting all of the terms together gives
\begin{eqnarray}
&&\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v}
\frac{T_{0s}\nu_H}{F_{0s}}  h_s \nabla_\perp^4 g_s \\
&=& 
\sum_{\V{k}}\int d^{3}{\bf v}\frac{T_{0s}}{F_{0s}}
\nu_H  k_\perp^4  \left[ |h_\V{k}|^2 
- J_{0s} \frac{ q_s\hat{\phi}_{\V{k}}}{T_{0s}} \hat{h}^*_{s \V{k}} F_{0s}
-\frac{ J_{1s}}{\gamma_s}
\frac{m_s v_\perp^2}{T_{0s}} \frac{\delta \hat{B}_{\parallel \V{k}}}{B_0}
\hat{h}^*_{s \V{k}}  F_{0s} \right] \nonumber
\end{eqnarray}
%-----------------------------------------------------------------------------
\subsubsection{Normalization}
We multiply each term of the equation by $\frac{a_0}{v_{t0}} \left(
\frac{a_0}{\rho_0} \right)^2 \frac{1}{v_{t0}^3 F_{00} T_0}$.
The first term gives
\begin{eqnarray}
\lefteqn{ \left[\frac{a_0}{v_{t0}} \left(\frac{a_0}{\rho_0} \right)^2 
\frac{1}{v_{t0}^3 F_{00} T_0} \right]
\sum_{\V{k}}\int d^{3}{\bf v}\frac{T_{s}}{F_{0s}}
\nu_H  k_\perp^4  h_{s\V{k}}^2 } & &   \nonumber \\
&=&\sum_{\V{k}}\int \frac{d^3\V{v}}{v_{ts}^3} 
\left( \frac{v_{ts}}{v_{t0}} \right)^3
\frac{F_{0s}}{F_{00}} \frac{T_{s}}{T_0} 
\left(\frac{\nu_H a_0}{\rho_0^4 v_{t0}} \right) (k_\perp^4 \rho_0^4)
\left(\frac{h_{s\V{k}}}{F_{0s}}\frac{a_0}{\rho_0}\right)^2 \nonumber \\
&= &\sum_{\V{k}}\int d^3\hat{\V{v}}_s \frac{ e^{-\hat{v}_s^2}}{\pi ^{3/2}} 
\hat{n}_s \hat{T}_{s} \hat{\nu}_H \hat{k}_\perp^4 \hat{h}_{s\V{k}}^2.
\end{eqnarray}
The second term yields
\begin{eqnarray}
\lefteqn{- \left[\frac{a_0}{v_{t0}} \left(\frac{a_0}{\rho_0} \right)^2 
\frac{1}{v_{t0}^3 F_{00} T_0} \right]
\sum_{\V{k}}\int d^{3}{\bf v}\frac{T_{s}}{F_{0s}}
\nu_H  k_\perp^4 J_{0s} \frac{ q_s\hat{\phi}_{\V{k}}}{T_{s}} 
\hat{h}_{s \V{k}} F_{0s} } & &   \nonumber \\
&=&-\sum_{\V{k}}\int \frac{d^3\V{v}}{v_{ts}^3} 
\left( \frac{v_{ts}}{v_{t0}} \right)^3
\frac{F_{0s}}{F_{00}} \frac{q_{s}}{q_0} 
\left(\frac{\nu_H a_0}{\rho_0^4 v_{t0}} \right) (k_\perp^4 \rho_0^4)
\left(\frac{ q_0\hat{\phi}_{\V{k}}}{T_{0}} \frac{a_0}{\rho_0}\right)
\left(\frac{h_{s\V{k}}}{F_{0s}}\frac{a_0}{\rho_0}\right) \nonumber \\
&= &-\sum_{\V{k}}\int d^3\hat{\V{v}}_s \frac{ e^{-\hat{v}_s^2}}{\pi ^{3/2}} 
\hat{n}_s \hat{q}_{s} \hat{\nu}_H \hat{k}_\perp^4 \hat{J}_{0s} \hat{\phi}_{\V{k}}\hat{h}_{s\V{k}}.
\end{eqnarray}
The third term produces
\begin{eqnarray}
\lefteqn{- \left[\frac{a_0}{v_{t0}} \left(\frac{a_0}{\rho_0} \right)^2 
\frac{1}{v_{t0}^3 F_{00} T_0} \right]
\sum_{\V{k}}\int d^{3}{\bf v}\frac{T_{s}}{F_{0s}}
\nu_H  k_\perp^4 \frac{ J_{1s}}{\gamma_s}
\frac{m_s v_\perp^2}{T_{s}} \frac{\delta \hat{B}_{\parallel \V{k}}}{B_0}
\hat{h}_{s \V{k}}  F_{0s}} & &   \nonumber \\
&=&-\sum_{\V{k}}\int \frac{d^3\V{v}}{v_{ts}^3} 
\left( \frac{v_{ts}}{v_{t0}} \right)^3
\frac{F_{0s}}{F_{00}} 
\left(\frac{\nu_H a_0}{\rho_0^4 v_{t0}} \right) (k_\perp^4 \rho_0^4)
\frac{2 T_{s}}{T_0} \frac{m_s v_\perp^2}{2 T_{s}} \frac{J_{1s}}{\gamma_s}
\left(\frac{ \delta \hat{B}_{\parallel \V{k}}}{B_{0}} \frac{a_0}{\rho_0}\right)
\left(\frac{h_{s\V{k}}}{F_{0s}}\frac{a_0}{\rho_0}\right) \nonumber \\
&= &-\sum_{\V{k}}\int d^3\hat{\V{v}}_s \frac{ e^{-\hat{v}_s^2}}{\pi ^{3/2}} 
\hat{n}_s \hat{T}_{s} \hat{\nu}_H \hat{k}_\perp^4 \hat{J}_{1s} 2\hat{v}_{\perp s}^2
\delta \hat{B}_{\parallel \V{k}}\hat{h}_{s\V{k}}.
\end{eqnarray}

Pulling everything together we find
\begin{equation}
\sum_{\V{k}}\int d^3\hat{\V{v}}_s \frac{ e^{-\hat{v}_s^2}}{\pi ^{3/2}} 
\hat{\nu}_H \hat{k}_\perp^4 \hat{n}_s \hat{T}_{s} 
\left(  \hat{h}_{s\V{k}}^2
- \hat{J}_{0s} \frac{\hat{q}_{s}\hat{\phi}_{\V{k}}}{\hat{T}_{s}} \hat{h}_{s\V{k}}
-\hat{J}_{1s} 2 \hat{v}_{\perp s}^2
\delta \hat{B}_{\parallel \V{k}}\hat{h}_{s\V{k}} \right)
\end{equation}



%----------------------------------------------------------------------------
\newpage
\subsection{Numerical Tests}
Let us consider a generalized equation for the energy evolution
\begin{equation}
\frac{d E}{dt} = -2\gamma E - \nu_h k_\perp^4 E + P_a
\end{equation}
where $\gamma$ is the natural collisionless damping rate, $\nu_h
k_\perp^4$ is the effective hyperviscous damping rate, and $P_a$ is
the antenna power driving the simulation.

When driven at a constant frequency and amplitude, the system
eventually reaches a steady state in which $\frac{d E}{dt} =0$.  In
this case, the amplitude of the energy in steady-state is given by
\begin{equation}
 E =  \frac{P_a}{2\gamma + \nu_h k_\perp^4}
\end{equation}
 The total heating due to collisionless damping is then given by
\begin{equation}
P_\gamma= 2\gamma E =  \frac{2\gamma P_a}{2\gamma + \nu_h k_\perp^4}
\end{equation}
and that due to hyperviscous damping is 
\begin{equation}
P_{\nu_h}= 2\gamma E =  \frac{\nu_h k_\perp^4 P_a}{2\gamma + \nu_h k_\perp^4}
\end{equation}
Taking the ratio of the hyperviscous to the total heating, the unknown antenna power
drops out, leaving the theoretical value
\begin{equation}
\frac{P_{\nu_h}}{P_{tot}}= \frac{\nu_h k_\perp^4}{2\gamma + \nu_h k_\perp^4}
\label{eq:ptheory}
\end{equation}

Figure~\ref{fig:hyp12_14} plots $P_e/(P_i+P_e)$, roughly the ratio
$\frac{P_{\nu_h}}{P_{tot}}$ (since electron heating is primarily
hyperviscous and ion heating is primarily due to collisionless
damping), against the theoretical relation given by
equation~(\ref{eq:ptheory}) above.
\begin{figure}
\hbox to \hsize{ \hfill \epsfxsize16cm \epsffile{figs/hyp12_14.ps} \hfill}
\caption{Plot of $P_e/(P_i+P_e)$ vs. the theoretical relation 
given by equation~(\ref{eq:ptheory}).  For a hyperviscosity term of
the form $\nu_h k_\perp^4 h_s$ (squares), the hyperviscous heating
rate must be increased by the mass ratio $m_i/m_e$ matches the
numerical results. For a hyperviscosity term of the form $\nu_h
k_\perp^4 g_s$ (triangles), the results agree much more closely with
equation~(\ref{eq:ptheory}). }
\label{fig:hyp12_14}
\end{figure}
When the hyperviscous term used is of the form $\nu_h k_\perp^4 h_s$,
the effective hyperviscous damping rate is increased by a factor of
the mass ratio $m_i/m_e$ compared to equation~(\ref{eq:ptheory}); to
match the results, the formula must be amended to
\begin{equation}
\frac{P_{\nu_h}}{P_{tot}}= \frac{(m_i/m_e)\nu_h k_\perp^4}
{2\gamma + (m_i/m_e)\nu_h k_\perp^4}.
\end{equation}
When the hyperviscous term used is of the form $\nu_h k_\perp^4 g_s$,
the results agree fairly well with equation~(\ref{eq:ptheory}).


Here are some additional notes and considerations:
\begin{itemize}
\item Note that the antenna power is unknown because, although the amplitude
of the antenna is known, the power transmitted into the plasma depends
on the plasma response which is not a straightforward function of the
impedance matching.
\item Since the linear modes are damped by collisionles damping frequency 
$\gamma$, should the energy effectively be damped by $2 \gamma$?
\item Check the ion hyperviscosity to see if it improves the fit.
\item If the antenna power is written $P_a =\frac{k_\perp^2 A_{\| 0}}{\Delta t}$,
how do we determine this timescale for power input?  More generally,
knowing the linear damping rates and antenna amplitude, can we predict
the steady state amplitude of the energy?  This is complicated by the
fact that there are more than one energy in the system, and so perhaps
it is not so simple.
\end{itemize}