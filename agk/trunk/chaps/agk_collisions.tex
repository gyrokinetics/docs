%==============================================================================
% AstroGK Collisions
%==============================================================================
\chapter{Collisions}
%==============================================================================

%==============================================================================
\section{\T{AstroGK}'s Implementation of Collisions}

Let us be specific about the implementation of collisions and
hypercollisionality in \T{GS2}.  This is well described in a note by
Greg Hammett dated June 26, 2003; this note also includes insightful
discussion that is not included here.  I will summarize his results
here and update them to include the classical diffusion operator that
arises from taking pitch angle derivative with at constant guiding
center $\V{R}$ rather than constant position $\V{r}$.

The electron collision operator includes both electron-electron and
electron-ion collisions with the equilibrium Maxwellian distribution,
\begin{equation}
\langle \C_e(h_e)\rangle_{\V{R}_e} = \langle \C_{ee}(h_e, F_{0e}) 
+  \C_{ei}(h_e, F_{0i}) \rangle_{\V{R}_e}
\end{equation}
 while the ion collision operator includes
only ion-ion collisions with the equilibrium Maxwellian distribution,
\begin{equation}
\langle \C_i(h_i)\rangle_{\V{R}_i} = \langle \C_{ii}(h_i, F_{0i})\rangle_{\V{R}_i}.
\end{equation}

\T{Question: What happens to the adiabatic piece of the distribution function 
in the collision operator?}
%------------------------------------------------------------------------------
\subsection{Electron Collision Operator}
The electron collision operator  is given by
\begin{equation}
\langle \C_e(h_e)\rangle_{\V{R}_e}= \sum_\V{k} e^{i \V{k} \cdot \V{R}_e}
\nu_e(v) \frac{1}{2} \left\{ \frac{\partial}{\partial \xi} \left[ (1-\xi^2) 
\frac{\partial h_{\V{k}e}}{\partial \xi} \right]
-\frac{v^2  }{v_{te}^2}(1+\xi^2)  \frac{k_\perp^2 \rho_e^2}{2} h_{\V{k}e}
\right\}
\label{eq:defce}
\end{equation}
where the coefficient, dependent only on the magnitude of velocity, is
given by
\begin{equation}
\nu_e(v) = \nu_{ei} \left(\frac{v_{te}}{v}\right)^3 \left[ Z_i^2 
+ H_{ss} \left(\frac{v}{v_{te}}\right) \right]
\label{eq:nue_def}
\end{equation}
(NOTE here that in the \T{GS2}code, the factor of $1/2$ in
\eqref{eq:defce} is actually absorbed into the constant $\nu_e(v)$
and so actually appears only in \eqref{eq:nue_def}) with the
like-particle collision coefficient is given by
\begin{equation}
H_{ss} (x)= \left( 1- \frac{1}{2x^2} \right) \frac{2}{\sqrt{\pi}} \int_0^{x}
dt e^{-t^2} +   \frac{1}{\sqrt{\pi}} \frac{ e^{-x^2}}{x}
\end{equation}
and with 
\begin{equation}
\nu_{ei}= \frac{4 \pi e^4 n_e \lambda}{m_e^{1/2} ( 2 T_e)^{3/2}}.
\end{equation}
Here $\lambda$ denotes the Coulomb logarithm $\lambda = \ln
(\Lambda_{ee}) \simeq \ln (\Lambda_{ei})$, the pitch angle coordinate is
defined by $\xi \equiv v_\parallel/v$, and the thermal velocity is defined
by $v_{te}^2 = 2 T_e/m_e$. The first term in
\eqref{eq:defce} is the standard pitch-angle collision operator and the
second term is a classical-diffusion correction due to the fact that
the derivatives $\partial/\partial \xi$ are evaluated at constant
guiding center $\V{R}_s$ rather than at constant position $\V{r}$.  At
the moment the second term is included when \T{cfac=1.0} in the
collisions namelist; this is the default behavior (a change from
earlier versions of the code).  This correction term is motivated and
derived in Schekochihin et al. (2006).

\figref{fig:coeff_coll} plots the variation of the functions
$H_{ss}(v/v_{ts})$ and  $\nu_e(v/v_{te})$ with energy, or magnitude of velocity.
%------------------------------------------------------------------------------
\subsection{Ion Collision Operator}
Analogous to the electron collision operator, but neglecting
ion-electron collisions, the ion collision operator is given by
\begin{equation}
\langle \C_i(h_i)\rangle_{\V{R}_i}=\sum_\V{k} e^{i \V{k} \cdot \V{R}_i}
\nu_i(v) \frac{1}{2} \left\{ \frac{\partial}{\partial \xi} \left[ (1-\xi^2) 
\frac{\partial h_{\V{k}i}}{\partial \xi} \right]
-\frac{v^2  }{v_{ti}^2}(1+\xi^2)  \frac{k_\perp^2 \rho_i^2}{2} h_{\V{k}i}
\right\}
\label{eq:defci}
\end{equation}
where
\begin{equation}
\nu_i(v) = \nu_{ii} \left(\frac{v_{ti}}{v}\right)^3
H_{ss} \left(\frac{v}{v_{ti}}\right)
\end{equation}
and 
\begin{equation}
\nu_{ii}= \frac{4 \pi Z_i^4 e^4 n_i \lambda}{m_i^{1/2} ( 2 T_i)^{3/2}}.
\end{equation}

\figref{fig:coeff_coll} plots the variation of the function
$\nu_i(v/v_{ti})$ with energy, or magnitude of velocity.

\begin{figure}
\hbox to \hsize{ \hfill \epsfxsize16cm \epsffile{figs/coeff_coll.ps} \hfill}
\caption{Plot of the functions $H_{ss}(v/v_{ts})$, $\nu_e(v/v_{te})/\nu_{ei}$,
and  $\nu_i(v/v_{ti})/\nu_{ii}$ to demonstrate how these coefficients 
vary with energy (magnitude of velocity).}
\label{fig:coeff_coll}
\end{figure}

%------------------------------------------------------------------------------
\subsection{Collisional Coefficients}
In the \T{GS2} input file, the collisional coefficients \T{vnewk}$_s$ 
are used as the values of $\nu_{ei}$ and $\nu_{ii}$ in the electron and
ion collision operators above.  If we denote these user supplied values as
\T{vnewk}$_e = \hat{\nu}_{ce}$ and \T{vnewk}$_i = \hat{\nu}_{ci}$, then 
the normalization is given by
\begin{equation}
\hat{\nu}_{cs}= \frac{\nu_{cs} a_0}{v_{t0}}.
\end{equation}

%------------------------------------------------------------------------------
\subsection{Collisional Heating}
To estimate the collisional heating, we take the gyrokinetic equation,
\begin{equation}
\frac{\partial h_s}{\partial t} 
+v_\parallel \frac{\partial h_s}{\partial z} +
\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right]  -
\left\langle \C(h_s) \right\rangle_{\V{R}_s}  
= \frac{q_s}{T_{0s}} \frac{\partial \langle
\chi \rangle_{\V{R}_s}}{\partial t} F_{0s},
\end{equation}
multiply by $T_s h_s/F_{0s}$ and integrate over all space and velocity
to give the entropy-balance equation.  (In fact, to be more precise,
in complex space we must actually take $\int_{\V{R}} \int_\V{v}T_s
h_s^* \mbox{GK}/F_{0s} + \int_{\V{R}} \int_\V{v}T_s h_s
\mbox{GK}^*/F_{0s}$, where $\mbox{GK}$ denotes the gyrokinetic equation; but 
we will be somewhat loose with notation here.)  The second and third
terms on the left-hand side give nothing when integrated over all
space for periodic boundary conditions, leaving the result
\begin{eqnarray}
 \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
q_s \frac{\partial \left\langle\chi\right\rangle_{\V{R}_s} }{\partial t} h_s - 
\frac{d}{dt}\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{0s}}{2F_{0s}}h^2_{s} 
+  \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{0s}}{F_{0s}}\left\langle h_s\C(h_s) \right\rangle_{\V{R}_s} = 0  
\end{eqnarray}

Let us now take a closer look at the heating from the collisional term.  Writing
the gyrokinetic distribution function  as a Fourier series 
\begin{equation}
h_s(\V{R}_s,\V{v},t)=\sum_\V{k} h_{\V{k}s}(\V{k},\V{v},t) e^{i \V{k} \cdot \V{R}_s}
\end{equation}
and using a generalization of \eqref{eq:defce} and \eqref{eq:defci}, we find
\begin{eqnarray}
\lefteqn{\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{0s}}{F_{0s}}\left\langle h_s\C(h_s) \right\rangle_{\V{R}_s}}&&  \\
& =& 
\sum_\V{k}\sum_{\V{k}'}
\int\frac{d^{3}{\bf R}_s}{V}  e^{i (\V{k}+\V{k}') \cdot \V{R}_s} T_s
\int d^{3}{\bf v} \frac{\nu_s(v)}{2F_{0s}} h_{\V{k}'s}
 \left\{ \frac{\partial}{\partial \xi} \left[ (1-\xi^2) 
\frac{\partial h_{\V{k}s}}{\partial \xi} \right]
-\frac{v^2  }{v_{ts}^2}(1+\xi^2)  \frac{k_\perp^2 \rho_s^2}{2} h_{\V{k}s}
\right\} \nonumber
\end{eqnarray}
Using the property
\begin{equation}
\int d^{3}{\bf R}_s  e^{i (\V{k}+\V{k}') \cdot \V{R}_s}
= \delta (\V{k}+\V{k}'),
\end{equation}
the reality condition that
\begin{equation}
h_{\V{-k}s}= h^*_{\V{k}s},
\end{equation}
and transforming to velocity $v$, pitch angle $\xi=
v_\parallel/v$, and gyrophase angle $\theta$ coordinates
\begin{equation}
\int d^{3}{\bf v} = \int_0^\infty v^2 dv \int_{-1}^1 d\xi \int_0^{2\pi} d\theta,
\end{equation}
this simplifies to
\begin{eqnarray}
\lefteqn{\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{0s}}{F_{0s}}\left\langle h_s\C(h_s) \right\rangle_{\V{R}_s}}&&  \\
& =& 
\sum_\V{k} \pi T_s
 \int_0^\infty v^2 dv \frac{\nu_s(v)}{F_{0s}(v)} \left\{ \int_{-1}^1 d\xi
 h^*_{\V{k}s}\frac{\partial}{\partial \xi} \left[ (1-\xi^2) 
\frac{\partial h_{\V{k}s}}{\partial \xi} \right]
-\frac{v^2  }{v_{ts}^2} \frac{k_\perp^2 \rho_s^2}{2}
\int_{-1}^1 d\xi(1+\xi^2)  |h_{\V{k}s}|^2 \right\}. \nonumber
\end{eqnarray}
We may now perform an integration by parts in pitch angle on the pitch
angle scattering term to obtain the final, sign-definite result
\begin{eqnarray}
\lefteqn{\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{0s}}{F_{0s}}\left\langle h_s\C(h_s) \right\rangle_{\V{R}_s}}&& 
\label{eq:collheat}  \\
& =& 
-\sum_\V{k} \pi T_s
 \int_0^\infty v^2 dv \frac{\nu_s(v)}{F_{0s}(v)} \left\{ \int_{-1}^1 d\xi
(1-\xi^2) 
\left|\frac{\partial h_{\V{k}s}}{\partial \xi} \right|^2
+\frac{v^2  }{v_{ts}^2} \frac{k_\perp^2 \rho_s^2}{2}
\int_{-1}^1 d\xi(1+\xi^2)  \left|h_{\V{k}s}\right|^2 \right\}. \nonumber
\end{eqnarray}


%==============================================================================
\newpage
\section{Hypercollisionality}
The hypercollisionality operator for a species $s$ is defined by
\begin{equation}
\langle \C_{ns}(h_s)\rangle_{\V{R}_s}=\sum_\V{k} e^{i \V{k} \cdot \V{R}_s}
\nu_{Hs} (k_\perp \rho_i)^n  \frac{1}{2} 
\left\{ \frac{\partial}{\partial \xi} \left[ (1-\xi^2) 
\frac{\partial  h_{\V{k}s}}{\partial \xi} \right]
-\frac{v^2}{v_{ts}^2}(1+\xi^2)  \frac{k_\perp^2 \rho_s^2}{2}  h_{\V{k}s}
\right\}
\label{eq:defhcs}
\end{equation}
where $\nu_{Hs}$ is a constant coefficient, independent of velocity.
%------------------------------------------------------------------------------
\subsection{Goal of Hypercollisionality}
The aim of any hyperdamping term is to model the transfer of energy
from the smallest resolved scales in the box to smaller, unresolved
scales.  Without such a mechanism, energy builds up at the smallest
scales because there are no smaller scale modes with which to couple
nonlinearly.  Hence, a bottleneck in the energy spectrum results.

One task to be completed is to estimate the nonlinear energy transfer
rate (as a function of amplitude) and determine the minimum necessary
hyperdamping to remove that energy.  Although, at the moment the
magnitude of linear hypercollisional damping is not well understood
(in relation to the coefficient supplied in the input file), in
principle we can at least connect the required effective hyperdamping
to the nonlinear energy transfer rate.  Due to \emph{critical
balance}, the nonlinear transfer rate should simply be of order the
linear wave frequency, $k_\perp v_\perp \sim k_\parallel v_A$.  Hence,
the requirement for hyperdamping is to achieve $\gamma/\omega >1$ at
the end of the cascade, meaning that the energy will be damped out in
roughly one wave period.

A more elegant technique, rather than to just choose some constant
coefficient for the hypercollisional damping in any given run, is to
allow the coefficient for the hypercollisional damping to vary as a
function of simulation quantities (such as $|\chi|^2$), so that the
effective damping supplies just the right amount of energy transfer
for a given amplitude at the smallest resolves scales in the box.
Hence, rather than specifying two parameters, the driving amplitude
and hyperdamping amplitude, we simply specify the driving amplitude
and allow the hyperdamping to vary in such a way as to always provide
at least the minimum required damping rate.
%------------------------------------------------------------------------------
\subsection{Hypercollisionality in the Reduced MHD Limit}
To find the what form a hypercollisional term takes in the Reduced MHD
Limit will shed light on the effect of hypercollisionality.  

We begin with the gyronkinetic equation written in terms of $g_s$,
\begin{equation}
\frac{\partial g_s}{\partial t} 
+v_\parallel  \frac{\partial g_s}{\partial z} 
+\frac{q_s}{T_s} v_\parallel F_{0s} \frac{\partial \phit}{\partial z} 
+\frac{c}{B} \left[ \phit - A ,h_s \right] -
\langle \C(h_s) \rangle  -
\langle \C_n(h_s) \rangle 
= -\frac{q_s}{T_s}F_{0s} \frac{\partial A}{\partial t} 
\end{equation}
where we have used following definitions
\begin{equation}
g_s \equiv h_s - \frac{q_s \langle \phi \rangle}{T_s}F_{0s} 
+  \frac{q_s \langle \V{v}_\perp \cdot \V{A}_\perp \rangle}{c T_s}F_{0s} ,
\end{equation}
\begin{equation}
\phit \equiv   J_0(\frac{k_\perp v_\perp}{\Omega_s}) \hat{\phi}  + 
\frac{J_1\left(\frac{k_\perp v_\perp}{\Omega_s}\right)} {\frac{k_\perp
v_\perp}{\Omega_s}}
\frac{m_s v^2_\perp}{q_s} \frac{\delta \hat{B}_\parallel}{B_0},
\end{equation}
and
\begin{equation}
A \equiv   J_0(\frac{k_\perp v_\perp}{\Omega_s}) \frac{v_\parallel A_\parallel }{c}
\end{equation}
To derive the vorticity equation, the first step is to multiply the
gyrokinetic equation by $q_s$, ring average at constant position
$\V{r}$, integrate the equation over velocity, and sum over species.
We will consider each term in turn.
Using the quasineutrality condition in
terms of $g_s$, integrating over velocity for the potential terms, and
solving for the integral of $g_s$ gives us a form for the first term
\begin{equation}
\frac{\partial }{\partial t} \sum_s \int_\V{v}  q_s \langle g_s \rangle_\V{r} =
\sum_s  \frac{q_s^2 n_{0s}}{T_{0s}} (1-\Gamma_{0s}) \frac{\partial \hat{\phi}}{\partial t} 
- \sum_s \frac{q_s^2 n_{0s}}{T_{0s}} \Gamma_{1s} \frac{\partial
}{\partial t} \left( \frac{T_s}{q_s}
\frac{\delta \hat{B}_\parallel}{B_0} \right).
\end{equation}

The second term, after using the Parallel Ampere's Law in terms of 
$g_s$, becomes
\begin{equation}
\frac{\partial }{\partial z} \sum_s \int_\V{v}  q_s v_\parallel 
\langle g_s \rangle_\V{r} 
=- \frac{c}{ 4 \pi} \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel.
\end{equation}

The third term integrates to zero because it is odd in $v_\parallel$.

The fourth term, the nonlinear term, we neglect because it does not affect the
form of the hypercollisional term.

The fifth term is the physical collisional term.  Since we are
generally interested in collisionless problems for which $\omega >
\nu_c$, we neglect this term as well.

The term on the right-hand side is also  odd in $v_\parallel$ and so 
contributes nothing after integration over velocity.

The sixth term on the left-hand side is the hypercollisional term. 
The  ring average at constant position
$\V{r}$ of the hypercollisional operator becomes
\begin{equation}
\langle\langle \C_{ns}(h_s)\rangle_{\V{R}_s}\rangle_{\V{r}}
= \sum_\V{k} e^{i \V{k} \cdot \V{r}} J_0(\alpha_s)
\nu_{Hs} (k_\perp \rho_i)^n  \frac{1}{2} 
\left\{ \frac{\partial}{\partial \xi} \left[ (1-\xi^2) 
\frac{\partial  h_{\V{k}s}}{\partial \xi} \right]
-\frac{v^2}{v_{ts}^2}(1+\xi^2)  \frac{k_\perp^2 \rho_s^2}{2}  h_{\V{k}s}
\right\}
\end{equation}
where $\alpha_s=k_\perp v_\perp/ \Omega_s$
Summing over species and integrating over velocity gives
\begin{eqnarray}
\lefteqn{\sum_s  \int_\V{v} q_s \langle\langle 
\C_{ns}(h_s)\rangle_{\V{R}_s}\rangle_{\V{r}} } && \\
&=& \sum_s  \int_\V{v} q_s \sum_\V{k} e^{i \V{k} \cdot \V{r}} J_0( \alpha_s)
\nu_{Hs} (k_\perp \rho_i)^n  \frac{1}{2} 
\left\{ \frac{\partial}{\partial \xi} \left[ (1-\xi^2) 
\frac{\partial  h_{\V{k}s}}{\partial \xi} \right]
-\frac{v^2}{v_{ts}^2}(1+\xi^2)  \frac{k_\perp^2 \rho_s^2}{2}  h_{\V{k}s}
\right\}
\end{eqnarray}

%==============================================================================
\newpage
\section{Adaptive Hypercollisionality}
Because the effectiveness of the hypercollisionality in damping the
turbulent cascade is dependent on the structure of the distribution
function in velocity space, determining the value of the
hypercollisional coefficient needed to achieve the desired amount of
damping is difficult. Thus, an algorithm for adaptively adjusting the
hypercollisional coefficient to yield just the right amount of damping
is extremely valuable.  But even this requires a diagnostic of the
effective damping of the cascade.  Here we review a model-dependent
derivation of this diagnostic and explain the algorithm for adjusting
the hypercollisionality.

%------------------------------------------------------------------------------
\subsection{Estimation of $\gamma/\omega$ from \T{AstroGK}}
To dissipate the turbulent cascade at the highest resolved
wavenumbers---and thus avoid a bottleneck of energy---we want the
effective normalized dissipation in the nonlinear simulation
$\gamma_{nl}(k_\perp)/\omega_{nl}(k_\perp)$ to take on a large enough
value to damp the cascade, where $\gamma_{nl}(k_\perp)$ is the damping
rate of the nonlinear fluctuations at some value $k_\perp$ and
$\omega_{nl}(k_\perp)$ is the nonlinear energy transfer rate to higher
wavenumbers.  At the same time, we do not want
$\gamma_{nl}/\omega_{nl}$ to be too large or the hypercollisional
damping will affect too much of the dynamic range of the simulation.
A good estimate for the value needed is $\gamma_{nl}/\omega_{nl}
\simeq 1/(2\pi)\simeq 0.16$ at $k_\perp = k_{\perp \mbox{max}}1/\sqrt{2}$.

We can use the heating diagnostics in \T{AstroGK} along with a model
for the nonlinear transfer rate to calculate a value of
$\gamma_{nl}(k_\perp)/\omega_{nl}(k_\perp)$.  An estimate of the
damping rate is $\gamma_{nl}(k_\perp)= P_k(k_\perp)/E_k(k_\perp)$,
where $P_k(k_\perp)$ is the heating power within a given wavenumber
band centered at $k_\perp$ and $E_k(k_\perp)$ is the total energy
contained in that band.  We must normalize this damping rate by a nonlinear
energy transfer frequency; we use the  cascade model from
\cite{Howes:2007} to determine $\omega_{nl}(k_\perp)$. The nonlinear
cascade rate from this model is 
\begin{equation} 
\omega_{nl}= C_2
k_\perp v_\perp(k_\perp) = C_2 k_\perp \alpha \delta
B_\perp(k_\perp)/\sqrt{4
\pi n_i m_i}
\end{equation}
where 
\begin{equation}
\alpha(k_\perp)   = \left\{ 
\begin{array}{cc} 
1,& k_\perp\rho_i\ll 1  \\
k_\perp \rho_i/
{\sqrt{\beta_i+ 2/(1 + T_e/T_i)}}, & k_\perp\rho_i\gg 1
\end{array} \right.
\label{eq:alpha}
\end{equation}
and we can use 
\begin{equation}
\alpha = \overline{\omega} \simeq \left[ 1 + \frac{(k_\perp \rho_i)^2}
{\beta_i + 2/(1 + T_e/T_i)} \right]^{1/2}
\end{equation}
 over all values of $k_\perp$.
Thus we find 
\begin{equation} 
\omega_{nl}= C_2 k_\perp \delta B_\perp(k_\perp)/\sqrt{4 \pi n_i m_i} 
\left[ 1 + \frac{(k_\perp \rho_i)^2} {\beta_i + 2/(1 + T_e/T_i)}
\right]^{1/2} 
\end{equation} 
where the value of the critical balance Kolmogorov constant is $C_2
\in [1,2]$.
Normalizing this to the \T{AstroGK} normalization,
\begin{equation} 
\left( \frac{\omega_{nl} a_0}{v_{t0}} \right)= C_2 \frac{( k_\perp \rho_0)^2}{2}
\left( \frac{v_{t0}}{c} \frac{q_0 A_\parallel}{T_0}\frac{a_0}{\rho_0}\right)
\left( \frac{B_0}{v_{t0}\sqrt{4 \pi n_0 m_0} } \right)
\left(\frac{2 T_0}{m0} \frac{m_0 c}{q_0 B_0} \frac{1}{\rho_0 v_{t0}} \right)
\left[ 1 + \frac{(k_\perp \rho_i)^2} {\beta_i + 2/(1 + T_e/T_i)}
\right]^{1/2} 
\end{equation} 
which gives
\begin{equation} 
\hat{\omega}_{nl}= C_2 \frac{\hat{k}_\perp^2 \hat{A}_\parallel}{2 \sqrt{\beta_0}}
\left[ 1 + \frac{\hat{k}_\perp^2} {\beta_i + 2/(1 + T_e/T_i)}\right]^{1/2} 
\end{equation} 

%------------------------------------------------------------------------------
\subsection{Connection to Code Diagnostics}
One can use the code heating and energy diagnostics to determine the
magnitude of the magnetic field fluctuations at a given $k_\perp$. For
a given mode $(k_x, k_y)$, the magnetic energy in fluctuations
perpendicular to the mean field is 
\begin{equation} 
E_{B_\perp}(  k_x, k_y)= \frac{k_\perp^2 |A_{\parallel }(k_x, k_y)|^2}{8 \pi}
\end{equation} 
In normalized units this becomes 
\begin{equation} 
\hat{E}_{B_\perp}(  k_x, k_y)= \frac{\hat{k}_\perp^2 |\hat{A}_{\parallel }(k_x, k_y)|^2}{4 \beta_0}
\end{equation} 
which is calculated in \T{hk(it,ik)\%eapar} in the code.

Summing over all of the $N_{mode}$ modes in the band from $\hat{k}_\perp
- \Delta \hat{k}_\perp/2$ to $\hat{k}_\perp + \Delta \hat{k}_\perp/2$,
we find
\begin{equation} 
\hat{E}_{B_\perp}^{\Delta k_\perp}= \frac{\hat{k}_\perp^2 |\hat{A}_{\parallel }|^2}{4 \beta_0}
N_{mode}
\end{equation} 
assuming that all of the modes have statistically the same value of
$\hat{A}_{\parallel }$.
To connect with the formula for $\hat{\omega}_{nl}$ above,
I can use this to find
\begin{equation} 
 \frac{\hat{k}_\perp \hat{A}_\parallel}{2 \sqrt{\beta_0}} =
\sqrt{\frac{\hat{E}_{B_\perp}^{\Delta k_\perp}}{N_{mode}}}
\end{equation} 
so the nonlinear frequency can be determined by
\begin{equation} 
\hat{\omega}_{nl}= C_2 \hat{k}_\perp \sqrt{\frac{\hat{E}_{B_\perp}^{\Delta k_\perp}}{N_{mode}}}
\left[ 1 + \frac{\hat{k}_\perp^2} {\beta_i + 2/(1 + T_e/T_i)}\right]^{1/2} 
\end{equation} 


%------------------------------------------------------------------------------
\subsection{Rules for Adaptivity}
The rules for adapting the hypercollisional coefficient are outlined
here. First, we define a few quantities. The measures for 
\begin{equation}
f_1 = \frac{\gamma_{nl}}{\omega_{nl}} = \frac{P_1}{E_k \omega_{nl}}
\quad f_2 = \frac{\gamma_{nl}}{\omega_{nl}} =\frac{P_2}{E_k \omega_{nl}}
\end{equation}
where for the ions
\begin{equation}
P_1 =  P_{c i } + P_{Hc i}\quad P_2 = P_{Hc i}
\end{equation}
and for the electrons
\begin{equation}
P_1 =  P_{c i } + P_{Hc i} + P_{c e } + P_{Hc e}\quad P_2 = P_{Hc e}.
\end{equation}
For example, for the electrons, $f_1$ is the total normalized damping
rate and $f_2$ is the normalized damping due only to the electron
hypercollisionality.

The coefficient is only adjusted adaptively if there is enough energy in the
band compared to the total turbulent energy 
\begin{equation}
\frac{E_k}{E_{tot}} > 10^{-6}
\end{equation}
and adaptive hypercollisionality is specified for the species
\T{spec(is)\%adapt\_hc = T}. If these conditions are specified, 
then the following tests are performed to determine if the
hypercollisionality coefficient should be increased or decreased.

The threshold normalized damping rate is $f_t=$\T{spec(is)\%gw\_hc}; a
typical value is $f_t = 1/2 \pi \simeq 0.16$. When the
hypercollisional coefficient is adjusted, the collisional matrix must
be recalculated, so one wants to adjust the value as infrequently as
possible---thus, we define a buffer around the threshold value for
which the hypercollisional damping is acceptable. We define this as
$q=$\T{spec(is)\%gw\_frac}; a typical value is $q=0.1$.  The test is
simply that when $|f_n - f_t|/(f_n+f_t) > q$, the damping rate is
beyond the threshold buffer and the hypercollisional value \emph{may}
need to be adjusted.  For the moment, only the electron
hypercollisionality is adaptive---preliminary tests suggests adaptive
\emph{both} ion and electron hypercollisionality is unstable.

\paragraph{FIRST TEST: Damping too strong}
If total damping is outside the buffer and too strong
\begin{equation}
\frac{|f_1 - f_t|}{f_1+f_t} > q \mbox{ and } f_1 > f_t
\end{equation}
then we need to determine if reducing the electron hypercollisionality
will help (if real collisional damping dominates and is too strong, then reducing
electron hypercollisionality will not make any difference).  If the
electron hypercollisional damping is also outside the buffer and too
strong
\begin{equation}
\frac{|f_2 - f_t|}{f_2+f_t} > q \mbox{ and } f_2 > f_t
\end{equation}
and if the electron hypercollisionality is greater than the minimum value
\begin{equation}
\nu_{He} > \nu_{He \mbox{min}}
\end{equation}
then the electron hypercollisionality is adjusted according to
\begin{equation}
\nu_{He} = \max\left(  \nu_{He} \frac{f_t}{f_2},\nu_{He \mbox{min}} \right)
\end{equation}
The flag is set to \T{nuh\_changed = T} so that the collisional matrix
will be recalculated.

\paragraph{SECOND TEST: Damping too weak}
Else if  total damping is outside the buffer and too weak
\begin{equation}
\frac{|f_1 - f_t|}{f_1+f_t} > q \mbox{ and } f_1 < f_t
\end{equation}
then increase electron hypercollisionality up to the maximum value
\begin{equation}
\nu_{He} = \min\left(  \nu_{He} \frac{f_t}{f_1},\nu_{He \mbox{max}} \right)
\end{equation}
and the flag is set to \T{nuh\_changed = T} so that the collisional
matrix will be recalculated.


%%==============================================================================
