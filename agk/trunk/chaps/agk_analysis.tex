%==============================================================================
% Methods for Analysis of \GS2 Results
%==============================================================================
\chapter{Methods for Analysis of \agk Results}

%==============================================================================
\section{Analysis}

%------------------------------------------------------------------------------
\subsection{General Analysis Methods for Turbulence Runs}
\begin{enumerate}
\item Energy Spectra: Plot the steady-state spectra of all the fields.  How do 
the slopes of these spectra compare to theoretical expectations?
\item Steady-state at all scales:  Check the time evolution of the  $k$ by $k$ energy to 
determine if each scale has reached a quasi-steady state.
\item Movie of Energy Spectra: Compile a quick movie of the evolving 
spectra to visually see if it appears to reach a steady state.  Also, is 
it possible to see fluctuations in energy injection pass down through
the cascade?
\item Steady-State: Look at plots of the evolution of the plasma energy to
see if it has reached a steady-state.  Is the heating $d \E /dt$
fluctuating about zero (no net energy buildup in plasma)?
\item Heating: How much heating goes to ions vs.~electrons?
\item Heating by Mode: Are the channels of heating peaked in the wavenumber
ranges we expect? Is there ample dissipation at smaller scales?
\item Composite Spectra: After removing dissipation ranges, assemble spectra from 
different wavenumber ranges to get a clue about the global spectrum,
similar to the Great Power Law in the Sky.  This is not quite right
because of varying anisotropy, but it will make a nice rough picture
before trying a monumental size run (Insight Awards, etc.).
\item Electron Energy vs.~Beta: Plot $\delta_e$ vs.~$\beta_i$ to see how the
ratio $P_i/P_e$ changes with parameters.
\item Entropy Cascade: Can we identify the entropy cascade?
\item Make movies of field lines with existing run data.
\end{enumerate}

%------------------------------------------------------------------------------
\subsection{Analysis Tools for Development}
\begin{enumerate}
\item Contour plot of power on the $k_\perp$-$k_\parallel$ plane.  Note that
this needs to use the facility that derives the actual $k_\parallel$
following the fields lines, not just the $k_z$ as output in the
\T{.fields} file.  To get this output, I need to turn on the \T{write\_gs}
flag in the diagnostics namelist.  The procedure in memory intensive,
and should be done only for one timestep to get the output.
\item Polar spectra of heating rates
\item Make movies of field lines.
\end{enumerate}

%------------------------------------------------------------------------------
\subsection{Notes on \T{GS2} Output Files}
Here are some notes on analysis and data files:
\begin{enumerate}
\item The file \T{runname.fields} contains $z$-structure of 
all $k_x$ and $k_y$ modes.
\item The file \T{runname.kspec\_raw} contains polar spectra of energies
for all possible $k_\perp$ from all of the $k_x$ and $k_y$ modes.
\item The file \T{runname.kspec\_avg} contains binned and log-averaged values 
of polar spectra of energies using \T{nkpolar} bins. Default is such that
\T{nkpolar}$= \mbox{int}[\mbox{real}(\mbox{\T{naky}}+1)\sqrt{2}]$
\end{enumerate}
\newpage
\section{Transient Fit by Laplace Transform for Linear Runs}

The best way to get precise fits of the resonant frequency and damping
rate is to fit the transient behavior of the driven, damped system.

%------------------------------------------------------------------------------
\subsection{Laplace Transform}
Assume the linear system behaves as an linear operator with a source term
\begin{equation}
\frac{\partial \phi(t)}{\partial t} = \mathcal{L} \phi(t) + \mathcal{S}(t)
\end{equation}
where the linear operator gives a complex eigenvalue $-i(\omega_0 -i\gamma)$
\begin{equation}
\mathcal{L} \phi(t) = -i(\omega_0 -i\gamma)\phi(t)
\end{equation}
and the source term simply drives a constant frequency $\omega_s$
\begin{equation}
\mathcal{S}(t) = \left\{
\begin{array}{l}
S_0 e^{-i \omega_s (t-t_0)} \\
0
\end{array}
 \right. .
\end{equation}
Thus, we have the equation
\begin{equation}
\frac{\partial \phi(t)}{\partial t} =-i(\omega_0 -i\gamma)\phi(t) 
+ S_0 e^{-i \omega_s }.
\end{equation}
To solve the the time-dependent behavior if the antenna is turned on 
at time $t_0$, we do a Laplace transform of this system.

For the source term, we use the property of Laplace transforms that
\begin{equation}
\int_0^\infty \mathcal{U} (t-t_0) e^{-st} dt = e^{-t_0s} f(s)
\end{equation}
where the $\mathcal{U} (t-t_0)$ is Heaviside's unit function times $F(t)$
\begin{equation}
\mathcal{U}(t-t_0) = \left\{
\begin{array}{l}
F(t) \\
0
\end{array}
 \right.
\end{equation}
and $f(s)$ is the Lapace transform of $F(t)$.
The Laplace transformed equation becomes
\begin{equation}
s \hat{\phi}(s) -\phi(0) =-i(\omega_0 -i\gamma)\hat{\phi}(s) 
+ \frac{ S_0 e^{-as}}{s+i \omega_s}
\end{equation}
Solving for the Laplace transform $\hat{\phi}(s) $ gives
\begin{equation}
\hat{\phi}(s) = \frac{\phi(0)}{s+ i\omega_0 +\gamma}
+ \frac{ S_0 e^{-as}}{(s+i \omega_s)(s+ i\omega_0 +\gamma)}
\end{equation}

Taking the inverse Laplace transform of this solution gives
\begin{equation}
\phi(t) = \phi(0)e^{-i \omega_0 t}e^{-\gamma t} 
+ S_0 \frac{  e^{-i \omega_s (t-t_0)} - e^{-i \omega_0 (t-t_0)}e^{-\gamma (t-t_0)}}
{i(\omega_0- \omega_s) +\gamma}
\end{equation}

Let us take the driving antenna to be turned on at $t_0=0$ and zero initial amplitude
$\phi(0)=0$, so we get
\begin{equation}
\phi(t) = S_0 \frac{  e^{-i \omega_s t} - e^{-i \omega_0 t}e^{-\gamma t}}
{i(\omega_0- \omega_s) +\gamma},
\end{equation}
For late times $\gamma t \gg 1$, this solution agrees with the Fourier solution
\begin{equation}
\phi(t) = S_0 \frac{  e^{-i \omega_s t}}{i(\omega_0- \omega_s) +\gamma}.
\end{equation}

Solving for  the amplitude $|\phi(t)|^2 $of the antenna as a function of time, 
we obtain
\begin{equation}
|\phi(t)|^2 = S_0^2 \frac{  1+ e^{-2\gamma t} - 2e^{-\gamma t} \cos [(\omega_0- \omega_s)t]}
{(\omega_0- \omega_s)^2 +\gamma^2}.
\end{equation}
Again, at long times,  we get the usual Lorentzian response 
\begin{equation}
|\phi(t)|^2 = \lim_{t \rightarrow \infty}  \frac{  S_0^2}
{(\omega_0- \omega_s)^2 +\gamma^2}
\end{equation}

%------------------------------------------------------------------------------
\subsection{Fitting the Laplace Transform Solution}
 There is a simple strategy for obtaining precise fits of the Lapace
Transform solution. For a given driving frequency $\omega_s$, there are
three parameters to the fit: the amplitude $S_0$, the resonant
frequency $\omega_0$, and the damping rate $\gamma$.  These parameters can
be determine to high precision using the simple precodure below.
\begin{enumerate}
\item Run one case (A) off resonance $|\omega_0- \omega_s| \gg \gamma$.  
To obtain convergence to a fraction $f$, the runtime needs to be 
\begin{equation}
t > \frac{-\ln(f/2)}{\gamma}.
\end{equation}
For a convergence of $f=0.001$, this is approximately $t>7.6/\gamma$.
\item From case A, you can determine the resonant frequency quite precisely
by fitting the oscillation at the beat frequency $\omega_0- \omega_s$.
\item After determining the value $\omega_0- \omega_s$, one can determine 
the amplitude $S_0$ by fitting the final amplitude after the
transients have settled down.  This is only possible if $|\omega_0-
\omega_s| \gg \gamma$.
\item Run a secondd case at resonance (B) such that
$|\omega_0- \omega_s| \ll \gamma$.  The required run time is the same as before.
\item From case B, determine the damping rate $\gamma$ by fitting for 
the final amplitude. 
\item Check the fit of B at all times to see if any further adjustments
are necessary.  Notice, if the resonant frequency of the fit is adjusted
slightly, the fit changes dramatically.
\end{enumerate}

%==============================================================================
\newpage
\section{Frequency Sweep Runs}

\begin{enumerate}
\item Set $k_\perp \rho_i$, $n_\lambda$, $n_E$ for the run.
\item Collisionality of each species $C_i$, $C_e$ must be less 
the desired damping rate.
\item Timestep $\Delta t$ must be much less than the frequency of the 
resonance (or the resonant frequency will be too low).
\item Guess at resonant frequency $\omega_0$ and set initial 
frequency $\omega_i$ lower.
\item Choose sweeping rate $\dot{\omega}$ such that the sweep is slower than
the damping rate (or the Lorentzian will be pushed over and the amplitude will
show a lot of oscillations).
\end{enumerate}

To plot the data using Gnuplot
\begin{enumerate}
\item First, extract the data from the run\_name.out file using grep \\
\T{ grep 'w='  swpb1t1k10.out > swpb1t1k10.w}
\item Next use Gnuplot to plot the data against a Lorentzian with three parameters,
the peak amplitude $A_0$, the damping rate $\gamma$ and the resonant frequency $\omega_0$.

\item To plot, type \\ 
\T{plot 'swpb1t1k12.w' u 2:4, 'swpb1t1k12.w' u 2:(A*g*((\$2-w)**2 + g**2)**(-0.5))}

This will plot the data against a Lorentzian of the form
\[ A(\omega) = \frac{A_0 \gamma}{\sqrt{(\omega -\omega_0)^2 +\gamma^2}}
\]


\item If you want to plot the lorentzian and have gunplot automatically fit:\\
\T{f(x)=(A/sqrt((x-w)**2.+g**2.))}\\
\T{fit f(x) 'test.dat' u 2:4 via A, g, w}\\
\T{plot 'test.dat' u 2:4, '' u 2:(f(\$2))}\\
\T{replot}


\end{enumerate}

%==============================================================================
\section{Ion to Electron Heating Ratio Runs}
\begin{enumerate}
\item Set $k_\perp \rho_i$, $n_\lambda$, $n_E$ for the run.  In these cases,
a large number of points in velocity space ($n_\lambda=40$ and  $n_E=40$)
is necessary to resolve the very weak damping of the subdominant species.
\item Collisionality of each species $C_i$, $C_e$ must be less 
the desired damping rate for that species; for the less damped species
this can be VERY small.
\item Run the code until it converges with $\omega_{tol}=10^{-6}$.
\item Columns 4 and 5 (the first two after heating\_rate) in the .h file 
give the ion and electron power; copy them to the data file to keep.
\end{enumerate}

To plot the data using Gnuplot
\begin{enumerate}
\item First, extract the data from the run\_name.out file using grep \\
\T{ grep heating\_rate hrb10t\_01k10.out > hrb10t\_01k10.h}

\item Next use Gnuplot to plot the data to check that it has stabilized by typing \\
\T{plot 'hrb10t\_01k10.h' u 2:4, 'hrb10t\_01k10.h' u 2:5}

\end{enumerate}
%==============================================================================
\section{Decaying Alfven Wave Runs}

\begin{enumerate}
\item Two runs are necessary: the first to build up a single eigenmode
by driving at a certain frequency and wavenumber; the second to turn off the
driving
antenna and watch the wave decay
\item Wave set up runs 
\begin{enumerate}
\item nstep$=10000$
\item $nk_{stir}=1$
\item $ant_{off}=$F
\item $nwrite=10$
\item $navg=20$
\item $omegatol=1.0 \times 10^{-6}$
\item save\_for\_restart $=$ T
\end{enumerate}
\item Decay runs 
\begin{enumerate}
\item nstep$=5000$ and  delt\_option $=$ check\_restart
\item $nk_{stir}=0$
\item $ant_{off}=$T
\item $nwrite=1$
\item $navg=1$
\item $omegatol=-1.0$
\item save\_for\_restart $=$ F
\end{enumerate}
\item Set $k_\perp \rho_i$, $n_\lambda$, $n_E$ for the run.  In these cases,
a large number of points in velocity space ($n_\lambda=40$ and  $n_E=40$)
is necessary to resolve the very weak damping of the subdominant species.
\item Collisionality of each species $C_i$, $C_e$ must be less 
the desired damping rate for that species; for the less damped species
this can be VERY small.
\item Run the code until it converges with $\omega_{tol}=10^{-6}$.
\item Columns 4 and 5 (the first two after heating\_rate) in the .h file 
give the ion and electron power; copy them to the data file to keep.
\end{enumerate}

To plot the data using Gnuplot
\begin{enumerate}
\item First, extract the data from the run\_name.out file using grep \\
\T{ grep heating\_rate hrb10t\_01k10.out > hrb10t\_01k10.h}

\item Next use Gnuplot to plot the data to check that it has stabilized by typing \\
\T{plot 'hrb10t\_01k10.h' u 2:4, 'hrb10t\_01k10.h' u 2:5}

\end{enumerate}
%==============================================================================
\newpage
\section{Analysis for Astrophysical Turbulence Runs}
This section describes a number of different diagnostics and scripts for 
analysis of runs simulating astrophysical turbulence.
\begin{enumerate}
\item Parallel Fourier spectra: Analyze the \T{runname.fields} fields by taking
each $(k_x,k_y)$ mode and Fourier analyzing the structure along the
field.  This proves that the run was resolved along the field and did not 
reach the Nyquist frequency of the parallel grid. We use the \T{sm} script
\T{/sm/agk/fields\_fft.sm} to create a series of postscript files, one for each 
mode, showing the real space structure and fast Fourier transform.  These can
be made into an animated gif move using \T{pstogif.all} and \T{whirlgif}.
\item Steady State: It is extremely important that turbulence
data used for analysis have reached a statistically steady state,
where energy injected at large scale is balanced by total energy
dissipated (mostly at small scales). There are several methods to check this:
\begin{enumerate}
\item Movie of energy spectrum: One can compile a movie of the energy spectrum
using \T{/sm/agk/kspec\_mov.sm} and inspect visually if some part of
the spectrum is monotonically increasing or decreasing in time, rather than just
fluctuating about some steady solution.
\item Total energy vs.~time: A better solution is to plot the evolution of 
the energy in the plasma over time to see if any components are
increasing or decreasing.  However, this is dominated by the driving
scale; the Driving scale \Alfven waves produce an oscillating behavior
in $\delta B_\perp$ and $\delta f_i$ (or $v_{\perp i}$).
\item Total energy by $k_\perp$ vs.~time: We can also analyze the
\T{runname.kspec\_raw} file using \T{gs2\_analysis.f90}, option 0, to extract 
the time behavior at each mode, which can then be plotted using
\T{/agk/ek\_vs\_t.sm}. This is the best diagnsotic to determine if you have 
reached a steady state at all wavenumbers.  Note the $h_i$ seems to adjust most,
slowly; you can also look at \T{etot} for each wavenumber to get the overall
picture.
\end{enumerate}

\item Contour plot of Power in $k_\perp$--$k_z$ plane: One can use
the Fortran program \T{kplane.e} to analyze the \T{runname.fields}
file to create a contour map of the power in various fields on the
$k_\perp$--$k_z$ plane using the script \T{/sm/agk/map\_kpkz.sm}. It
is a bit difficult to interpret this compared to theory since you
really should do this along the perturbed field lines to get the power
in the $k_\perp$--$k_\parallel$ plane.  But this can be a useful
diagnostic to ensure that nothing awful has occurred.
\end{enumerate}



%==============================================================================
\section{Dissipation Estimates for Nonlinear Runs}
\begin{enumerate}
\item I can use python script \T{tnl\_hr\_sum.py} to read the \T{runname.out.nc}
 file and output a \T{runname.tnl} file. 
\item Run \T{~/sm/kspec/swt\_hk\_diss.sm} to plot the estimated normalized
dissipation $P/(E \omega)\sim \gamma/\omega$.  Note that this does not take into
account the reduction in nonlinear frequency due to dissipation, so 
we may need a similar estimate based on $P/(E \omega_{nl})$.
\end{enumerate}
%==============================================================================
\newpage
\section{Amplitude Scan for Turbulent Driving}
In this section I review methods for analyzing a suite of runs to test
the proper amplitude for driving turbulence.  In short, I list here the basic 
methods used to interpret the data.
\begin{enumerate}
\item Plot final spectrum for each run to extract the slope of the steady-state 
spectrum.
\item Plot the energy in the driving mode vs.~time to show that we have reached a
steady state at different levels.
\item Contour plots of mode energy on the  $k_\perp-k_z$  plane.  Using the
\T{write\_gs} option, I should be able to also produce the same data (or at least
for $v_x^2$ and $v_y^2$) on the $k_\perp-k_\parallel$ plane; this plot
should produce the Goldreich-Sridhar $k_\parallel \propto
k_\perp^{2/3}$ law for driving amplitudes in the strong turbulence
regime.
\item Plot the antenna power in $P_{ant}$ vs. the heating measures $P_i+P_e$ and
$P_{ci}+P_{ce}+P_{Hci}+P_{Hce}$ to show that the net plasma energy
change is zero.  We should be able to look at the heating data by
$k_\perp$ as well to show that energy is indeed cascading to high
$k_\perp$ before it is dissipated.
\item We can also plot  the steady state $P_{ant}$ vs. the estimated 
saturated value of the energy $\partial \E_{sat} /\partial t$.
\item Plot the measured value of $\omega_{nl}/\omega$ as a function of 
the driving amplitude.  Can I explain what appears to be a relation
$\omega_{nl}/\omega \propto A^{3/2}$?
\end{enumerate}



%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
%OLD
%
%%------------------------------------------------------------------------------
%\subsection{$N$-dimensional Energy Spectra}
%The $N$-dimensional energy spectrum  $E^{(N)}$ is defined by the equation
%\begin{equation}
%E = \int E^{(N)} d^N \V{k}
%\end{equation}
%where the total energy is $E$.  Hence, the $N$-dimensional energy
%spectrum $E^{(N)}$ must have units of energy over the volume in 
%$N$-dimensional $\V{k}$-space.
%
%Here I define the  $N$-dimensional energy spectrum as
%\begin{equation}
%E^{(N)}= \frac{E}{\int d^N \V{k}}.
%\end{equation}
%We'll see if this functions as a working definition.  Below we'll
%consider a few examples to make this more concrete.
%%------------------------------------------------------------------------------
%\subsubsection{Hydrodynamic Kolmogorov Turbulence}
%The energy cascade rate of hydrodynamic turbulence is given by the
%eddy turn-around time at each scale, $\omega_{NL} \sim k v$.  For turbulence
%driven at $k_0$ with velocity $v_0$, taking the Kolmogorov hypothesis that
%the energy cascade rate is constant throughout the inertial range, and 
%defining the energy as $E=v^2$,
%we find 
%\begin{equation}
%\frac{\partial E}{\partial t} = \mbox{constant} = E \omega_{NL}= k v^3 
%=  k_0 v_0^3 
%\end{equation}
%Solving for the velocity as a function of the scale $k$ gives
%\begin{equation}
%v= \left(\frac{k_0}{k}\right)^{1/3} v_0.
%\end{equation}
%Hence, the energy as a function of  scale $k$  is 
%\begin{equation}
%E= \left(\frac{k_0}{k}\right)^{2/3} v_0^2.
%\end{equation}
%
%For one, two and three dimensions, the volumes and $N$-dimensional energy spectra
%are given by
%\begin{equation}
%\begin{array}{lll}
%\mbox{1-D} & \int d k=k & E^{(1)} = \frac{k_0^{2/3}}{k^{5/3}} v_0^2 \\
%\mbox{2-D} & \int d^2 \V{k}=\pi k^2 & E^{(2)} = \frac{k_0^{2/3}}{\pi k^{8/3}} v_0^2 \\
%\mbox{3-D} & \int d^3 \V{k}=\frac{4 \pi}{3} k^3 & E^{(3)} = \frac{3 k_0^{2/3}}{4 \pi k^{11/3}} v_0^2 
%\end{array}
%\end{equation}
%
%Now let's go ahead and integrate these functions to see if they give
%the same answer (as they should).
%The $1$-dimensional energy spectrum is integrated by
%\begin{equation}
%E = \int_{k_0}^{\infty} E^{(1)} dk  = \int_{k_0}^{\infty} \frac{k_0^{2/3}}{k^{5/3}} v_0^2  dk
%= \frac{3}{2} v_0^2.
%\end{equation}
%The $2$-dimensional energy spectrum is integrated by
%\begin{equation}
%E =\int_{0}^{2 \pi}  \int_{k_0}^{\infty} E^{(2)} k dk d\phi 
%= 2 \pi \int_{k_0}^{\infty} \frac{k_0^{2/3}}{\pi k^{8/3}} v_0^2 k dk 
%= 3 v_0^2.
%\end{equation}
%The $3$-dimensional energy spectrum is integrated by
%\begin{equation}
%E = \int_{0}^{\pi}\int_{0}^{2 \pi}\int_{k_0}^{\infty} E^{(3)} k^2 \sin \theta dk d\theta d\phi   
%= 4 \pi \int_{k_0}^{\infty}  \frac{3 k_0^{2/3}}{4 \pi k^{11/3}} v_0^2  k^2 dk
%= \frac{9}{2} v_0^2.
%\end{equation}
%The difference between the first two is simply because the 1-D
%spectrum is only integrated from $k_0$ to $\infty$ and does not
%include the $-k_0$ to $-\infty$ that the 2-D result includes because
%of the integration around in $\phi$.  I am not quite sure how to
%explain easily the difference of the 3-D result.
%%------------------------------------------------------------------------------
%\subsubsection{Magnetohydrodynamic Goldreich-Sridhar Turbulence}
%The energy cascade rate of magnetohydrodynamic turbulence is given by the
%eddy turn-around time at each perpendicular scale, $\omega_{NL} \sim k_\perp v_\perp$.  For turbulence
%driven at $k_0$ with velocity $v_0$ isotropically, taking the Kolmogorov hypothesis that
%the energy cascade rate is constant throughout the inertial range, and 
%defining the energy as $E=v_\perp^2$,
%we find 
%\begin{equation}
%\frac{\partial E}{\partial t} = \mbox{constant} = E \omega_{NL}= k_\perp v_\perp^3 
%=  k_0 v_0^3 
%\end{equation}
%Solving for the velocity as a function of the scale $k$ gives
%\begin{equation}
%v_\perp= \left(\frac{k_0}{k_\perp}\right)^{1/3} v_0.
%\end{equation}
%Hence, the energy as a function of  scale $k_\perp$  is 
%\begin{equation}
%E= \left(\frac{k_0}{k_\perp}\right)^{2/3} v_0^2.
%\end{equation}
%
%For strong turbulence, Goldreich-Sridhar conjectured that the cascade remains
%in \emph{critical balance}, where the nonlinear energy transfer frequency 
%balances the linear frequency of \Alfven waves, $\omega = k_\parallel v_A \sim  k_\perp v_\perp$.
%Plugging in the solution for $v_\perp$ above, we can solve for $k_\parallel$,
%\begin{equation}
%k_\parallel= k_\perp^{2/3} k_0^{1/3} \frac{v_0}{v_A}.
%\end{equation}
%
%I am having problems with understanding how to incorporate
%$k_\parallel$ into the $N$-dimensional energy spectrum, so I will drop
%the $k_\parallel$ part for the following argument, examining only the
%one, two and three dimensional spectra below.
%
%For one and two dimensions, the volumes and $N$-dimensional energy spectra
%are given by
%\begin{equation}
%\begin{array}{lll}
%\mbox{1-D} & \int d k_\perp=k_\perp & E^{(1)} = \frac{k_0^{2/3}}{k_\perp^{5/3}} v_0^2 \\
%\mbox{2-D} & \int d^2 \V{k_\perp}=\pi k_\perp^2 & E^{(2)} = \frac{k_0^{2/3}}{\pi k_\perp^{8/3}} v_0^2 
%\end{array}
%\end{equation}
%
%Now let's go ahead and integrate these functions to see if they give
%the same answer (as they should).
%The $1$-dimensional energy spectrum is integrated by
%\begin{equation}
%E = \int_{k_0}^{\infty} E^{(1)} dk_\perp  = \int_{k_0}^{\infty} \frac{k_0^{2/3}}{k_\perp^{5/3}} v_0^2  dk_\perp
%= \frac{3}{2} v_0^2.
%\end{equation}
%The $2$-dimensional energy spectrum is integrated by
%\begin{equation}
%E =\int_{0}^{2 \pi}  \int_{k_0}^{\infty} E^{(2)} k_\perp dk_\perp d\phi 
%= 2 \pi \int_{k_0}^{\infty} \frac{k_0^{2/3}}{\pi k_\perp^{8/3}} v_0^2 k_\perp dk_\perp
%= 3 v_0^2.
%\end{equation}
%Again, the difference between the first two is simply because the 1-D
%spectrum is only integrated from $k_0$ to $\infty$ and does not
%include the $-k_0$ to $-\infty$ that the 2-D result includes because
%of the integration around in $\phi$.



