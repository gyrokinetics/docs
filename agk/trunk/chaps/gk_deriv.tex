%==============================================================================
% Derivation of the Gyrokinetic Equation
%==============================================================================
\chapter{Derivation of the Gyrokinetic Equation}

%==============================================================================

This chapter is intended as a brief derivation of the gyrokinetic
equations for the astrophysical community; the plasma physics
literature contains detailed papers relating the development of linear
\cite{Antonsen:1980} and nonlinear \cite{Frieman:1982} gyrokinetic theory 
[NOTE: more references to be added].

The general approach to deriving the gyrokinetic equations follows.
We begin with the Fokker-Planck equation and Maxwell's equations and
define the limits of the gyrokinetic approximation.  First, the
Fokker-Planck equation is transformed to guiding center coordinates
and the ordering specified by the gyrokinetic approximations is
applied.  At lowest order, we discover that the equilibrium
distribution function is independent of gyrophase angle.  At the next
order, it is proven that the equilibrium distribution function must
take the form of a Maxwellian; this solution is used to solve the
equation at this order which consists of of a particular solution
related to the equilibirium and a homogeneous part that is
indepedent of the gyrophase angle.  At the next order, a ring average
at fixed guiding center yields a closed equation for the unknown
homogeneous solution---the Gyrokinetic equation.

The remaining equations necessary to define a complete system of
gyrokinetic equations are derived similarly from Maxwell's equations:
integration of the distribution function (to the same order as the
gyrokinetic equation) over the velocity and performing a ring average
ar constant position gives the charge and current necessary in
Coulomb's Law and Ampere's Law. Performing this procedure on Coulomb's
Law yields the Quasineutrality condition; the parallel and perpendicular
components of Ampere's Law provide the remaining two equations.

NOTE: This chapter uses the variable $g$ for the gyrokinetic
distribution function; in our standard convention, it should actually
be $h$.  I will change this when I get around to it.

%==============================================================================
\section{The Gyrokinetic Approximations and Ordering}
Two approximations are necessary to derive the gyrokinetic equations:
weak coupling and strong magnetization.

The weak coupling approximation, $g = n_e \lambda_{De} \gg 1$, where
$n_e$ is the electron number density and $\lambda_{De}$ is the
electron Debye length, means that a given charge interacts with many
electrons that fall within its Debye sphere. This is the standard
approximation in plasma physics that allows the use of Fokker-Planck
equation to describe plasma species.

For the discussion to follow, we specify standard definitions of
thermal velocity, cyclotron frequency, and Larmour radius of a plasma
species $s$ (in cgs units): $v_{th_s}^2 = 2 T_s/m_s$, $\Omega_s= q_s
B_0/(m_s c)$, and $\rho_s = v_{th_s}/\Omega_s$ where we have mass
$m_s$, charge $q_s$, temperature $T_s$ (absorbing Boltzmann's constant
to give temperature in units of energy), mean magnetic field strength
$B_0$, and speed of light $c$.

The condition of strong magnetization for gyrokinetics restricts the
scales of validity in space and time
\begin{equation}
\begin{array}{lcr}
\frac{\rho_i}{L} \ll 1& \mbox{                } & \omega \ll \Omega_i.
\end{array}
\end{equation}
where $L$ is the macroscopic length scale of the equilibrium plasma
and $\omega$ is the frequency of fluctuations described by the theory.

Gyrokinetics employs a formal ordering to manage the disparate length
and time scales typical of magnetized plasmas. Two length and three
time scales are specified: a large macroscopic length $L$, the small
ion larmour radius $\rho_i$, the fast time scale associated with the
ion cyclotron frequency $\Omega_i$, the intermediate time scale of the
fluctuation freqency $\omega \sim v_{th_i}/L$, and the slow time scale
of the transport rate $1/\tau$. A small dimensionless parameter is
defined by $\epsilon = \rho_i/L \ll 1$.  Splitting the distribution
function into an equilibrium and fluctuating component $f= F_0 +
\delta f$, we define the formal gyrokinetic ordering by
\begin{equation}
\frac{\rho_i}{L} \sim \frac{\omega}{\Omega_i} \sim \frac{\delta f}{F_0} 
\sim \frac{|\delta \V{B}|}{B_0} \sim \frac{|\delta \V{E}| c}{B_0 v_{th_i}} 
\sim \frac{k_\parallel}{k_\perp} \sim \epsilon \ll 1.
\label{eq:gk_ordering}
\end{equation}
Here $\delta \V{B}$ and $\delta \V{E}$ are the fluctuating components
of the magnetic and electric field and $k_\parallel$ and $k_\perp$ are
typical wavenumbers parallel and perpendicular to the mean magnetic
field $\V{B}_0$.  In this ordering, the equlibrium quantities $F_0$
and $\V{B}_0$ vary at the slow transport rate $1/\tau \sim \epsilon^3
\Omega_i$.  Note that fluctuations perpendicular to the magnetic field
are of the same order as the ion Larmour radius
\begin{equation}
k_\perp \rho_i \sim 1.
\end{equation}

%==============================================================================
\section{The Gyrokinetic Ordering}


%==============================================================================
\section{Definitions}

%=========================================
\subsection{General}
\begin{enumerate}
\item Cyclotron frequency: The cyclotron frequency (in cgs units) for 
a species $s$ in a equilibrium field of strength $B_0$ is given by
\begin{equation}
\Omega_s=\frac{ q_s B_0}{m_s c}
\label{eq:omdef}
\end{equation}

\item Thermal velocity: The thermal velocity of a particle species $s$ 
is defined by
\begin{equation}
v_{th_s}^2 = \frac{2 T_s}{m_s}
\label{eq:vthdef}
\end{equation}
\end{enumerate}

%=========================================
\subsection{Spatial Coordinates and Transformations}
\begin{enumerate}
\item Guiding Center Coordinates: The position $\V{r}$, guiding
center $\V{R}$, and gyroradius $\V{\rho}$ are related by
\begin{equation}
\V{r} = \V{R}+\V{\rho}
\end{equation}
 
\item Gyroradius: The gyroradius is related to the perpendicular 
velocity by
\begin{equation}
\V{\rho} = \frac{ \bhat \times \V{v}(\theta)}{\Omega}
\label{eq:rhodef}
\end{equation}
NOTE: The gyroradius is given by \eqref{eq:rhodef} for a positive charge
$q$.  If the charge is negative, there is a negative sign.

\item Particle velocity:
\begin{equation}
\V{v} = v_\parallel \zhat + v_\perp ( \cos \theta \xhat + \sin \theta \yhat)
\end{equation}

\end{enumerate}

%=========================================
\subsection{Velocity Coordinates and Transformations}
\begin{enumerate}
\item Velocity coordinates
\begin{tabular}{ccc}
$\E=\frac{v^2}{2}$ & $\mu=  \frac{v_\perp^2}{2 B_0}$ & 
$\theta = \tan^{-1} \left( \frac{v_y}{v_x} \right) $\\
\end{tabular}


\item 
The Jacobian for this transformation is
\begin{equation}
d^3\V{v} =\frac{d\E d\mu d\theta}{| \frac{\partial \E}{\partial \V{v}} \times
\frac{\partial \mu}{\partial \V{v}} 
\cdot \frac{\partial \theta}{\partial \V{v}} |}
= \frac{B_0 d\E d\mu d\theta}{|v_\parallel|}
\end{equation}

\item This can be seen using
\begin{equation}
\frac{\partial \E}{\partial \V{v}} = \V{v}
\end{equation}

\begin{equation}
\frac{\partial \mu}{\partial \V{v}} = \frac{\V{v}_\perp}{B_0}
\end{equation}
\begin{equation}
\frac{\partial \theta}{\partial \V{v}} =
\frac{ \zhat \times \V{v}_\perp}{\V{v}_\perp^2}
\end{equation}


\end{enumerate}
%=========================================
\subsection{Ring Averages}
\begin{enumerate}

\item Ring Average at constant guiding center $\V{R}$
\begin{equation}
\langle a(\V{r},\V{v},t)\rangle_\V{R} = 
\frac{1}{2 \pi} \oint d \theta a(\V{R}-\frac{\V{v} \times \zhat}{\Omega},\V{v},t)
\end{equation}

\item Ring Average at fixed position $\V{r}$
\begin{equation}
\langle a(\V{R},\E,\mu,\theta,t)\rangle_\V{r} = 
\frac{1}{2 \pi} \oint d \theta 
a(\V{r}+\frac{\V{v} \times \zhat}{\Omega},\E,\mu,\theta,t)
\end{equation}
Note here that 
\begin{equation}
\frac{\V{v}(\theta) \times \zhat}{\Omega}= - \rho(\theta)
\end{equation}

\end{enumerate}
%==============================================================================
\section{Maxwell's Equations}
\begin{enumerate}

\item The set of Maxwell's Equations (in cgs units) are
\begin{equation}
\nabla \cdot \delta \V{E} = 4 \pi \rho
\end{equation}
\begin{equation}
\frac{1}{c} \frac{\partial  \delta \V{E}}{\partial t} =
\nabla \times \delta \V{B} - \frac{ 4 \pi}{c} \delta \V{J} 
\end{equation}
\begin{equation}
\nabla \cdot \delta \V{B} = 0
\end{equation}
\begin{equation}
\frac{1}{c}\frac{\partial  \delta \V{B}}{\partial t}=
-\nabla \times \delta \V{E} 
\end{equation}
We'll look at each of these equations in the Gyrokinetic Approximation.

\item Faraday's Law:
\begin{equation}
\frac{1}{c} \frac{\partial  \delta \V{B}}{\partial t}= 
- \nabla \times \delta \V{E}
\end{equation}
We will use the standard potentials
\begin{equation}
 \delta \V{B}=  \nabla \times  \V{A}
\end{equation}
and 
\begin{equation}
 \delta \V{E}=  -\nabla \phi - \frac{1}{c}\frac{\partial   \V{A}}{\partial t}
\end{equation}

\item Ampere/Maxwell Law:
\begin{equation}
\nabla \times \delta \V{B} =  \frac{ 4 \pi}{c} \delta \V{J} 
+ \frac{1}{c} \frac{\partial  \delta \V{E}}{\partial t}
\end{equation}
Here we drop the displacement current under the approximations for
gyrokinetics.
This leaves us Ampere's Law:
\begin{equation}
\nabla \times \delta \V{B} = \frac{ 4 \pi}{c} \delta \V{J} 
\end{equation}

\item The Poisson equation from Coulomb's Law:
\begin{equation}
\nabla \cdot \delta \V{E} =  4 \pi \sum_s q_s n_s
\end{equation}
Under our approximations, the divergence is small, leaving us
the Quasineutrality Condition:
\begin{equation}
\sum_s q_s n_s=0
\end{equation}

\end{enumerate}
%==============================================================================
\section{Fokker-Planck Equation}
\begin{enumerate}
\item The Fokker-Planck Equation: For a species $\alpha$ with the 
distribution function $f_\alpha = f_\alpha(\V{r},\V{v},t)$, we have
\begin{equation}
\frac{\partial f_\alpha}{\partial t} 
+ \V{v} \cdot \frac{\partial f_\alpha}{\partial \V{r}}
+ \frac{q_\alpha}{m_\alpha}\left\{ \V{E} + \V{v} \times \V{B} \right\} 
\cdot \frac{\partial f_\alpha}{\partial \V{v}}
= \sum_\beta \C_{\alpha \beta}(f_\alpha,f_\beta)
\end{equation}

\item The Landau form of the Collision operator is
\begin{equation}
\C_{\alpha \beta}(f_\alpha,f_\beta) = 
- \frac{\partial \V{J}_{\alpha \beta}}{\partial \V{v}}
\end{equation}
where 
\begin{equation}
 \V{J}_{\alpha \beta} = 2 \pi \ln \Lambda_{\alpha \beta} 
\frac{ q^2_\alpha q^2_\beta}{m_\alpha} \int \frac{ d^3\V{v}'}{u^3}
\left( u^2 \mathcal{I} - \V{u}\V{u}\right) \cdot \left\{
\frac{ 1}{m_\alpha} f_\alpha(\V{v}) 
\frac{\partial f_\beta(\V{v}')}{\partial \V{v}'} -
\frac{ 1}{m_\beta} f_\beta(\V{v}') 
\frac{\partial f_\alpha(\V{v})}{\partial \V{v}} \right\}
\end{equation}



\item We begin with the Fokker-Planck equation and split the distribution
function $f$ into a slowly changing equilibrium solution $F_0$ and a
quickly changing behavior $\delta f$
\begin{equation}
f=F_0(\V{v},\tau) + \delta f (\V{r},\V{v},t,\tau)
\end{equation}
where the long time behavior $\tau=t/\epsilon^2$.


\end{enumerate}
%==============================================================================
\section{Moments of the Distribution Function}
\begin{enumerate}
\item Number Density $n_s$
\begin{equation}
n_s= \int d^3\V{v} f_s(\V{r},\V{v},t)
\label{eq:dist0}
\end{equation}
\item Current Density $\V{J}$
\begin{equation}
\V{J}= \sum_s \int d^3\V{v} q_s  \V{v} f_s(\V{r},\V{v},t)
\label{eq:dist1}
\end{equation}

\end{enumerate}
%==============================================================================

\section{Ordering the Equation}
\label{sec:gkordering}
%===========================================
\subsection{The Ordered Fokker-Planck Equation}
\begin{eqnarray}
\epsilon^2 \frac{\partial F_0}{\partial \tau} 
+ \frac{\partial \delta f}{\partial t}
+ \epsilon^2 \frac{\partial \delta f}{\partial \tau}
+ \V{v}_\perp \cdot \nabla \delta f
+ v_\parallel \zhat \cdot \nabla \delta f
+\frac{q}{m} \left( -\nabla \phi
- \frac{\partial \V{A}}{\partial t}
+ \V{v} \times \delta \V{B}
+ \V{v} \times \V{B}_0 \right) \cdot \frac{\partial F_0}{\partial \V{v}} \\
+\frac{q}{m} \left( -\nabla \phi
- \frac{\partial \V{A}}{\partial t}
+ \V{v} \times \delta \V{B}
+ \V{v} \times \V{B}_0 \right) \cdot \frac{\partial\delta f }{\partial \V{v}} 
= C(F_0,F_0) + C(\delta f ,F_0) + C(F_0,\delta f) + C(\delta f ,\delta f )  
\end{eqnarray}

\begin{equation}
\begin{array}{ccccc}
\epsilon^2 \partial F_0/\partial \tau  &
+ \partial \delta f/\partial t &
+ \epsilon^2 \partial \delta f/\partial \tau &
+ \V{v}_\perp \cdot \nabla \delta f &
+ v_\parallel \zhat \cdot \nabla \delta f \\
\epsilon^2  & \epsilon & \epsilon^3 & 1 & \epsilon \\
+\frac{q}{m} & 
\left( -\nabla \phi \right.&
- \partial \V{A}/\partial t&
+ \V{v} \times \delta \V{B}& 
+ \left. \V{v} \times \V{B}_0 \right) \cdot \partial F_0/\partial \V{v}\\
& 1 & \epsilon & 1 & 1/\epsilon \\
+\frac{q}{m} &
\left( -\nabla \phi \right.&
- \partial \V{A}/\partial t&
+ \V{v} \times \delta \V{B}&
+ \left. \V{v} \times \V{B}_0 \right) \cdot \partial\delta f/ \partial \V{v} \\
 & \epsilon & \epsilon^2 & \epsilon & 1 \\
=  &C(F_0,F_0) &
+ C(\delta f ,F_0) &
+ C(F_0,\delta f) &
+ C(\delta f ,\delta f )  \\
& 1 & \epsilon & \epsilon &\epsilon^2 
\end{array}
\end{equation}

%===========================================
\subsection{Order $\Order(1/\epsilon)$}
At lowest order we find 
\begin{equation}
\frac{\partial  F_0}{\partial \theta}=0,
\end{equation}
so the equilibirum distribution function does not depend on gyrophase,
\begin{equation}
 F_0= F_0(\E,\mu,\tau)
\end{equation}

%===========================================
\subsection{Order $\Order(1)$}
\begin{enumerate}
\item At first order we have the equation
\begin{equation}
\V{v}_\perp \cdot \nabla \delta f_1 + \frac{q}{m}\{ -\nabla \phi + 
\V{v} \times \delta \V{B}\} \cdot \frac{\partial F_0}{\partial \V{v}}
- \Omega_c \frac{\partial \delta f_1}{\partial \theta} = \C(F_0,F_0)
\label{eq:order1}
\end{equation}

\item From Boltzmann's H theorem, we can show that the equilibrium 
distribution function takes the form 
\begin{equation}
 F_0= \frac{n_0}{\pi^{3/2}} \frac{1}{v_{th}^3} \exp \left( -
 \frac{v^2}{v_{th}^2}
\label{eq:eqdist}
\right)
\end{equation}
where the mean density $n_0=n_0(\tau)$ evolves only on the long timescale.

\item Using this solution for $F_0$ and the fact that for an equilibirum 
state $\C(F_0,F_0)=0$, we can simplify \eqref{eq:order1} to yield
\begin{equation}
\V{v}_\perp \cdot \nabla \delta f_1 
- \Omega_c \frac{\partial \delta f_1}{\partial \theta} = -\V{v} \cdot
\nabla \left(\frac{q \phi}{T} \right) F_0
\end{equation}
This inhomogeneous equation supports a particular solution and 
a homogeneous solution.

\item The particular solution to this equation can
be picked from this equation to be
\begin{equation}
\delta f_p =  - \frac{q \phi}{T}  F_0
\end{equation}
 
\item We add a homogeneous solution $g$ to the solution for $\delta f_1$
to get a complete solution 
\begin{equation}
\delta f_1 =  - \frac{q \phi}{T}  F_0 +g
\end{equation}
where the homogeneous part of the solution
must satisfy the equation
\begin{equation}
\V{v}_\perp \cdot \frac{\partial g}{\partial \V{r}} 
- \Omega_c \frac{\partial g}{\partial \theta} = 0
\end{equation}
If we express $g$ in terms of the guiding center variables
$g=g(\V{R},\E,\mu,\theta,t,\tau)$, changing variables simplifies this
equation to
\begin{equation}
\left( \frac{\partial g}{\partial \theta} \right)_\V{R}= 0.
\end{equation}
This part of the solution is independent of gyrophase at 
constant guiding center $\V{R}$ (but not at constant position $\V{r}$).

\item The details of the coordinate transform follow. The relation between the 
position $\V{r}$ and guiding center $\V{R}$ is given by
\begin{equation}
\V{r} = \V{R} + \boldmath{\rho}
\end{equation}
where $\V{v} = v_\parallel \zhat + v_\perp (\cos \theta \xhat + \sin \theta \yhat)$
and $\boldmath{\rho}= - \V{v} \times \zhat$$ /\Omega = v_\perp /\Omega
(- \sin \theta \xhat + \cos \theta \yhat)$.  Thus, the relation between the components
is given by
\begin{eqnarray}
x &=& X -  (v_\perp /\Omega) \sin \theta  \nonumber\\
y &=& Y +  (v_\perp /\Omega) \cos \theta \\
z & = &Z \nonumber
\end{eqnarray}
The gyrokinetic distribution function $h$ is a function of the following variables:
$h = h( \V{r}(\V{R},\theta), v, v_\perp, \theta,t)$. Thus,
\begin{equation}
\left( \frac{\partial h }{\partial \theta} \right)_{\V{R}} = 
\left( \frac{\partial h }{\partial \theta} \right)_{\V{r}} +
\left( \frac{\partial h }{\partial \V{r}} \right)
\left( \frac{\partial \V{r} }{\partial \theta} \right)_{\V{R}} =
\left( \frac{\partial h }{\partial \theta} \right)_{\V{r}} +
\left( \frac{\partial h }{\partial x} \right)
\left( \frac{\partial x }{\partial \theta} \right)_{\V{R}} + 
\left( \frac{\partial h }{\partial y} \right)
\left( \frac{\partial y }{\partial \theta} \right)_{\V{R}} + 
\left( \frac{\partial h }{\partial z} \right)
\left( \frac{\partial z }{\partial \theta} \right)_{\V{R}} 
\end{equation}
Plugging in the partial derivatives 
$ \partial x/\partial \theta = -  (v_\perp /\Omega) \cos \theta $,
$ \partial y/\partial \theta = -  (v_\perp /\Omega) \sin \theta $, and
$ \partial z/\partial \theta = 0$,
we find
\begin{eqnarray}
\left( \frac{\partial h }{\partial \theta} \right)_{\V{R}} &=& 
\left( \frac{\partial h }{\partial \theta} \right)_{\V{r}} +
-  (v_\perp /\Omega) \cos \theta \left( \frac{\partial h }{\partial x} \right) + 
-  (v_\perp /\Omega) \sin \theta \left( \frac{\partial h }{\partial y} \right) \nonumber  \\
&=& \left( \frac{\partial h }{\partial \theta} \right)_{\V{r}} +
-  (v_\perp /\Omega) (\cos \theta \xhat +  \sin \theta \yhat) \cdot \nabla h =
\left( \frac{\partial h }{\partial \theta} \right)_{\V{r}} +
-  (\V{v}_\perp /\Omega) \cdot \nabla h 
\end{eqnarray}
Thus, we obtain the final result
\begin{equation}
-\Omega \left( \frac{\partial h }{\partial \theta} \right)_{\V{R}}
= \V{v}_\perp \cdot \nabla h - \Omega
\left( \frac{\partial h }{\partial \theta} \right)_{\V{r}} 
\end{equation}




\item We must go to the next order to learn how $g$ evolves on the medium
timescale.

\item NOTE: In our solution thus far, we can take the term $1-q \phi / T =
\exp (q \phi / T) + \Order(\epsilon^2)$ and combine the $\epsilon^2$ 
bit with the second order part of the solution $\delta f_2$.

\item Summary: We have found a solution of the form
\begin{equation}
f=F_0(\E,\tau) \exp (-\frac{q \phi(\V{r},t)}{T}) 
+ g(\V{R},\E,\mu,t,\tau) + \delta f_2 + \cdots
\label{eq:distfunc}
\end{equation}
The first term in the solution is the Boltzmann, or adiabatic, term;
the second term is the gyroaveraged distribution function that we want
to solve.

\end{enumerate}

%===========================================
\subsection{Order $\Order(\epsilon)$}
\begin{enumerate}
\item Here we go to second order to find an equation for $g$.  We will also need
to find equations for $\phi$, $A_\parallel$, and $\delta B_\parallel$.
\item At second order we have the equation
\begin{eqnarray}
\frac{\partial g}{\partial t} +\frac{\partial \V{R}}{\partial t} \cdot
\frac{\partial g}{\partial \V{R}} + \frac{q}{m}\{ -\nabla \phi + 
\V{v} \times \delta \V{B}\} \cdot \left\{ \V{v} \frac{\partial g}{\partial \E} 
+\frac{ \V{v}_\perp}{B_0} \frac{\partial g}{\partial \mu} \right\}
-\C_L(g,F_0) = \nonumber \\
\Omega_c \left(\frac{\partial \delta f_2}{\partial \theta} \right)_{\V{R}}
+ \frac{q}{T} \left( \frac{\partial \phi}{\partial t} -\V{v} \cdot
\frac{\partial \V{A}}{\partial t} \right) F_0 + \Order{\epsilon^2} + \cdots
\label{eq:order2}
\end{eqnarray}

\item We need to eliminate $\delta f_2$ from this equation; we accomplish this by
gyroaveraging over $\theta$ at fixed guiding center position $\V{R}$.
We eliminate it using the fact that $\delta f_2$ must be periodic in
$\theta$.
\begin{equation}
\int^{2 \pi}_{0} d \theta  \frac{\partial \delta f_2}{\partial \theta}=0
\end{equation}

\item After this ring average, we are left with
\begin{equation}
\frac{\partial g}{\partial t} 
+\left\langle \frac{\partial \V{R}}{\partial t} \right\rangle_\V{R} \cdot
\frac{\partial g}{\partial \V{R}} 
- \left\langle\C_L(g,F_0)\right\rangle_\V{R} = 
 \frac{q}{T} \frac{\partial \langle \chi \rangle_\V{R}}{\partial t} F_0 
\end{equation}
where
\begin{equation}
\chi=\phi-\frac{\V{v}\cdot\V{A}}{c}
\label{eq:chidef}
\end{equation}

\item A little more simplification yields the Gyrokinetic Equation
\begin{equation}
\frac{\partial g}{\partial t} 
+v_\parallel  \cdot \zhat \frac{\partial g}{\partial \V{R}} 
+ \frac{1}{B_0} \left[ \langle \chi \rangle ,g \right]
- \left\langle\C_L(g,F_0)\right\rangle = 
 \frac{q}{T} \frac{\partial \langle \chi \rangle}{\partial t} F_0 
\label{eq:gkequation}
\end{equation}
Here the Poisson bracket, which is the nonlinear term, is defined by
\begin{equation}
\left[ \langle \chi \rangle_{\V{R}} ,g \right]= 
\left(\frac{\partial \langle \chi \rangle_{\V{R}} }{\partial \V{R}} \times
\frac{\partial g}{\partial \V{R}} \right) \cdot \zhat =
\frac{\partial \langle \chi \rangle_{\V{R}} }{\partial x} 
\frac{\partial g}{\partial y} - 
\frac{\partial \langle \chi \rangle_{\V{R}} }{\partial y} 
\frac{\partial g}{\partial x} 
\label{eq:poisbrack}
\end{equation}

\end{enumerate}

%==============================================================================
\section{Maxwell's Equations in Gyrokinetics}
\begin{enumerate}

\item Ampere's Law:
\begin{equation}
\nabla \times \delta \V{B} = \frac{ 4 \pi}{c} \delta \V{J} 
\end{equation}

\item Quasineutrality
\begin{equation}
\rho=0
\end{equation}

\item Potentials
\begin{equation}
 \delta \V{B}=  \nabla \times  \V{A}
\end{equation}
and 
\begin{equation}
 \delta \V{E}=  -\nabla \phi - \frac{1}{c}\frac{\partial   \V{A}}{\partial t}
\end{equation}

\item We choose the Coulomb Gauge
\begin{equation}
\nabla \cdot \V{A}=0
\end{equation}
Thus, we can decompose the current into
\begin{equation}
\nabla \times \delta \V{B} = - \nabla^2 A_\parallel \zhat 
+ \nabla \delta B_\parallel \times \zhat
\label{eq:jdecomp}
\end{equation}

This is true in the gyrokinetic limit, since, for the case Fourier components
$\V{k}= k_\parallel \zhat + k_\perp \xhat$, 
\begin{equation}
\V{J}=  \nabla \times \V{B} = 
(k_\perp^2 A_\parallel  -  k_\parallel k_\perp A_{\perp_x} ) \zhat +
(k_\parallel^2 A_{\perp_x} - k_\parallel k_\perp A_\parallel) \xhat +
(k_\perp^2 A_{\perp_y} + k_\parallel^2 A_{\perp_y}) \yhat
\end{equation}
In the gyrokinetic limit, $k_\parallel \ll k_\perp$, so we can
drop the terms with $k_\parallel$ to yield
\begin{equation}
\V{J}=  \nabla \times \V{B} \simeq
k_\perp^2 A_\parallel  \zhat +
k_\perp^2 A_{\perp_y}  \yhat
\end{equation}
We know that 
\begin{equation}
B_\parallel = \zhat \cdot \nabla \times \V{A} 
= i(\V{k}_\perp \times \V{A}_{\perp}) \cdot \zhat = 
i k_\perp A_{\perp_y},
\end{equation}
 so the relation for current can be expressed by \eqref{eq:jdecomp}

\item Discussion of potentials in gyrokinetics, or how we reduce to
just three free independent variables for the fields (from six).

We specify the potentials 
\begin{equation}
 \delta \V{B}=  \nabla \times  \V{A}
\end{equation}
and 
\begin{equation}
 \delta \V{E}=  -\nabla \phi - \frac{1}{c}\frac{\partial   \V{A}}{\partial t}
\end{equation}
and  choose the Coulomb Gauge
\begin{equation}
\nabla \cdot \V{A}=0.
\end{equation}
The gyrokinetic ordering means that, to lowest order, $\nabla_\perp
\cdot \V{A}_\perp=0$, so we can write the perpendicular component of the vector potential 
 as the curl of a scalar $\V{A}_\perp= \nabla \times \xi \zhat =
 \nabla \xi \times \zhat$.
Thus, the vector potential depends on only two independent components
 $A_\parallel$ and $\xi$,
\begin{equation}
\V{A}=A_\parallel \zhat + \nabla \xi \times \zhat
\end{equation}

Solving for the magnetic field gives
\begin{equation}
 \delta \V{B}= \nabla A_\parallel  \times \zhat + \nabla (\nabla \cdot \xi \zhat)
- \nabla^2 \xi \zhat
\end{equation}
For the case that $\V{k}=k_\perp \xhat + k_\parallel \zhat$, we can
Fourier transform the equation and solve for the magnetic field to find
\begin{equation}
\delta  \hat{\V{B}}= - k_\perp k_\parallel \xi \xhat - i k_\perp \hat{A}_\parallel \yhat
+ k_\perp^2 \xi \zhat
\end{equation}
Choosing to express our potentials in terms of $\delta
\hat{B}_\parallel$ instead of $\xi$, we substitute
\begin{equation}
\xi = \frac{\delta \hat{\V{B}}_\parallel}{k_\perp^2}
\end{equation}

Hence, the vector potential is expressed as 
\begin{equation}
\hat{\V{A}}=\hat{A}_\parallel \zhat 
- i  \frac{\delta \hat{\V{B}}_\parallel}{k_\perp} \yhat.
\end{equation}
Hence, in the gyrokinetic ordering, the electric and magnetic fields
are then given by
\begin{equation}
 \delta \hat{\V{E}}= -i k_\perp \hat{\phi} \xhat 
+ \frac{\omega \delta \hat{\V{B}}_\parallel}{k_\perp c } \yhat
-i k_\parallel \left( \hat{\phi} 
- \frac{\omega \hat{A}_\parallel}{k_\parallel c} \right) \zhat
\end{equation}
and 
\begin{equation}
 \delta \hat{\V{B}}=-\frac{k_\parallel}{k_\perp} \delta \hat{\V{B}}_\parallel \xhat
-i k_\perp \hat{A}_\parallel \yhat
+   \delta \hat{\V{B}}_\parallel \zhat
\end{equation}


\end{enumerate}

%===========================================
\subsection{Quasineutrality}
\begin{enumerate}
\item Quasineutrality
\begin{equation}
n_{0i} q = n_{0e} e
\end{equation}
or
\begin{equation}
\sum_s n_s q_s=0
\end{equation}

\item Since the number density is found by integrating the distribution 
function over velocity, we can find this by \eqref{eq:dist0} using 
the distribution function from \eqref{eq:distfunc}.  
The integration over velocity of the Boltzmann term becomes
\begin{equation}
q_s \int d^3\V{v} F_{0s}(\E,\tau) e^{-q_s \phi(\V{r},t)/T_s} =
q_s n_s e^{-q_s \phi(\V{r},t)/T_s}
\end{equation}
Expanding the exponential and dropping terms higher then order
$\Order(\epsilon)$ , this terms yields
\begin{equation}
\sum_s q_s n_s \left(1-\frac{q_s \phi(\V{r},t)}{T_s}\right)=
\sum_s \frac{q_s^2 n_s  }{T_s}\phi(\V{r},t)
\end{equation}
since the equilibrium must satisfy neutrality $\sum_s q_s n_s= 0$

\item At order $\Order(\epsilon)$ we have
\begin{eqnarray}
\sum_s - \frac{q_s^2 n_s}{T_{s}} \phi +  q_s
\int d^3\V{v} g_s(\V{r}+ \frac{\V{v} \times \zhat}{\Omega_s},\E,\mu,t) 
 = 0 
\end{eqnarray}

 \item Performing a ring average at fixed $\V{r}$ , this becomes
\begin{equation}
\sum_s \left( - \frac{q_s^2 n_s}{T_{s}} \phi + q_s \int d^3 \V{v}
 \langle g_s \rangle_\V{r} \right)= 0 
\end{equation}
where 
\begin{equation}
\int d^3 \V{v} = 
\int_0^\infty v_\perp dv_\perp \int_{-\infty}^{\infty} dv_\parallel 
\int_0^{2 \pi}  d \theta
\end{equation}


\end{enumerate}

%===========================================
\subsection{Parallel Ampere's Law}
\begin{enumerate}
\item Here we take the parallel component of Ampere's Law
\begin{equation}
\zhat \cdot \left(\nabla \times \delta \V{B}\right) =  
\frac{ 4 \pi}{c} \delta \V{J}_\parallel
\end{equation}

\item The current can be found by taking a moment of the 
distribution function \eqref{eq:distfunc} according to
\eqref{eq:dist1} and integrating the over velocity.
For the parallel component of the current, the contribution from the
Boltzmann part of the distribution function vanishes upon
integrating over $v_\parallel$.  

\item After performing a ring average at fixed $\V{r}$ and
using \eqref{eq:jdecomp}, the parallel component of Ampere's Law
yields the equation
\begin{equation}
-\nabla_\perp^2 A_\parallel = \frac{ 4 \pi}{c} \delta J_\parallel  
= \sum_s \frac{ 4 \pi}{c} q_s
\int d^3 \V{v}  v_\parallel \langle g_s \rangle_\V{r} 
\end{equation}

\end{enumerate}

%===========================================
\subsection{Perpendicular Ampere's Law}
\begin{enumerate}
\item Here we take the perpendicular component of Ampere's Law
\begin{equation}
\zhat \times \left(\nabla \times \delta \V{B} \right) = 
\frac{ 4 \pi}{c} \zhat \times \delta \V{J}
\end{equation}

\item Again,the current is found by taking a moment of the 
distribution function \eqref{eq:distfunc} according to
\eqref{eq:dist1} and integrating the over velocity.
For the perpendicular component of the current, the contribution from the
Boltzmann part of the distribution function vanishes upon
integrating over $\theta$.  

\item  After performing a ring average at fixed $\V{r}$ and
using \eqref{eq:jdecomp}, the perpendicular component of Ampere's Law
yields the equation
\begin{equation}
\nabla_\perp \delta B_\parallel = \frac{ 4 \pi}{c} \zhat \times \delta \V{J}
= \sum_s \frac{ 4 \pi}{c} q_s
\int d^3 \V{v} \langle\zhat \times \V{v}_\perp g_s \rangle_\V{r} 
\end{equation}

\item This can be written in a more physical way as a pressure balance in 
the direction perpendicular to the field
\begin{equation}
\nabla_\perp \cdot \left[ \delta B_\parallel B_0 \mathcal{I} + 
\delta \mathcal{P}_\perp \right] = 0
\end{equation}
where $\mathcal{I}$ is the identity matrix and $\delta
\mathcal{P}_\perp$ is the ring averaged perturbed perpendicular
pressure tensor.  Rewriting this, we find
\begin{equation}
\nabla \delta B_\parallel B_0 = -\frac{ 4 \pi}{c} 
\nabla \cdot \delta \mathcal{P}_\perp = 
\sum_s \frac{ 4 \pi}{c}  \int d^3 \V{v} 
\langle m_s(\V{v}_\perp\V{v}_\perp) g_s(\V{R},\E,mu,t) \rangle_\V{r}
\end{equation}

\end{enumerate}

%==============================================================================
\newpage
\section{The Driven Gyrokinetic Equation}
To see the changes in the gyrokinetic equation when it is driven with 
an external antenna, we first must look at the driven Fokker-Planck Equation.

%==============================================================================
\subsection{The Driven Fokker-Planck Equation}
We begin with the full Fokker-Planck equation including a source on the 
right hand side due to an external driving antenna that creates a
driving electric field $-\V{E}_a$ (the negative sign is just so that the
total electric field, plasma plus antenna, ends up as $\V{E}+\V{E}_a$)
\begin{equation}
\frac{d f_s}{dt}= \frac{\partial f_s}{\partial t} + {\bf v} \cdot \nabla f_s 
+ \frac{q_s}{m_s} \left({\bf E}+ 
\frac{{\bf v} \times {\bf B}}{c} \right) \cdot 
\frac{\partial f_s}{\partial{\bf v}} = \sum_r C_{sr}(f_s,f_r) + C_{ss}(f_s,f_s) 
-\frac{q_s}{m_s}{\bf E}_a \cdot \frac{\partial f_s}{\partial{\bf v}}
\end{equation}
Note that if we take the energy moment of this equation by multiplying
by $\frac{1}{2} m_s v^2$, integrating over all space and velocity, and
simplifying according to \secref{sec:fpenergy}, we obtain
\begin{equation}
\frac{3}{2} n_{0s} \frac{d  T_{0s}}{d t} +
\frac{\partial }{\partial t}\int \frac{d^3\V{r}}{V} \int d^3\V{v} 
\frac{1}{2}m_s v^2 \delta f_{2s} 
= \int \frac{d^3\V{r}}{V} \V{J} \cdot (\V{E}+ \V{E}_a) +
\int \frac{d^3\V{r}}{V} \int d^3\V{v} \frac{1}{2} m_s v^2 C_{sr}(f_s,f_r).
\end{equation}
%==============================================================================
\subsection{Ordering the Driven Gyrokinetic Equation}
Folowing the method of \secref{sec:gkordering}, we will note here the
additions to the derivation of the gyrokinetic equation created by
including the driving term.  Splitting the driving antenna electric
field into antenna potentials, $\V{E}_a=-\nabla \phi_a
-\frac{1}{c}\frac{\partial \V{A}_a}{\partial t}$, and splitting the 
distribution function into its equilibrium and perturbed quantities, 
the right hand side of the gains the additional terms
\begin{equation}
\begin{array}{cccccccc}
\frac{q_s}{m_sc} (& c\nabla \phi_a &
+ \partial \V{A}_a/\partial t
 ) & \cdot \partial F_{0s}/\partial \V{v}
&+\frac{q_s}{m_s c} (& c \nabla \phi_a &
+ \partial \V{A}_a/\partial t)&
 \cdot \partial \delta f_s/\partial \V{v}\\
&1 & \epsilon& & & \epsilon & \epsilon^2 &
\end{array}
\end{equation}
Now we will look at each of the orders of the equation and find the differences.
%===========================================
\subsubsection{Order $\Order(1/\epsilon)$}
At lowest order there is no change from the usual derivation.

%===========================================
\subsubsection{Order $\Order(1)$}
At first order, the equation for the first order perturbation can be found to be
\begin{equation}
\V{v}_\perp \cdot \nabla \delta f_1 
- \Omega_c \frac{\partial \delta f_1}{\partial \theta} = -\V{v} \cdot
\nabla \left(\frac{q \phi}{T} \right) F_0 -\V{v} \cdot
\nabla \left(\frac{q \phi_a}{T} \right) F_0
\end{equation}
which has the particular solution
\begin{equation}
\delta f_p =  - \frac{q (\phi+\phi_a)}{T}  F_0.
\end{equation}
Hence, we eventually end up with a solution of the form
\begin{equation}
f_s=F_{0s}(\E,\tau) \exp (-\frac{q_s (\phi+\phi_a)}{T_s}) 
+ h_s(\V{R}_s,\E,\mu,t,\tau) + \delta f_{2s} + \cdots
\end{equation}


%===========================================
\subsubsection{Order $\Order(\epsilon)$}
At second order, we find we must replace all instances of $\phi$ by 
$\phi+\phi_a$; we also must change only the instances of $\V{A}$
that are multiplied by $F_{0s}$ to  $\V{A}+ \V{A}_a$.  The resulting 
driven gyrokinetic equation for $h_s$ becomes
\begin{equation}
\frac{\partial h_s}{\partial t} 
+v_\parallel  \frac{\partial h_s}{\partial z} 
+ \frac{c}{B} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right]
+\frac{c}{B} \left[ \langle \phi_a \rangle_{\V{R}_s} ,h_s \right]
-\left\langle \left(\frac{\partial h_s}{\partial t} \right)_{\rm
coll}\right\rangle_{\V{R}_s}= 
\frac{q_s F_{0s}}{T_s} \frac{\partial \langle \chi \rangle_{\V{R}_s}}{\partial t}
+\frac{q_s F_{0s}}{T_s} 
\frac{\partial \langle \phi_a - \V{v} \cdot \V{A}_a /c \rangle_{\V{R}_s}}{\partial t}
\end{equation}


%==============================================================================
\subsection{Driven \Alfven Wave Antenna}
In our case, we drive with an antenna whose only nonzero component is
$A_{\parallel a}$, so the effective gyrokinetic equation that we need is
\begin{equation}
\frac{\partial h_s}{\partial t} 
+v_\parallel  \frac{\partial h_s}{\partial z} 
+ \frac{c}{B} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right]
-\left\langle \left(\frac{\partial h_s}{\partial t} \right)_{\rm
coll}\right\rangle_{\V{R}_s}= 
\frac{q_s F_{0s}}{T_s} \frac{\partial \langle \chi \rangle_{\V{R}_s}}{\partial t}
-\frac{q_s F_{0s}}{T_s} \frac{\partial 
\langle v_\parallel A_{\parallel a} /c \rangle_{\V{R}_s}}{\partial t}
\label{eq:gk_driven}
\end{equation}
%==============================================================================
\subsection{The Entropy Equation from the Driven Gyrokinetic Equation}
We multiply the driven gyrokinetic equation \eqref{eq:gk_driven} by
$T_{0s}h_s/F_{0s}$ and integrate over all space and velocity at constant
guiding center $\V{R}$ to obtain the {\bf driven entropy equation}
\begin{equation}
\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
q \frac{\partial \left <\chi\right >}{\partial t} h_s - 
\frac{d}{dt}\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{0s}}{2F_{0s}}h^2_{s} 
+ \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{0s}}{F_{0s}}\left\langle h_sC(h_s) \right\rangle_{\V{R}_s}
- \int\frac{d^{3}{\bf r}}{V} J_{\parallel s} E_{\parallel a} =0.
\label{eq:ent_driven}
\end{equation}
where we have manipulated the driving antenna term by 
\begin{equation}
\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} q_s h_s \frac{\partial 
\langle v_\parallel A_{\parallel a} /c \rangle_{\V{R}_s}}{\partial t}
=\int\frac{d^{3}{\bf r}}{V}\frac{1}{c}
\frac{\partial A_{\parallel a}}{\partial t} 
\int d^{3}{\bf v}q_s v_\parallel  \langle h_s \rangle_\V{r}= 
\int\frac{d^{3}{\bf r}}{V} J_{\parallel s} E_{\parallel a} 
\end{equation}
and used the definition of the species parallel current $J_{\parallel s}=
\int d^{3}{\bf v}q_s v_\parallel  \langle h_s \rangle_\V{r}$.