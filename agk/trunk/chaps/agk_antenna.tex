%==============================================================================
% AstroGK Antenna
%==============================================================================
\chapter{Antenna}
%==============================================================================

%==============================================================================
\section{Driving Antenna}
\label{sec:antenna}
The main parameters for the antenna are \T{amplitude} and frequency
\T{w\_antenna}.  Setting up the antenna for a single driving wavenumber $\V{k}$ 
and for traveling waves (the default is \T{travel=.true.}), then given
\T{amplitude}$=A$ and \T{w\_antenna}$=\omega_0$, we have
\begin{equation}
\T{a\_ant} = \frac{A}{2}(1+i) e^{-i \omega_0 t}
\end{equation}
\begin{equation}
\T{b\_ant} =0.
\end{equation}
NOTE: The actual equation in the code is $a_{n+1}=a_{n} e^{-i \omega_0
\Delta t}$, which is seen to simply be $a=A e^{-i \omega_0 t}$ where we
take $a_n=A  e^{-i \omega_0 t_n}$, so for $t_{n+1}=t_n+\Delta t$
\begin{equation}
a_{n+1}=A e^{-i \omega_0 t_{n+1}} = A e^{-i \omega_0 t_n} e^{-i \omega_0
\Delta t} = a_ne^{-i \omega_0 \Delta t} .
\end{equation}

Now, the driving potential for real driving frequency is given by
\begin{equation}
A_{\parallel a} = \frac{\T{a\_ant}+\T{b\_ant}}{\sqrt{2}} e^{i k_\parallel z}
\end{equation}
and so for \T{a\_ant} and \T{b\_ant} above we find
\begin{equation}
A_{\parallel a} = \frac{A}{2}\frac{(1+i)}{\sqrt{2}} e^{i (k_\parallel
z- \omega_0 t)}
\end{equation}
The routine \T{get\_volume\_average} puts in a factor of 1/2 for all
$k_y \neq 0$ modes, so the variable \T{apar2} (output for
\T{write\_flux\_line=.true.}) becomes
\begin{equation}
\T{apar2} = \frac{|A_{\parallel a}|^2}{2} = \frac{A^2}{8}.
\end{equation}
Hence, in the magnitude of $A_\parallel$ we find a factor of
$\sqrt{8}$ difference between the analytical theory of
\secref{sec:laplacebpar0} and the output file from \T{GS2}.
%==============================================================================
\subsection{Amplitudes for Driving Strong Turbulence}
According to the hypothesis of critical balance, the nonlinear energy
cascade rate, which can be equated with the perpendicular eddy
turnaround rate $\gamma_{nl}=k_\perp v_\perp$, balances with the
linear wave frequency $\omega=k_\parallel v_A$ to give
\begin{equation}
k_\perp v_\perp \sim \omega
\label{eq:critbal}
\end{equation}
The reduced MHD limit of gyrokinetics, the perpendicular fluid velocity is
given by
\begin{equation}
\V{u}_\perp = \frac{c}{B_0} \zhat \times \nabla \phi
\end{equation}
and the perpendicular magnetic field perturbation is given by
\begin{equation}
\V{B}_\perp = - \zhat \times \nabla A_\parallel.
\end{equation}
To find the amplitude of $A_\parallel$ that corresponds to the
critical balance of the Goldreich-Sridhar strong turbulent cascade,
we note that in MHD $v_\perp \sim B_\perp/(4 \pi n_i m_i)^{1/2}$, so
we get
\begin{equation}
\V{u}_\perp \sim \frac{\omega}{k_\perp} 
\sim \frac{k_\perp A_\parallel}{\sqrt{4 \pi n_i m_i}}.
\end{equation}
Hence, the resulting amplitude is
\begin{equation}
A_\parallel \sim \frac{\omega}{k_\perp^2}\sqrt{4 \pi n_i m_i}.
\end{equation}
Normalizing this relation to dimensionless code units, this becomes
\begin{equation}
\frac{v_{t0}}{c} \frac{q_0 A_\parallel}{T_0} \frac{a_0}{\rho_0}
 \sim 2 \left(\frac{\omega}{k_\parallel v_A} \right) \frac{(k_\parallel \rho_0)}
{(k_\perp \rho_0)^2} \left(\frac{\sqrt{4 \pi n_i m_i}}{B_0} \right)
 \left(\frac{ q_0 B_0}{m_0 c} \right)  \left(\frac{ m_0}{2T_0} \right) 
\frac{v_A v_{t0} \rho_0^2}{\rho_0}
\end{equation}
which becomes
\begin{equation}
\hat{A}_\parallel \sim 2 \frac{\overline{\omega} \hat{k}_\parallel}{\hat{k}_\perp^2}
\end{equation} 
This is the steady-state amplitude of $A_\parallel$ necessary to be in
critical balance.

We now need to connect this steady state amplitude to the driving
amplitude $A_0$ in the \T{GS2} input file.  If the loss of energy in
the driving mode is given purely by the linear damping, a
Laplace-Fourier solution to the linear system tells us that the saturation
amplitude for $A_\parallel$ in the long time limit is given by
\begin{equation}
|A_\parallel(t\rightarrow \infty)| =\frac{A_{\parallel 0}}{2 D'(\overline{\omega}_0)} 
\end{equation}
where $A_0$ and $\omega_0$ are the amplitude and frequency of the
driving antenna and the dispersion relation $D'(\overline{\omega}_0)$ is 
given by
\begin{equation}
D'(\overline{\omega})= \frac{\overline{\omega}^2}{\alpha_i A} \left[
B(A-B) + \frac{(AE +BC)^2}{2A/\beta_i -AD+C^2} \right] -1.
\end{equation}
However, this neglects the drain of energy from the driving mode (or
modes) through nonlinear transfer of energy to other wave modes.  We
can roughly include this transfer of energy by choosing a total energy
loss rate for a given mode $\gamma_{tot}=\gamma +\gamma_{nl}$ where
$\gamma$ is the linear collisionless damping rate and $\gamma_{nl}$ is the
nonlinear rate of energy transfer to other modes.  A Fourier solution for the 
saturation amplitude in steady state is given by
\begin{equation}
|A_\parallel(t\rightarrow \infty)|^2 =\frac{A_{\parallel 0}^2}
{(\overline{\omega}-\overline{\omega}_0)^2 +\overline{\gamma_{tot}}^2}
\end{equation}
For almost any system in which we have interest, at the wavenumbers
where we drive $\gamma_{nl}\gg \gamma$, so we can neglect the linear
damping.  Since the critical balance suggests the nonlinear transfer rate
$\gamma_{nl} \sim \omega$, we find
\begin{equation}
|A_\parallel(t\rightarrow \infty)|^2 \simeq \frac{A_{\parallel 0}^2}
{(\overline{\omega}-\overline{\omega}_0)^2 +\overline{\omega}^2}.
\end{equation}
If we drive fairly near the resonant frequency such that 
$|\overline{\omega}-\overline{\omega}_0| \le |\overline{\omega}|$, we can 
neglect the driving term; we arrive at last at the result for the 
saturated amplitude in a strongly turbulence nonlinear simulation
\begin{equation}
|A_\parallel(t\rightarrow \infty)| \simeq \frac{A_{\parallel 0}}
{\overline{\omega}}.
\end{equation}
Note, however, that in reality if the decorrelation of the driving
frequnecy is of the same order as the driving frequency, then
$|\overline{\omega}-\overline{\omega}_0| \simeq |\overline{\omega}|$,
so a slightly more accurate solution may be $|A_\parallel(t\rightarrow
\infty)| \simeq A_{\parallel 0}/ ( \sqrt{2}\overline{\omega})$.

When the driver is not decorrellated, if travelling waves are
specified, the effective amplitude of each mode is actually
$A_{\parallel 0}/2$ (see \secref{sec:antenna} above for details); if
$|\Im (\omega_0)|>0$, in other words if the driving antenna has a
non-zero decorellation time, then the amplitude is $A_{\parallel 0}$.
Additionally, taking into account that we stir with $N_{stir}$ driving
modes, the total energy is increased by $N_{stir}$, and thus the
saturation amplitude is increased by $\sqrt{N_{stir}}$. This modifies
the formula above to
\begin{equation}
|A_\parallel(t\rightarrow \infty)| \simeq \frac{A_{\parallel 0} \sqrt{N_{stir}}}
{\overline{\omega}}.
\end{equation}

All that remains now is to connect the driving amplitude in the
\T{GS2} input file to the desired final saturated amplitude. Finally, then, 
the solution for the necessary driving amplitude to be in critical
balance for strong turbulence is given by
\begin{equation}
A_{\parallel 0} \sim \frac{2 \overline{\omega}^2 \hat{k}_\parallel}
{\sqrt{N_{stir}} \hat{k}_\perp^2}
\end{equation}

%==============================================================================
\newpage
\subsection{Amplitudes for Driving Strong Turbulence (NEW)}
From the cascade model, we have $\omega=\omega_{nl}=C_2 k_\perp
v_\perp=C_2 k_\perp \alpha \delta B_\perp/\sqrt{4 \pi n_i m_i}$.
Since $\delta B_\perp = k_\perp A_\parallel$, we end up with
\begin{equation}
A_{\parallel } = \frac{ \omega \sqrt{4 \pi n_i m_i}}{k_\perp^2 C_2 \alpha(k_\perp) }
\end{equation}
Normalizing this equation, we get
\begin{equation}
\frac{v_{t0}}{c} \frac{q_0 A_\parallel}{T_0} \frac{a_0}{\rho_0} =
\left(\frac{\omega}{k_\parallel v_A} \right)
\frac{(k_\parallel a_0)}{(k_\perp \rho_0)^2} 
\left(\frac{ q_0 B_0}{m_0 c} \right)  \left(\frac{ m_0}{2T_0} \right) 
\frac{ 2 \rho_0 v_{t0}}{C_2 \alpha(k_\perp)}
\end{equation}
giving
\begin{equation}
\hat{A}_\parallel =  \frac{2 \overline{\omega} \hat{k}_\parallel}{\hat{k}_\perp^2C_2 \alpha (k_\perp) }
\end{equation}
This is the steady state amplitude of $\hat{A}_\parallel$ necessary to
satisfy critical balance for the normalized wavenumbers
$\hat{k}_\parallel$ and $\hat{k}_\perp$.

Now, we must determine the antenna driving amplitude
$\hat{A}_{\parallel 0}$necessary to achieve this steady state
amplitude $\hat{A}_\parallel$. The Fourier solution for the 
saturation amplitude in steady state is given by
\begin{equation}
|\hat{A}_\parallel(t\rightarrow \infty)|^2 =\frac{\hat{A}_{\parallel 0}^2}
{(\overline{\omega}-\overline{\omega}_0)^2 +\overline{\gamma_{tot}}^2}
\end{equation}
where the damping term includes not only the linear damping but also
the nonlinear energy transfer, $\overline{\gamma}_{tot}=
\overline{gamma}+\overline{\omega}_{nl}$. For the driving wavenumber,
the dissipation is generally weak, $\overline{gamma} \ll
\overline{\omega}_{nl}$; as well, in critical balance we have
$\overline{\omega}=\overline{\omega}_{nl}$, so this becomes
\begin{equation}
|\hat{A}_\parallel(t\rightarrow \infty)|^2 =\frac{\hat{A}_{\parallel 0}^2}
{(\overline{\omega}_{nl}-\overline{\omega}_0)^2 +\overline{\omega}_{nl}^2}
\end{equation}
For driving at resonance,
$\overline{\omega}_0=\overline{\omega}=\overline{\omega}_{nl}$ and the
denominator reduces to $\overline{\omega}_{nl}^2$; on the other hand,
because of the decorrelation of the antenna, the driving frequency
drifts such that $(\overline{\omega}_{nl}-\overline{\omega}_0) \simeq
\overline{\omega}_{nl}^2$, and therefore the denominator will be 
$2\overline{\omega}_{nl}^2$.  To cover both of these limits, we introduce a 
parameter $\delta \in [1,2]$, and we have the result\begin{equation}
|\hat{A}_\parallel(t\rightarrow \infty)|^2 =\frac{\hat{A}_{\parallel 0}^2}
{\delta \overline{\omega}_{nl}^2}.
\end{equation}

Taking into account that we stir with $N_{stir}$ driving modes, the
total energy is increased by $N_{stir}$, and thus the saturation
amplitude is increased by $\sqrt{N_{stir}}$. Hence we find a final
result connecting the driving amplitude of the antenna $\hat{A}_{\parallel 0}$
to the saturation amplitude $\hat{A}_\parallel$
\begin{equation}
|\hat{A}_\parallel(t\rightarrow \infty)| =
\frac{\hat{A}_{\parallel 0} \sqrt{N_{stir}}} {\delta^{1/2}\overline{\omega}}.
\end{equation}

Now we set these two values equal to determine the correct driving
amplitude in order to be in critical balance. This gives
\begin{equation}
\hat{A}_{\parallel 0} \sim \frac{2  \hat{k}_\parallel \overline{\omega}^2}
{\hat{k}_\perp^2 \sqrt{N_{stir}} } \frac{\delta^{1/2}}{C_2 \alpha(k_\perp)}
\end{equation}

