%==============================================================================
% Hyperdiffusivity in Gyrokinetics
%==============================================================================
\chapter{Hyperdiffusivity in Gyrokinetics}

%==============================================================================
\section{The Need for Non-Physical Damping in Nonlinear Runs}
Nonlinear simulations of the cascade of turbulent energy in
collisionless magnetized plasmas require the addition of a
non-physical damping term to dissipated at the smallest scales in the
system, thereby avoiding a build-up of energy, or bottleneck, in the
smallest scale modes in the simulation domain.  This is clear even in
runs at high $\beta_i$, where the damping on ions is very strong at
$k_\perp \rho_i \sim 1$, as shown in Figure~\ref{fig:trans2_3}.  Note here
that the hyperdiffusive damping was much too strong in run \T{trans3}.

\begin{figure}
\hbox to \hsize{ \hfill \epsfxsize16cm \epsffile{figs/trans2_3_kspec.ps} \hfill}
\caption{Plot of two energy spectra vs. wavenumber for two runs \
with $\beta_i=100$, \T{trans2} (blue, without hyperdiffusivity) and \T{trans3}
(red, with hyperdiffusivity).  Different components of the fluctuating
energy $\delta W$ are ploteed: total energy (solid), $\delta f_i$
(dotted), $E_{A_\|}$ (short-dashed), $E_{\delta B_\|}$ (long-dashed),
and $\delta f_e$ (dash-dotted).  The thin black line denotes $(k_\perp
\rho_i)^{-5/3}$.  At the highest wavenumbers in the box, there is
still significant energy in the cascade for trans2, even for a run in
which the ion damping at $k_\perp \rho_i \sim 1$. }
\label{fig:trans2_3}
\end{figure}

Therefore, we need to implement some non-physical damping mechanism to
remove energy rapidly at the smallest scales in the box.  There appear
to be two avenues to accomplish this:
\begin{enumerate}
\item Hyperdiffusivity: Adding a hyperdiffusive term to the gyrokinetic equation.  
A successful choice for the hyperdiffusive operator will have two
properties: it must reduce, in the MHD limit, to some form
recognizable as hyperviscosity or hyperresistivity; and, heating by
this term must be positive definite.
\item Hypercollisionality: Adding a hypercollisional term where the coefficient
is wavenumber dependent.  
\end{enumerate}
These two possibities are explored in the remainder of this document.

%==============================================================================
\section{Hyperdiffusivity}
In this section, we look at the possibility of adding some
hyperdiffusive operator $\hyp$ to the gyrokinetic equation to achieve
a wavenumber dependent damping.

%-----------------------------------------------------------------------------
\subsection{Early Attempts at Hyperdiffusivity}
Early attempts at including hyperdiffusivity terms tried adding a term
of the form $\hyp =\nu_{Hs} \nabla_\perp^4$ to the gyrokinetic equation for
each species $s$.  I summarize here the attempts to use this approach:
\begin{enumerate}
\item When a hyperviscosity term of the form $\nu_H \nabla^4 h_s$
is added to the gyrokinetic equation, the resulting vorticity equation is the 
reduced MHD limit is given by 
\begin{equation}
\frac{\partial }{\partial t} \nabla_\perp^2 
\Phi - v_A \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
+  \left(1+ \frac{T_{0i}}{T_{0e}}\right) \frac{2 }{\rho_i^2}  
\nu_H \nabla_\perp^4  \Phi=0.
\end{equation}
This does not agree with the form we expect for hyperviscosity in
reduced MHD.
\item If, on the other hand, a hyperviscosity term of the form 
$\nu_H \nabla^4 g_s$ is used, the reduced MHD limit gives
\begin{equation}
\frac{\partial }{\partial t} \nabla_\perp^2 
\Phi - v_A \frac{\partial }{\partial z} \nabla_\perp^2 A_\parallel
+ \nu_H \nabla_\perp^4  \nabla_\perp^2 \Phi=0,
\label{eq:vort}
\end{equation}
precisely the form one desires.  Hence, it appears one needs to use $g_s$ in the 
hyperviscous term to correctly model hyperviscous damping.
\item The magnetic flux equation, in the reduced MHD limit, resulting from the
use of a hyperviscosity term of the form $\nu_H \nabla^4 g_s$, is
\begin{equation}
 \frac{\partial \Psi }{\partial t} -v_A \frac{\partial \Phi }{\partial z}
+ \frac{\partial }{\partial t} \left(\frac{ c^2}{\omega_{pe}^2}\nabla_\perp^2 
\Psi \right)
+  \nu_H \nabla^4 \left(\frac{ c^2}{\omega_{pe}^2} \nabla_\perp^2 \Psi \right)= 0.
\end{equation}
Hence, the hyperdiffusive term on $\Psi$ is of order
\begin{equation}
 k_\perp^2 \frac{ c^2}{\omega_{pe}^2} = \frac{k_\perp^2 \rho_i^2}{\beta_i} 
\frac{m_e}{m_i} \ll 1.
\end{equation}
Thus, the hyperviscous term gives only weak hyperresisitivity. 
\item To achieve a magnetic flux equation with hyperresistivity of the
form 
\begin{equation}
 \frac{\partial \Psi }{\partial t} -v_A \frac{\partial \Phi }{\partial z}
+  \eta_H \nabla^4 \Psi = 0,
\label{eq:flux}
\end{equation}
one should add the right-hand side of 
the gyrokinetic equation the term
\begin{equation}
-\frac{q_s}{T_{0s}} \eta_H \nabla^4  
\left\langle \frac{v_\parallel A_\parallel}{c} \right\rangle_{\V{R}_s} F_{0s}.
\end{equation}
\item Unfortunately, when the hyperviscous term of the form 
$\nu_H \nabla^4 g_s$ is used, the hyperviscous heating is expressed
in a non-positive definite form.
\item Finally, numerical tests with \T{GS2} show that when  $\nu_H \nabla^4 h_s$
is the hyperviscous term, the effective damping is of order
$(m_i/m_e)\nu_h k_\perp^4$, a mass ratio higher than expected. When
the term is $\nu_H \nabla^4 g_s$, the damping rate is consistent with
predictions.
\end{enumerate}

%-----------------------------------------------------------------------------
\subsection{More Recent Thoughts on Hyperdiffusivity}
In considering the above information, Bill noticed that if you write the 
gyrokinetic equation in this form,
\begin{equation}
\frac{\partial}{\partial t} \left(  h_s - \frac{q_s F_{0s} }{T_s} \langle \chi \rangle \right)
+v_\parallel \frac{\partial h_s}{\partial z} +
\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right] 
=\C(h_s),
\end{equation}
and then multiply by $T_s h_s/F_{0s}$ and integrate over
all space and velocity, the second and third terms on the left-hand side 
integrate to zero over all space, leaving
\begin{eqnarray}
\int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}h_{s} \frac{\partial}{\partial t}
\left(  h_s - \frac{q_s F_{0s} }{T_s} \langle \chi \rangle \right)
=  \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}\left\langle h_s
\C(h_s) \right\rangle_{\V{R}_s}  
\end{eqnarray}
The general form of this seems to be 
\begin{equation}
\frac{\partial}{\partial t} \E = \C_{\E}
\end{equation}
where $ \C_{\E}$ is the collisional heating rate.

This inspires us to try to add a hyperdiffusive term to the 
right-hand side of the gyrokinetic equation in this manner
\begin{equation}
\frac{\partial}{\partial t} \left(  h_s - \frac{q_s F_{0s} }{T_s} \langle \chi \rangle \right)
+v_\parallel \frac{\partial h_s}{\partial z} +
\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right] 
=\C(h_s)+ \hyp\left(  h_s - \frac{q_s F_{0s} }{T_s} \langle \chi \rangle \right).
\end{equation}

Doing this, the resulting entropy balance equation is given by
\begin{eqnarray}
&&\int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}h_{s} \frac{\partial}{\partial t}
\left(  h_s - \frac{q_s F_{0s} }{T_s} \langle \chi \rangle \right) \nonumber \\
&& =  \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}\left\langle h_s
\C(h_s) \right\rangle_{\V{R}_s}  
+\int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}} h_s
 \hyp\left(  h_s - \frac{q_s F_{0s} }{T_s} \langle \chi \rangle \right)
\end{eqnarray}

The limits of this operator in reduced MHD should give 
equations like \eqref{eq:vort} and \eqref{eq:flux}, but I have not 
gone through this in detail. 

%-----------------------------------------------------------------------------
\subsection{Positive-Definite Properties of this approach}
Here we investigate whether the hyperdiffusive heating term,
\begin{equation}
\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}
h_s\nu_H \nabla_\perp^4 \left(  h_s - \frac{q_s F_{0s} }{T_s} 
\langle \chi \rangle_{\V{R}_s} \right)
\end{equation}
is positive definite. We go through this examination term by term below

Integrating by parts twice shows that the first term is positive
definite for each species
\begin{equation}
\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}} h_s\nu_H \nabla_\perp^4  h_s  
= \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}\nu_H \left|\nabla_\perp^2  h_s \right|^2 \ge 0.
\end{equation}

Using the property of ring averages that, when integrated over all space, one can
interchange averages,
\begin{equation}
\int\frac{d^{3}{\bf R}_s}{V} f(\V{R}_s) \langle g(\V{r}) \rangle_{\V{R}_s} 
=\int\frac{d^{3}{\bf r}}{V} \langle  f(\V{R}_s)\rangle_{\V{r}}  g(\V{r}) ,
\end{equation}
we can write the second as
\begin{equation}
\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} q_{s} h_s\nu_H \nabla_\perp^4 
\langle \phi \rangle_{\V{R}_s}
= \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} q_{s} \langle h_s \rangle_{\V{r}} 
\nu_H \nabla_\perp^4 \phi
\end{equation}
To demonstrate positive-definiteness, we must now sum over species,
and use the quasineutrality condition to obtain
\begin{equation}
\int\frac{d^{3}{\bf r}}{V} \nu_H \nabla_\perp^4 \phi 
\left(\sum_s \int d^{3}{\bf v} q_{s} \langle h_s \rangle_{\V{r}} \right) =
\int\frac{d^{3}{\bf r}}{V} \nu_H \nabla_\perp^4 \phi 
\sum_s \frac{q_s^2 n_s}{T_s} \phi 
\end{equation}
Finally integration by parts in space twice gives the final result
\begin{equation}
\int\frac{d^{3}{\bf r}}{V} \nu_H \nabla_\perp^4 \phi 
\sum_s \frac{q_s^2 n_s}{T_s} \phi =\nu_H\sum_s \frac{q_s^2 n_s}{T_s}
\int\frac{d^{3}{\bf r}}{V} \left| \nabla_\perp^2 \phi \right|^2 \ge 0.
\end{equation} 

Similar manipulations, also requiring a sum over species but using the
parallel component of Ampere's Law, give for the third term
\begin{eqnarray}
&& \sum_s \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} q_{s} h_s\nu_H \nabla_\perp^4 
\left\langle \frac{v_\parallel A_\parallel}{c}\right\rangle_{\V{R}_s}
=\sum_s \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} q_{s} \langle h_s \rangle_{\V{r}}\nu_H \nabla_\perp^4 
\frac{v_\parallel A_\parallel}{c} \nonumber \\
&& =\int\frac{d^{3}{\bf r}}{V}\nu_H \nabla_\perp^4 
\frac{A_\parallel}{c} \left(\frac{-c}{4 pi} \nabla_\perp^2 A_\parallel \right)
= \frac{\nu_H}{4 \pi}\int\frac{d^{3}{\bf r}}{V} \left| \nabla_\perp^3 
A_\parallel \right|^2 \ge 0.
\end{eqnarray}

The fourth term requires use of the identity
\begin{equation}
\int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} q_{s} h_s\nu_H \nabla_\perp^4 
\left\langle \frac{\V{v}_\perp \cdot \V{A}_\perp}{c} \right\rangle_{\V{R}_s}
=\int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} q_{s} 
\langle \V{v}_\perp h_s \rangle_{\V{r}} \cdot \nu_H \nabla_\perp^4 
\frac{\V{A}_\perp}{c}.
\end{equation}
Summing over species once more, using the perpendicualr component of Ampere's law, 
and integrating by parts once, we find
\begin{eqnarray}
&& \int\frac{d^{3}{\bf r}}{V} \left(\sum_s \int d^{3}{\bf v} q_{s} 
\langle \V{v}_\perp h_s \rangle_{\V{r}}\right) \cdot \nu_H \nabla_\perp^4 
\frac{\V{A}_\perp}{c}
=\int\frac{d^{3}{\bf r}}{V} \left( \frac{-c}{4 \pi} \nabla^2_\perp \V{A}_\perp 
\right) \cdot \nu_H \nabla_\perp^4 \frac{\V{A}_\perp}{c} \nonumber \\
&&=\frac{\nu_H}{4 \pi}\int\frac{d^{3}{\bf r}}{V} \left| \nabla^3_\perp 
\V{A}_\perp \right|^2 \ge 0.
\end{eqnarray}

Collecting all of the terms, we obtain the final result
\begin{eqnarray}
\lefteqn{\sum_s \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}
h_s\nu_H \nabla_\perp^4 \left(  h_s - \frac{q_s F_{0s} }{T_s} 
\langle \chi \rangle_{\V{R}_s} \right)}&&\\
&=& \nu_H \sum_s \int\frac{d^{3}{\bf R}_s}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}\left|\nabla_\perp^2  h_s \right|^2
+ \frac{\nu_H}{4 \pi}\int\frac{d^{3}{\bf r}}{V}
\left[ \left( \sum_s \frac{4 \pi q_s^2 n_s}{T_s}\right)
\left| \nabla_\perp^2 \phi \right|^2 + \left| \nabla_\perp^3 
A_\parallel \right|^2 + \left| \nabla^3_\perp \V{A}_\perp \right|^2 
\right] \nonumber 
\end{eqnarray}
This form is positive definite, but only when summed over species.
Since the entropy balance should hold for each species individually,
entropy production from each species should be positive-definite.  But
we see that this operator only becomes positive definite after summing
over species; there may exist non-physical transfer of energy between
species (mediated by the field).  Hence, although this approach may
suffice, it may experience severe problems, particularly in
determining the heating for each species.

%==============================================================================
\section{Hypercollisionality}
Steve prefers using a hypercollisionality, basically using our present
collision operator but with a wavenumber dependent coefficient.  Using
the pitch-angle scattering component of the collision operator as an
example, the operator has the form
\begin{equation}
\C(f_e) = \nu_e(E) \frac{1}{2} \frac{\partial}{\partial \xi}
\left[ (1-\xi^2) \frac{\partial f_e}{\partial \xi} \right]
\end{equation}
where the coefficient has the form
\begin{equation}
\nu_e(E) = \nu_{ei} \left( \frac{v_{te}}{v}\right)^3 \left[ Z_{eff} + H_{ee}
\left(\frac{v}{v_{te}}\right) \right]
\end{equation}
and 
\begin{equation}
\nu_{ei} = \frac{ 4 \pi n_e e^4 \log \lambda}{(2 T_e)^{3/2} m_e^{1/2}}
\end{equation}

In this case, the gyrokinetic equation would become
\begin{equation}
\frac{\partial}{\partial t} \left(  h_s - \frac{q_s F_{0s} }{T_s} \langle \chi \rangle \right)
+v_\parallel \frac{\partial h_s}{\partial z} +
\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right] 
=\C(h_s) + \C_n(h_s),
\end{equation}
where the hypercollisional term $\C_n$ is given by
\begin{equation}
\C_n(f_e) = \nu_e(E) (k_\perp \rho_i)^n\frac{1}{2} \frac{\partial}{\partial \xi}
\left[ (1-\xi^2) \frac{\partial f_e}{\partial \xi} \right].
\end{equation}

This form has the advantage that the heating is certainly positive
definite. As well, the collisionality coefficient for each species can
differ, presumably providing the freedom of adjusting the effective 
magnetic Prandtl number of the simulation.





%==============================================================================
\section{Hyperdiffusive Formulation using $G_s$}
In this section, we investigate the use of a new generalized
hyperdiffusive formulation.
%-----------------------------------------------------------------------------
\subsection{Gyrokinetic Equation}
We begin with the gyrokinetic equation
\begin{equation}
\frac{\partial h_s}{\partial t} 
+v_\parallel \frac{\partial h_s}{\partial z} +
\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,h_s \right] -
\C(h_s) = \frac{q_s}{T_{0s}} \frac{\partial \langle
\chi \rangle_{\V{R}_s}}{\partial t} F_{0s}.
\end{equation}
Now, let us define a new distribution function $G_s$ defined by
\begin{equation}
G_s \equiv h_s - \frac{q_s F_{0s} }{T_s} \langle \chi \rangle
\end{equation}
Writing the gyrokinetic equation in terms of $G_s$ gives
\begin{equation}
\frac{\partial G_s}{\partial t} 
+v_\parallel \frac{\partial G_s}{\partial z} 
+v_\parallel  \frac{q_s F_{0s}}{T_s}
\frac{\partial \langle \chi \rangle}{\partial z} 
+\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,G_s \right] -
\C(h_s) = 0.
\end{equation}

We define a new linear hyperdiffusive operator,
\begin{equation}
\hyp = \nu_H \nabla^4
\end{equation}
and operate on $G_s$,
adding it to the gyrokinetic equation to give
\begin{equation}
\frac{\partial G_s}{\partial t} 
+v_\parallel \frac{\partial G_s}{\partial z} 
+v_\parallel  \frac{q_s F_{0s}}{T_s}
\frac{\partial \langle \chi \rangle}{\partial z} 
+\frac{c}{B_0} \left[ \langle \chi \rangle_{\V{R}_s} ,G_s \right] -
\left\langle \C(h_s)\right\rangle_{\V{R}_s} + \hyp(G_s) = 0.
\label{eq:gkG}
\end{equation}
%-----------------------------------------------------------------------------
\subsection{Deriving the Entropy-Balance Equation}
Now we multiply \eqref{eq:gkG} by $T_s G_s/F_{0s}$ and integrate over
all space and velocity.  Then second and fourth terms contribute
nothing upon integration over all space, leaving us with
\begin{eqnarray}
& & \frac{d}{dt}\int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{2F_{0s}}G^2_{s} 
+ \int\frac{d^{3}{\bf R}}{V}\int d^{3}{\bf v} 
v_\parallel G_s q_s
 \frac{\partial \left\langle \chi\right\rangle_{\V{R}_s} }{\partial z}
-  \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}\left\langle G_s \C(h_s) \right\rangle_{\V{R}_s}  \nonumber \\ 
& & +  \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}} G_s \hyp(G_s)  = 0  
\label{eq:t1}
\end{eqnarray}

To eliminate the problematic second term in this equation, we now
multiply \eqref{eq:gkG} by $q_s \left\langle
\chi\right\rangle_{\V{R}_s}$ and integrate over all space and
velocity.  The third and fourth terms integrate over space to zero, and we integrate
by parts in space on the second term to obtain
\begin{eqnarray}
& & \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
q_s \left\langle \chi\right\rangle_{\V{R}_s} \frac{\partial G_s }{\partial t}
-  \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\left\langle q_s \chi \C(h_s) \right\rangle_{\V{R}_s}  
\nonumber \\ 
& & +  \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}q_s \left\langle
\chi\right\rangle_{\V{R}_s}  \hyp(G_s)  = 0  
\label{eq:t2}
\end{eqnarray}

Adding \eqref{eq:t1} and \eqref{eq:t2} results in 
\begin{eqnarray}
& & \frac{d}{dt}\int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{2F_{0s}}G^2_{s} 
+ \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
q_s \left\langle \chi\right\rangle_{\V{R}_s} \frac{\partial G_s }{\partial t}
-  \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}}\left\langle \left( G_s+ \frac{q_sF_{0s}}{T_{s}}  \chi \right) 
\C(h_s) \right\rangle_{\V{R}_s}  \nonumber \\ 
& & +  \int\frac{d^{3}{\bf r}}{V}\int d^{3}{\bf v} 
\frac{T_{s}}{F_{0s}} \left( G_s+ \frac{q_sF_{0s}}{T_{s}}  
\left\langle \chi\right\rangle_{\V{R}_s} \right) \hyp(G_s)  = 0  
\end{eqnarray}

%==============================================================================
\section{A Few Additional Remarks on Hyperviscosity}
\begin{enumerate}
\item If the reduced MHD hyperviscosity limits are wrong, does this 
mean the hyperviscosity as usually
framed in reduced MHD is a bad model?
\item Does hyperdiffusivity in the $\nu_H \nabla^4 G_s$ have zero mean even if 
the $\nu_H \int h_s \nabla^4 \langle \chi \rangle$ piece goes less than zero?
\end{enumerate}
