%==============================================================================
% AstroGK  Fields Advance
%==============================================================================
\chapter{Fields Advance}
%==============================================================================

%%%%%%%%%%%%%%%     section  1     %%%%%%%%%%%%%%%
\section{Normalized field equations}

Here is the list of normalized field equations in terms of \T{gs2} variables:
\begin{align}
  Q_{\phi} \phi_{\Vec{k}} - Q_B \delta B_{\parallel\Vec{k}}
    = {\cal Q}_d (g_{s\Vec{k}}), \\
  k_{\perp}^2 A_{\parallel\Vec{k}} = {\cal P}_d(g_{s\Vec{k}}), \\
  R_{\phi} \phi_{\Vec{k}} + R_B \delta B_{\parallel\Vec{k}}
    = - {\cal R}_d (g_{s\Vec{k}}),
\end{align}
where
\begin{align}
  &Q_{\phi} = \sum_s \frac{q_s^2 n_{0s}}{T_{0s}} [1 - \Gamma_0(\alpha_s)],
    &Q_B = \sum_s q_s n_{0s} \Gamma_1(\alpha_s), \nonumber \\
  &{\cal Q}_d = \sum_s q_s n_{0s} \int {\cal J} J_0(a_s) \cdot \, d\lambda
      \, dE, \nonumber \\
  &{\cal P}_d = 2 \beta_0 \sum_s q_s n_{0s}
    \int {\cal J} v_{\parallel} \cdot \, d\lambda \ dE, \nonumber \\
  &R_{\phi} = \frac{\beta_0}{2} \sum_s q_s n_{0s} \Gamma_1(\alpha_s),
    &R_B = 1 + \frac{\beta_0}{2} \sum_s n_{0s} T_{0s}
      \Gamma_2(\alpha_s), \nonumber \\
  &{\cal R}_d = \sum_s n_{0s} T_{0s} \beta_0 \int {\cal J}
      \check{v}_{\perp}^2 \frac{J_1(a_s)}{a_s} \cdot \, d\lambda \, dE,
\end{align}
$\check{v}$ is under species dependent normalization and ${\cal J}$ is the Jacobian of the velocity integral:
\begin{equation}
  {\cal J} = \frac{1}{2\sqrt{\pi}} \sqrt{\frac{E}{1-\lambda}} e^{-E}.
\end{equation}
Notice that $\check{v}_{\perp}$ is normalized species-dependently while $v_{\parallel}$ uses general thermal velocity $v_{t0}$.

In \T{GS2}, the following variables are defined:
\begin{align}
  \T{gamtot} &= Q_{\phi}
    = \sum_s \frac{q_s^2 n_{0s}}{T_{0s}} [1 -\Gamma_0(\alpha_s)],\\
  \T{gamtot1} &= Q_B = \frac{2}{\beta_0} R_{\phi}
    = \sum_s q_s n_{0s} \Gamma_1(\alpha_s), \\
  \T{gamtot2} &= \frac{1}{\beta_0} (R_B - 1) 
    = \frac{1}{2} \sum_s n_{0s} T_{0s} \Gamma_2(\alpha_s), \\
  \T{antot} &= {\cal Q}_d(g_{s\Vec{k}}) = \sum_s q_s n_{0s} \int {\cal J}
    J_0(a_s) g_{s\Vec{k}} \, d\lambda \, dE, \\
  \T{antota} &= {\cal P}_d(g_{s\Vec{k}}) = 2 \beta_0 \sum_s q_s n_{0s} \int
    {\cal J} v_{\parallel} g_{s\Vec{k}} \, d\lambda \, dE, \\
  \T{antotp} &= \frac{1}{\beta_0} {\cal R}_d(g_{s\Vec{k}})
    = \sum_s n_{0s} T_{0s} \int {\cal J} \check{v}_{\perp}^2
    \frac{J_1(a_s)}{a_s} g_{s\Vec{k}} \, d\lambda \, dE.
\end{align}
%Thus field equations are written as
%\begin{align}
%  \T{gamtot} * \phi_{\Vec{k}} - \T{gamtot1} * \delta B_{\parallel\Vec{k}}
%    = \T{antot}, \\
%  k_{\perp}^2 A_{\parallel} = \T{antota}, \\
%  \frac{1}{2} \T{gamtot1} * \phi_{\Vec{k}} + \inp{ \frac{1}{\beta_0}
%    + \T{gamtot2} } \delta B_{\parallel\Vec{k}} = - \T{antotp}.
%\end{align}


%%%%%%%%%%%%%%%     section  2     %%%%%%%%%%%%%%%
\section{Discretization}

Let's discuss about the difference between $g_*$ and $g_{\rm inh}$ or $g_{\rm h}$.
We may introduce $g_*$ by
\begin{equation}
  g_*^{n+1} = g_{\rm inh}^{n+1} + g_{\rm h}^n.
\end{equation}
The discretization of gyrokinetic eqn is straightforward, and the equivalence of field equations is explained for quasi-neutrality condition in the electrostatic case for simplicity.
The discretization of the electrostatic quasi-neutrality condition at the future time step
\begin{align}
  Q_{\phi} \phi_i^{n+1} &= {\cal Q}_d(g_i^{n+1}) \nonumber \\
  &= {\cal Q}_d(g^{n+1}_{{\rm inh},i}) + {\cal Q}_d \left(
    \frac{\delta g_i}{\delta \phi_j} \right) (\phi_{*,j}^{n+1} + \phi_j^n)
    \nonumber \\
  &= {\cal Q}_d(g_{*,i}^{n+1}) + {\cal Q}_d \left(\frac{\delta g_i}
      {\delta \phi_j} \right) \phi_{*,j}^{n+1},
\end{align}
where we used $(\delta g / \delta \phi) \phi^n = g_{\rm h}^n$, is solved with respect to $\phi_*^{n+1}$ and yields
\begin{equation}
  \left[ - Q_{\phi} \delta_{ij} + {\cal Q}_d \left( \frac{\delta g_i}
      {\delta \phi_j} \right) \right] \phi_{*,j}^{n+1} = Q_{\phi} \phi_i^n
    - {\cal Q}_d(g_{*,i}^{n+1}).
\end{equation}
Since the form of the operator on both sides are exactly the same, the matrix on the left hand side can actually be obtained by computing the right hand side with $\delta_{ij}$ and $\delta g_i / \delta \phi_j$ for $\phi_i^n$ and $g_{*,i}^{n+1}$.

General form of the discretized field equations are summarized as follows.
\begin{align}
  \left[ -Q_{\phi} \delta_{ij} + {\cal Q}_d \left(\frac{\delta g_i}
      {\delta \phi_j} \right) \right] \phi_{*,j}^{n+1} + {\cal Q}_d
    \left( \frac{\delta g_i}{\delta A_j} \right) A_{*,j}^{n+1} %\nonumber \\
    + \left[ Q_B \delta_{ij} + {\cal Q}_d \left(\frac{\delta g_i}{\delta B_j}
      \right) \right] B_{*,j}^{n+1} %\nonumber \\
    = Q_{\phi} \phi_i^n - Q_B B_i^n - {\cal Q}_d(g_{*,i}^{n+1}), 
    \label{GS2: quasi-neutrality} \\
  {\cal P}_d \left(\frac{\delta g_i}{\delta \phi_j} \right) \phi_{*,j}^{n+1}
    + \left[ -k_{\perp}^2 \delta_{ij} + {\cal P}_d \left(\frac{\delta g_i}
      {\delta A_j} \right) \right] A_{*,j}^{n+1} + {\cal P}_d \left(
    \frac{\delta g_i}{\delta B_j}\right) B_{*,j}^{n+1} %\nonumber \\
    = k_{\perp}^2 A_i^n - {\cal P}_d(g_{*,i}^{n+1}),
    \label{GS2: parallel Ampere} \\
  \left[ R_{\phi} \delta_{ij} + {\cal R}_d \left(
    \frac{\delta g_i}{\delta \phi_j} \right) \right] \phi_{*,j}^{n+1}
    + {\cal R}_d \left( \frac{\delta g_i}{\delta A_j} \right) A_{*,j}^{n+1}
    + \left[ R_B \delta_{ij} + {\cal R}_d\left(\frac{\delta g_i}{\delta B_j}
      \right) \right] B_{*,j}^{n+1} %\nonumber \\
    = - R_{\phi} \phi_i^n - R_B B_i^n - {\cal R}_d(g_{*,i}^{n+1}).
    \label{GS2: perpendicular Ampere}
\end{align}
Since these field equations are coupled together, we build a big matrix containing the information of all $z$ components for all three field equations, where the each component of $z$ appears alternatively in the matrix.
Namely, the first component is for the first $z$ component of \eqn{GS2: quasi-neutrality}, the next is for that of \eqn{GS2: parallel Ampere}, the next is for that of \eqn{GS2: perpendicular Ampere}, and then we move along $z$ axis and start from \eqn{GS2: quasi-neutrality} again.
This is for the ease of obtaining inverted matrix.
Since the plasma response must be mostly concentrated around the same point where you put the source, this way makes the matrix to have large values around the diagonal components while they scatter when we separate equations and collect with $z$ components.


%%%%%%%%%%%%%%%     section  2     %%%%%%%%%%%%%%%
\section{Adiabatic option}

When we don't want to solve GK eqn for electrons, we may simply choose to not define electron species in the \T{species\_parameters} namelist.
In this case, we are allowed to choose \T{adiabatic\_option} in the namelist \T{dist\_fn\_knobs}.
There are four choices for the treatment of adiabatic electron dynamics.

\paragraph{\T{default}}
This choice changes the coefficient $Q_{\phi}$ as follows:
\begin{equation}
  Q_{\phi} = \frac{q_e^2 n_{0e}}{T_{0e}}
    + \sum_s \frac{q_s^2 n_{0s}}{T_{0s}} [1 - \Gamma_0(\alpha_s)],
\end{equation}
where the whole term $q_e^2 n_{0e} / T_{0e}$ should be put in the parameter \T{tite} in the namelist \T{parameters}.
Species sum may only represent ion species.

\paragraph{\T{iphi00=3}}
This choice applies the \T{default} option for $k_y \neq 0$ only.

\paragraph{\T{field-line-average-term}}
In addition to the \T{default} choice, it also changes the right hand side of the quasi-neutrality condition in a complicated manner.
I don't know the details.

\paragraph{\T{zero}}
This option cannot be chosen by the input file.

