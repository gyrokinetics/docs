%==============================================================================
% Limits of the Gyrokinetic Linear Dispersion Relation
%==============================================================================
\chapter{ Limits of the Gyrokinetic Linear Dispersion Relation}

%==============================================================================
\section{Analytical Limits of the Dispersion Relation} 
\label{sec:limits}
The complex eigenvalue solution $\overline{\omega}$ to \eqref{eq:disprel}
depends on three dimensionless parameters, the perpendicular scale
compared to the ion gyroradius $k_\perp \rho_i$, the ion plasma beta
$\beta_i$, and the ion to electron temperature ratio $T_i/T_e$.  The
linear, collisionless gyrokinetic dispersion relation can be
simplified and solved analytically in certain limits of these
parameters; the integrals of the Bessel functions over velocity space
$\Gamma_{0s}$, $\Gamma_{1s}$, and $\Gamma_{2s}$ and the plasma
dispersion function $Z(\xi_s)$ can be approximated by simple
analytical series for large and small arguments.  The arguments of the
integrals for ions and electrons are $\alpha_i = (k_\perp \rho_i)^2 /2$
and $\alpha_e = (m_e/m_i) (T_e/T_i) (k_\perp \rho_i)^2 /2$; for the
plasma dispersion function they are $\xi_i =
\overline{\omega}/\sqrt{\beta_i}$ and $\xi_e =
(m_e/m_i)^{1/2}(T_i/T_e)^{1/2}\overline{\omega}/\sqrt{\beta_i}$.
Natural parameters for expansion of these functions are $\alpha_i$ and
$\sqrt{\beta_i}$.  Below we explore the limits of large scale $\alpha_i
\ll 1$, weak magnetization $\sqrt{\beta_i} \gg 1$ and strong
magnetization $\sqrt{\beta_i} \ll 1$.

As we shall see in \secref{sec:large}, the gyrokinetic dispersion
relation separates into an \Alfven wave and a slow wave branch (the
fast wave is ordered out by the gyrokinetic approximation).  The slow
waves are damped in this limit; based on this fact, we expect a
nonlinear cascade of turbulent energy to smaller scales that reaches
the scale of the ion gyroradius to be comprised primarily of \Alfven
waves.  Hence, the limits of weak and strong magnetization around the
ion gyroradius scale, describe in \secsref{sec:weak}{sec:strong}, will
focus on the \Alfven wave solution to the dispersion relation.

%==============================================================================
\subsection{Large Scale Limit, $k_\perp^2 \rho_i^2 \ll 1$}
\label{sec:large}
In the large-scale limit of $k_\perp^2 \rho_i^2 \ll 1$, or $\alpha_i
\ll 1$, we can expand the functions $\Gamma_{0s}$, $\Gamma_{1s}$, and
$\Gamma_{2s}$ using an ascending series formula for the modified
Bessel functions $I_n$ \eqref{eq:ascendmodbess}. The resulting
approximations for species $s$ are $\Gamma_{0}(\alpha_s) \simeq 1 -
\alpha_s$, $\Gamma_{1}(\alpha_s) \simeq 1 - 3\alpha_s/2$, and
$\Gamma_{2}(\alpha_s) \simeq 2 - 3 \alpha_s$.  The dispersion relation
in this large scale limit simplifies to
\begin{equation}
\left[\frac{1}{\overline{\omega}^2} - 1 \right]
\left[  \frac{2}{\beta_i}- D +  \frac{C^2}{A} \right] = 0
\label{eq:disprel_all1}
\end{equation}
The first factor leads to the familiar \Alfven wave dispersion
relation for
\begin{equation}
\omega = k_\parallel v_A.
\label{eq:disprel_alf}
\end{equation}

The second factor represents the slow wave solution of the dispersion
relation.  This solution depends on the ion plasma beta $\beta_i$, so we can 
further simplify the slow wave solution for collisionless gyrokinetics
at large scale in limits of $\sqrt{\beta_i}$.

In the strongly magnetized limit, $\sqrt{\beta_i} \ll 1$, the
argument of the plasma dispersion function for the ion terms is large $\xi_i
\gg 1$; however, unless the temperature ratio $(T_i/T_e)^{1/2} \gg
\sqrt{\beta_i}(m_i/m_e)^{1/2}$, the argument of the electron terms
is not large.  Taking the limit $(m_e/m_i)^{1/2}(T_i/T_e)^{1/2} \le \sqrt{\beta_i}
\ll 1$, we drop the electron terms of the plasma dispersion function.
The plasma dispersion function can be expanded by \eqref{eq:plasdispg}
for $\xi_i \gg 1$ so that
\begin{equation}
\xi_i Z(\xi_i) \simeq i \sqrt{\pi} \xi_i \sigma e^{-\xi_i^2} - 1 + 
\frac{1}{2 \xi_i^2}
\end{equation}
Under this ordering, the first term in the slow wave portion of 
\eqref{eq:disprel_all1} is dominant leading to 
\begin{equation}
\frac{T_i}{T_e} - \frac{k_\parallel^2 v_{th_i}^2}{\omega^2} +
i \sqrt{\pi}  \frac{\omega}{k_\parallel v_{th_i}}
e^{-\frac{\omega^2}{k_\parallel^2 v_{th_i}^2}}=0
\end{equation}
Assuming weak damping, to be checked later, we can solve for the real
frequency and damping rate by expanding this equation about the 
the real frequency (Krall and Trivelpiece, Sec 8.6.2).  The dispersion 
relation $D(\omega)$ is expanded about $\omega=\omega_r$ assuming
the complex frequency $\omega=\omega_r+ i \gamma$
\begin{equation}
D(\omega)= D(\omega_r) + i\gamma \frac{\partial D(\omega_r)}{\partial \omega_r}
\end{equation}
Separating the dispersion relation into its real and imaginary parts
$D(\omega)= D_r(\omega) + i D_i(\omega)$ and substituting, we find
\begin{equation}
D(\omega)= D_r(\omega_r) + i D_i(\omega_r) + i\gamma 
\frac{\partial D_r(\omega_r)}{\partial \omega_r} - \gamma 
\frac{\partial D_i(\omega_r)}{\partial \omega_r}
\end{equation}
Setting the imaginary part to zero and solving for the damping rate yields
\begin{equation}
\gamma= - \frac{D_i(\omega_r)}
{\frac{\partial D_r(\omega_r)}{\partial \omega_r} }
\end{equation}
where $\omega_r$ solves the real part of the dispersion relation
$D_r(\omega_r)=0$.
The real part of the dispersion relation yields
\begin{equation}
\omega = k_\parallel c_s
\label{eq:disprel_sw_r}
\end{equation}
where $c_s^2=T_e/m_i$; this is the familiar 
ion acoustic wave.
Solving for the damping gives
\begin{equation}
\frac{\gamma}{\omega} = -\sqrt{\frac{\pi}{8}} \left( \frac{T_e}{T_i} \right)^{3/2}
e^{- \frac{1}{2}\frac{T_e}{T_i}}
\label{eq:disprel_sw_i}
\end{equation}
This agrees with the solution for ion acoustic waves in Krall and Trivelpiece
(Sec 8.6.3) in the limit $k^2 \lambda_{De}^2 \ll 1$.

%==============================================================================
\subsection{Weakly Magnetized Limit, $\sqrt{\beta_i} \gg 1$}
\label{sec:weak}
In the weakly magnetized limit, we simplify the gyrokinetic dispersion
relation for $\sqrt{\beta_i} \gg 1$ allowing for $\alpha_i \sim
1$. Here we focus on the \Alfven wave solution to the dispersion
relation since slow waves are heavily damped before reaching the scale
of the ion gyroradius.  For $T_i/T_e \gg (m_e/m_i) \alpha_i$,
$\alpha_e \ll 1$ and the electron contribution to the velocity
integrals of the Bessel functions can be simplified as in
\secref{sec:large}. For $\sqrt{\beta_i} \gg 1$, the plasma dispersion
function for both ions and electrons can be expanded in the limit of
small argument to give $\xi_s Z(\xi_s) \simeq i \sqrt{\pi} \xi_s$; in
the limit that $(T_i/T_e)^{1/2} \ll (m_i/m_e)^{1/2}$, the electron
terms are negligible compared to the ion terms and may be dropped

With these approximations, the terms of \eqref{eq:disprel} of order
$\Order(\beta_i^{-1}$ and higher can be dropped, resulting in the equation
\begin{equation}
\overline{\omega}^2 \left\{\Gamma_{2i}(1-\Gamma_{0i}) \left[ \frac{T_i}{T_e} 
- \Gamma_{0i} + 1-\Gamma_{1i}\right]- \Gamma_{0i} (1-\Gamma_{1i})^2
\right\} + i \overline{\omega} \sqrt{\frac{\beta_i}{\pi}}
(1+\frac{T_i}{T_e} )(1-\Gamma_{1i})^2 - \alpha_i \Gamma_{2i}(1+\frac{T_i}{T_e})
=0
\label{eq:weak1}
\end{equation}
Solving this quadratic equation yields the solution
\begin{equation}
\overline{\omega}=\frac{-i \sqrt{\frac{\beta_i}{\pi}}
(1+\frac{T_i}{T_e} )(1-\Gamma_{1i})^2 + \sqrt{ -\frac{\beta_i}{\pi}
(1+\frac{T_i}{T_e} )^2(1-\Gamma_{1i})^4
+ 4 \alpha_i \Gamma_{2i}(1+\frac{T_i}{T_e})G}}
{2 G}
\label{eq:weaksol}
\end{equation}
where 
\begin{equation}
G=\Gamma_{2i}(1-\Gamma_{0i}) \left[ \frac{T_i}{T_e} 
- \Gamma_{0i} + 1-\Gamma_{1i}\right]- \Gamma_{0i} (1-\Gamma_{1i})^2
\end{equation}




%==============================================================================
\subsection{Strongly Magnetized Limit, $\sqrt{\beta_i} \ll 1$}
\label{sec:strong}


%==============================================================================
\subsection{Limits of the Plasma Dispersion Function}
These limits can be found on page 30 of the NRL Plasma Formulary
\begin{enumerate}
\item A power series representation of the plasma dispersion relation for
a small argument $|\xi| \ll 1$ is
\begin{equation}
Z(\xi) = i \sqrt{\pi} e^{-\xi^2} - 2 \xi \left( 1- \frac{2 \xi^2}{3} 
+ \frac{4 \xi^4}{15} - \frac{8 \xi^6}{105} + \cdots \right)
\end{equation}
 
\item An asymptotic series representation of the plasma dispersion relation for
a large argument $|\xi| \gg 1$ is
\begin{equation}
Z(\xi) = i \sqrt{\pi} \sigma e^{-\xi^2} - \frac{1}{ \xi} \left( 1+ 
\frac{1}{2 \xi^2} + \frac{3}{4 \xi^4} +\frac{15}{8 \xi^6} + \cdots \right)
\label{eq:plasdispg}
\end{equation}
where 
\begin{equation}
\sigma = \left\{ \begin{array}{cc}
0 & y > |x|^{-1} \\
1 & |y| < |x|^{-1} \\
2 & y < |x|^{-1} \\
\end{array}
\right.
\end{equation}
 and $\xi= x+iy$.

\end{enumerate}

%==============================================================================
\section{Gamma Function Definitions}

Integrations over $v_\perp$ involve pairs of Bessel functions and can
be written as modified Bessel functions.  Three such integrals arise
in the calculation of the linear gyrokinetic dispersion relation; we
label them $\Gamma_0(\alpha)$, $\Gamma_1(\alpha)$, and
$\Gamma_2(\alpha)$.  These integrals are
\begin{eqnarray}
\Gamma_0 (\alpha)& =& \int_0^\infty \frac{2 v_\perp d v_\perp}{v_{th}^2} 
J_0^2\left(\frac{k_\perp v_\perp}{\Omega}\right) e^{- v_\perp^2/v_{th}^2}  
=  I_0(\alpha) e^{-\alpha}, \nonumber \\
\Gamma_1(\alpha) &=& \int_0^\infty \frac{2 v_\perp d v_\perp}{v_{th}^2} 
\frac{2 v_\perp^2}{v_{th}^2} 
\frac{ J_0\left(\frac{k_\perp v_\perp}{\Omega}\right) 
J_1\left(\frac{k_\perp v_\perp}{\Omega}\right) } 
{\frac{k_\perp v_\perp}{\Omega}}
e^{- v_\perp^2/v_{th}^2} = [I_0(\alpha)-I_1(\alpha)] e^{-\alpha}, \nonumber \\
\Gamma_2 (\alpha)&=& \int_0^\infty \frac{2 v_\perp d v_\perp}{v_{th}^2} 
\frac{4 v_\perp^4}{v_{th}^4} 
\left[\frac{ J_1\left(\frac{k_\perp v_\perp}{\Omega}\right)}
{\frac{k_\perp v_\perp}{\Omega}}\right]^2
e^{- v_\perp^2/v_{th}^2} =2 \Gamma_1(\alpha),
\label{eq:gam0def}
\end{eqnarray}
where $I_0$ and $I_1$ are the modified Bessel functions, 
the argument is $\alpha =\frac{ k_\perp^2 \rho^2}{2}$.

In the large-scale limit $k_\perp^2 \rho_i^2 \ll 1$, or $\alpha_i
\ll 1$, we can expand the functions $\Gamma_{0}(\alpha_s)$, 
$\Gamma_{1}(\alpha_s)$, and $\Gamma_{2}(\alpha_s)$ as follows:
$\Gamma_{0}(\alpha_s) \simeq 1 -
\alpha_s$, $\Gamma_{1}(\alpha_s) \simeq 1 - 3\alpha_s/2$, and
$\Gamma_{2}(\alpha_s) \simeq 2 - 3 \alpha_s$.

%==============================================================================
\section{High $\beta$ Expansion}

In the high beta limit, we make two asymptotic expansions: one for
$k_\perp \rho_i \sim \Order(\beta_i^{-1/4})$ and another for $k_\perp
\rho_i \sim \Order(1)$. For both of these expansions, the plasma
dispersion functions can be expanded for small arguments since
$\sqrt{\beta_i} \gg 1$ and we assume $(T_{0i}/T_{0e})^{1/2} \ll
(m_i/m_e)^{1/2}\sqrt{\beta_i}$.

%----------------------------------------------------------------------------
\subsection{$k_\perp \rho_i \sim \Order(\beta_i^{-1/4})$ Expansion}

For $k_\perp \rho_i \sim \Order(\beta_i^{-1/4})$, we will find that
$\overline{\omega} \sim \Order(1)$.  In this limit, we can use the
small argument limit of the perpendicular velocity integrals
\eqref{eq:gam0def} to find the order of the dispersion
relation coefficients $B \sim \Order(\beta_i^{-1/2})$ and $E \sim
\Order(\beta_i^{-1/2})$.  Keeping only terms of order
$\Order(\beta_i^{-1})$, the dispersion relation simplifies to
\begin{equation}
\alpha_i D( \overline{\omega}^2 - 1) = \frac{9}{4}\overline{\omega}^2 \alpha_i^2.
\end{equation}
We can write $D=i \sqrt{\pi} G_1
\frac{\overline{\omega}}{\sqrt{\beta_i}}$, where $G_1=2[
\Gamma_{1i} + (m_e/m_i)^{1/2}(T_{0e}/T_{0i})^{1/2}]$.  Solving for
$\overline{\omega}$ yields
\begin{equation}
\overline{\omega} = - i \frac{9\alpha_i  }{8 G_1} \sqrt{\frac{\beta_i}{\pi}} \pm
\sqrt{1-\frac{81 \beta_i \alpha_i^2}{64 \pi G_1^2}}.
\label{eq:highbeta_sol1}
\end{equation}

Let us consider the limits of this solution.  The first limit occurs when
\begin{equation}
\frac{81 \beta_i \alpha_i^2}{64 \pi G_1^2} \ll 1
\end{equation}
The $\alpha_i \ll 1$ limit gives $G_1 \simeq 2$ and substituting $\alpha_i 
=k_\perp^2 \rho_i^2 /2$ we find this limit is equivalent to
\begin{equation}
k_\perp \rho_i \ll \frac{8 \pi^{1/4}}{3 \beta_I^{1/4}}
\end{equation}
or more simply $k_\perp \rho_i \ll \beta_i^{-1/4}$.  In this limit,
we can expand the square root to obtain
\begin{equation}
\overline{\omega} = \pm 1 - i\frac{9\alpha_i }{8 G_1} \sqrt{\frac{\beta_i}{\pi}}
= \pm 1 - i\frac{9}{16} \frac{k_\perp^2 \rho_i^2 }{2}\sqrt{\frac{\beta_i}{\pi}}.
\end{equation}
This solution reproduces the large-wavelength limit of the \Alfven
wave with weak damping and verifies our assumption that
$\overline{\omega} \sim \Order(1)$.

The second limit is just the opposite, occurring for 
\begin{equation}
k_\perp \rho_i \gg \frac{8 \pi^{1/4}}{3 \beta_I^{1/4}}
\end{equation}
or more simply $k_\perp \rho_i \gg \beta_i^{-1/4}$.
In this limit, the solution is 
\begin{equation}
\overline{\omega} = \left\{
\begin{array}{c}
- i\frac{4 G_1}{9\alpha_i } \sqrt{\frac{\pi}{\beta_i}} \\
- i\frac{9\alpha_i }{4 G_1} \sqrt{\frac{\beta_i}{\pi}}
\end{array}
\right.
\end{equation}
Here, we find that the \Alfven waves solution become purely imaginary,
with a weakly (upper) and strongly (lower) damped solution.

%----------------------------------------------------------------------------
\subsection{$k_\perp \rho_i > 1$ Expansion}

In the limit $k_\perp \rho_i > 1$, we will find that
$\overline{\omega} \sim \Order(\beta_i^{-1/2})$.  Keeping only terms
of order $\Order(1)$, the dispersion relation reduces to
\begin{equation}
\overline{\omega}^2 \beta_i E^2 + \alpha_i \beta_i D - 2 \alpha_i=0.
\end{equation}
Solving for the frequency produces the result
\begin{equation}
\overline{\omega} = - i \frac{\alpha_i G_1  }{2 E^2} \sqrt{\frac{\pi}{\beta_i}} \pm
\sqrt{\frac{2\alpha_i}{ \beta_i E^2} -\frac{ \alpha_i^2 G_1^2 \pi}{4 E^4\beta_i}}.
\label{eq:highbeta_sol2}
\end{equation}
This solution confirms our assumption that $\overline{\omega} \sim
\Order(\beta_i^{-1/2})$.

The lower limit of this solution occurs for 
\begin{equation}
\frac{ \alpha_i^2 G_1^2 \pi}{4 E^4\beta_i} \gg \frac{2\alpha_i}{ \beta_i E^2},
\end{equation}
equivalent to 
\begin{equation}
k_\perp \rho_i  \ll \frac{2 \sqrt{\pi}}{3},
\end{equation}
or more simply $k_\perp \rho_i \ll 1$.
In this case, we find the solutions become
\begin{equation}
\overline{\omega} = \left\{
\begin{array}{l}
- i \frac{1}{\sqrt{\pi\beta_i}} \\
- i \frac{4 G_1}{9 \alpha_i} \sqrt{\frac{\pi}{\beta_i}}
\end{array}
\right.
\end{equation}
The lower solution matches the weakly damped (upper) root above in the
overlap region $ \beta_i^{-1/4} \ll k_\perp \rho_i \ll 1$. The upper
solution corresponds to the slow wave root.

The upper limit of this solution, which we expect to correspond to
kinetic \Alfven waves, is $k_\perp \rho_i  \ll \frac{2 \sqrt{\pi}}{3}$.
In this case, we find 
\begin{equation}
\overline{\omega} = \pm \sqrt{\frac{2 \alpha_i}{E^2 \beta_i}} 
- i\frac{\alpha_i G_1}{2 E^2} \sqrt{\frac{\pi}{\beta_i}}
\end{equation}
Assuming $\alpha_i \gg 1$ and $\alpha_e \ll 1$, we expand
\begin{equation}
G_1 = 2 \left[\frac{1}{\sqrt{\pi} (2 \alpha_i)^{3/2}} + 
\left( \frac{m_e}{m_i}\frac{T_{0e}}{T_{0i}} \right)^{1/2}  \right]
\end{equation}
and $E=-1$.
Substituting these values and simplifying finds the solution
\begin{equation}
\overline{\omega} = \pm \sqrt{\frac{k_\perp^2 \rho_i^2}{\beta_i}} 
- i\left(\frac{k_\perp^2 \rho_i^2}{2}\right)  \sqrt{\frac{\pi}{\beta_i}}
\left[\frac{1}{\sqrt{\pi} (k_\perp \rho_i)^3} + 
\left( \frac{m_e}{m_i}\frac{T_{0e}}{T_{0i}} \right)^{1/2}  \right]
\end{equation}
Hence, in this limit the solution becomes the $\beta_i \gg 1$ limit of 
kinetic \Alfven waves.