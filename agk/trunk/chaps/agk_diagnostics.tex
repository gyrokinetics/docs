%==============================================================================
% AstroGK Diagnostics
%==============================================================================
\chapter{Diagnostics}
%==============================================================================

%==============================================================================
\section{Polar Energy Spectrum Calculation}
The polar energy spectra in $k_\perp$ are calculated in the module
\T{gs2\_diagnostics} in the routines \T{init\_polar\_spectrum}, 
\T{get\_polar\_spectrum}, and \T{finish\_polar\_spectrum}.

The energies calculated are:
\begin{equation}
E_{B_\perp} = \int_\V{r} \frac{k_\perp^2 |A_{\parallel \V{k}}|^2}{8 \pi}
\end{equation}
\begin{equation}
E_{B_\parallel} = \int_\V{r} \frac{|\delta B_{\parallel \V{k}}|^2}{8 \pi}
\end{equation}
\begin{equation}
E_{\phi} = \int_\V{r} n_s T_s  \frac{|q_s \phi/T_s |^2}{2}
\end{equation}
\begin{equation}
E_{\delta f_s} = \int_{\V{R}_s} \int_\V{v} T_s F_{0s} \frac{|\delta f_s /F_{0s}|^2}{2}
\end{equation}
\begin{equation}
E_{h_s} = \int_{\V{R}_s} \int_\V{v} T_s F_{0s} \frac{|h_s /F_{0s}|^2}{2}
\end{equation}
where $\delta f_s = h_s - q_s \phi F_{0s}/T_s $.

NOTE: The equations above are not quite correct.  Really it is a sum
over $(k_x,k_y)$ modes, not an integration over space, that gives you
the energy.

%------------------------------------------------------------------------------
\subsection{Energy Spectrum Calculation in \agk}
\label{sec:gs2spec}
The perpendicular energy spectrum calculation in \agk is performed
by calculating raw and log-averaged spectra using an average of the
energies of each mode at a given $k_\perp$.

First, some notes on the indexing of perpendicular Fourier modes in
\T{GS2}.  In the input file, we specifiy \T{nx} and \T{ny}; these
values are used in the calculation for the number of $k_x$ modes
\begin{equation}
\T{ntheta0}= 2\mbox{int}\left(\frac{\T{nx}-1}{3}\right) + 1
\end{equation}
and the number of $k_y$ modes
\begin{equation}
\T{naky}= \mbox{int}\left(\frac{\T{ny}-1}{3}\right) + 1.
\end{equation}
Choosing \T{y0} specifies the minimum wavenumber in the box, $k_0=1/\T{y0}$.

As an example, for $ \T{nx}= \T{ny}=24$, we get the number of $k_x$
modes $\T{ntheta0}=15$ and the number of $k_y$ modes $\T{naky}= 8$.
The \T{akx}, \T{ikx}, \T{aky}, and \T{iky} arrays become
\begin{equation}
\begin{array}{ccl}
 \T{ikx} &  = & 0, 1, 2, 3, 4, 5, 6, 7, -7, -6, -5, -4, -3, -2, -1 \\
 \T{akx}& =& 0, k_0, 2k_0, 3k_0, 4k_0, 5k_0, 6k_0, 7k_0,-7k_0,-6k_0,-5k_0,-4k_0,-3k_0,-2k_0,-k_0\\
\T{iky} &  = & 0, 1, 2, 3, 4, 5, 6, 7 \\
 \T{aky}& =&  0, k_0, 2k_0, 3k_0, 4k_0, 5k_0, 6k_0, 7k_0
\end{array}
\end{equation}
%------------------------------------------------------------------------------
\subsubsection{Raw Spectrum}
The grid of $k_x$ and $k_y$ values gives a set of
$k_\perp=\sqrt{k_x^2+k_y^2}$ values, each with at least two modes at
that $k_\perp$. For each of the $k_\perp$ values in this set, the
energy of all the modes with that $k_\perp$ value is averaged. This
averaged value is then multiplied by $\pi \frac{k_\perp}{k_0}$ to give
the proper weighting that would occur by integrating over $\int_0^\pi
k_\perp d\theta$. The resulting spectrum is equivalent to the the
1-dimensional energy spectrum, but is typically very noisy. The output
is written to \T{runname.kspec\_raw}.

%------------------------------------------------------------------------------
\subsubsection{Averaged Spectrum}
The raw spectrum is then logarithmically averaged in both $k_\perp$
and energy in linear-spaced bins in $k_\perp$.  This is clearly the
correct approach if one hopes to find a power-law behaviour, which
produces a linear relation on a log-log plot.  

The number of bins for averaging (if \T{nkpolar} is not set in the
\T{kt\_grids} namelist) is calculated by
\begin{equation}
\T{nkpolar}= \mbox{int}\left( \sqrt{2}\mbox{real}(\T{naky}-1)\right).
\end{equation}
Effectively, this gives bins of width $k_0$. 

Within each bin, the averages of $\log(k_\perp)$ and $\log(E)$ are
taken.  The average values produce a much smoother spectrum, more
suitable for fitting to a power-law. The output is written to
\T{runname.kspec\_avg}.

NOTE: This log-averaged spectrum is currently broken.
%------------------------------------------------------------------------------
\subsubsection{Tests of Spectrum Calculation}
Here I present the spectrum calculated by four different methods: raw,
averaged, directly binned, and directly binned with correction. Each
of these methods is described below.

The raw spectrum calculation finds the energy at each possible $k_\perp$ arising 
from the set of $k_x$ and $k_y$ modes by
\begin{equation}
E_{raw} (k_\perp) = \pi \frac{k_\perp}{k_0} \frac{\sum_N E(k_x,k_y)}{N}
\end{equation}
where the sum is over the $N$ modes satisfying
$k_\perp=\sqrt{k_x^2+k_y^2}$.

The averaged spectrum begins with the raw spectrum values as
calculated above, averaging over bin of width $\Delta k_\perp$.  For
all $N_{bin}$ values of $E_raw(k_\perp) $ with $k_\perp$ falling
within the bin, the logarithmically averaged value is calculated by
\begin{equation}
E_{avg} (k_{\perp avg}) = \exp \left[ \frac{\sum_{N_{bin}} 
\log E_{raw}(k_\perp)}{N_{bin}} \right]
\end{equation}
where the value of $(k_{\perp avg}$ is calculated in a similar way
using
\begin{equation}
k_{\perp avg} = \exp \left( \frac{\sum_{N_{bin}} \log k_\perp}{N_{bin}} \right).
\end{equation}

The directly binned results simply sum the energy of all $(k_x,k_y)$ modes 
with $k_\perp$ falling within bins of width $\Delta k_\perp$,
\begin{equation}
E_{bin} (k_{\perp cen}) =  \sum_{N_{bin}} E (k_x,k_y) 
\end{equation}
where $k_{\perp cen}$ is taken at the bin center.

Finally a corrected, directly binned result is calculated from the
directly binned results above.  The correction is intended to
compensate for the missing modes in the corner of the box in
$k_x$-$k_y$ space with $k_\perp > \min(k_x,k_y)$.  The correction
calculates the area of ring corresponding to the bins of width $\Delta
k_\perp$ on the $k_x$-$k_y$ upper half-plane; this area is given by
\begin{equation}
A_{bin} = \frac{\pi}{2} \frac{ k_{\perp up}^2 - k_{\perp low}^2}{k_0^2}
\end{equation}
where the values $ k_{\perp up}$and $k_{\perp low}$ are the upper and lower 
$k_\perp$ values of each bin. The corrected energy is given by
\begin{equation}
E_{bincorr} (k_{\perp cen}) = E_{bin} \frac{A_{bin}}{N_{bin}}.
\end{equation}
This correction performs a similar average (although not logarithmic)
as the average spectrum described above, but beginning instead with
the directly binned results; it accounts for the decreasing number of
modes in the corner of the box because the possible modes at that
$k_\perp$ do not span the entire upper half-plane of $k_x$-$k_y$
space.

To compare these methods, we have initialized the energy at each
$(k_x,k_y)$ mode by the formula
\begin{equation}
E (k_x,k_y) = v_0^2 \left(\frac{k_0}{\sqrt{k_x^2+k_y^2}} \right)^{8/3} 
(1+f_{noise} w)
\end{equation}
where $f_{noise}$ gives the fractional noise and the random number $w
\in [-1,1]$. \figref{fig:spec_fn0} and \figref{fig:spec_fn1} compare 
the results of these methods for different levels of noise.

%================================================================================
\begin{figure}
\hbox to \hsize{ \hfill \epsfxsize12cm
\epsffile{figs/spec_fn0_0.ps} \hfill }
\caption{Comparison of the four methods for energy spectrum calculation with 
noise-free data.}
\label{fig:spec_fn0}
\end{figure}
%================================================================================
\begin{figure}
\hbox to \hsize{ \hfill \epsfxsize12cm
\epsffile{figs/spec_fn1_0.ps} \hfill }
\caption{Comparison of the four methods for energy spectrum calculation with a
noise level of $100$\%, or $f_{noise}=1.$}
\label{fig:spec_fn1}
\end{figure}



%------------------------------------------------------------------------------
\subsection{$N$-dimensional Energy Spectra}
The definition of the the $N$-dimensional energy spectrum $E^{(N)}$ is
not clearly defined in much of the literature on magnetized
turbulence.  Here I will try to sort out the story starting from the
single point of the Goldreich-Sridhar (GS) theory of strong
incompressible MHD turbulence.
%------------------------------------------------------------------------------
\subsubsection{Magnetohydrodynamic Goldreich-Sridhar Turbulence}

Here I review the heuristic argument behind the Goldreich-Sridhar (GS)
theory of strong incompressible MHD turbulence. We begin with
turbulence that is stirred isotropically at some wavenumber $k_0$ with
a velocity $v_0$ (there will be constraints on the magnitude of $v_0$ to 
ensure that we are in the regime of strong turbulence). Three basic conjectures
underlie the GS strong turbulence theory:
\begin{enumerate}
\item The frequency of nonlinear energy transfer to higher wavenumbers
in strong turbulence is quasi-two dimensional, governed by the eddy
turn-around frequency in the plane perpendicular to the mean magnetic
field
\begin{equation}
\omega_{NL} \sim k_\perp v_\perp.
\end{equation}
This effectively is an assumption that the interactions are local in
wavenumber space---that eddies on a given scale only interact with
eddies on nearby scales.
\item For strong turbulence, the cascade remains in \emph{critical balance}, 
where the nonlinear energy transfer frequency balances the linear
frequency of \Alfven waves
\begin{equation}
\omega = k_\parallel v_A \sim k_\perp v_\perp.
\end{equation}
\item Kolmogorov's hypothesis that the rate of energy transfer in the
inertial range is constant
\begin{equation}
\frac{\partial E}{\partial t} = \mbox{constant}.
\end{equation}
\end{enumerate}

Combining conjectures (1) and (3) allows us to derive the scaling of velocity
$v_\perp$ with wavenumber.  
\begin{equation}
\frac{\partial E}{\partial t} = \omega_{NL}v_\perp^2  = k_\perp v_\perp^3 
=  k_0 v_0^3 
\end{equation}
Solving for the velocity as a function of the scale $k$ gives
\begin{equation}
v_\perp= \left(\frac{k_0}{k_\perp}\right)^{1/3} v_0.
\end{equation}
Hence, the energy as a function of  scale $k_\perp$  is 
\begin{equation}
E= \left(\frac{k_0}{k_\perp}\right)^{2/3} v_0^2.
\end{equation}

Combining the critical balance conjecture (2) with the solution for
$v_\perp$ above allows us to relate the characteristic parallel
wavenumber of the turbulence $k_\parallel$ to the perpendicular
wavenumber $k_\perp$.
\begin{equation}
k_\parallel= k_\perp^{2/3} k_0^{1/3} \frac{v_0}{v_A}.
\end{equation}
If this constraint is precisely conserved, it means that turbulence
exists only on a the surface of a cone in 3-dimensional wavenumber
space defined by the equation above.  For any value of $k_\perp$, only
one value of $k_\parallel$ containing any energy; a $\delta$-function
can be used to incorporate this constraint into the full
$3$-dimensional energy spectrum. The turbulence is then, in fact,
entirely two-dimensional on the surface of this GS cone.  In practice,
this constraint is only as exact as this heuristic theory.

%------------------------------------------------------------------------------
\subsubsection{Constructing the $N$-dimensional Energy Spectra}
To determine the correct form of the $N$-dimensional energy spectra,
we will use the Goldreich-Sridhar result for the $1$-dimensional
energy spectrum as a starting point and normalize all spectra using
the total integrated energy.  To simplify the final form of the
integrated energy, we will integrate from $k_0$ to $\infty$ in
$k_\perp$; since the smallest scales contain the most energy, this will not 
change our determinations of the $N$-dimensional energy spectra.

We begin with the result that the $1$-dimensional energy spectrum
predicted by the GS theory has the form
\begin{equation}
E^{(1)}= \frac{k_0^{2/3}}{k_\perp^{5/3}} v_0^2.
\end{equation}
We'll see that this energy spectrum is the energy \emph{after}
integration over $k_\parallel$ and angle $\theta$.
We know that the total energy $E$ can be calculated from the 
 $1$-dimensional energy spectrum by
\begin{equation}
E = \int E^{(1)} d k_\perp.
\end{equation}
Therefore, we find that the total energy is
\begin{equation}
E = \int_{k_0}^\infty \frac{k_0^{2/3}}{k_\perp^{5/3}} v_0^2 d \V{k_\perp} 
= \frac{3}{2} v_0^2.
\end{equation}

Now, I define the  2-dimensional energy spectrum by
\begin{equation}
E = \int \int E^{(2)}k_\perp d\theta d k_\perp;
\end{equation}
this is the energy spectrum integrated only over all values of
$k_\parallel$.
To yield the result that the total energy $E=\frac{3}{2} v_0^2$, we discover
the  2-dimensional energy spectrum must be defined by
\begin{equation}
E^{(2)}= \frac{k_0^{2/3}}{2 \pi k_\perp^{8/3}} v_0^2.
\end{equation}

Similarly, we define the 3-dimensional energy spectrum by
\begin{equation}
E = \int \int \int E^{(3)} d k_\parallel k_\perp d\theta d k_\perp 
\end{equation}
To achieve a consistent result for the total energy, the 3-dimensional
energy spectrum must be defined by
\begin{equation}
E^{(3)}= \frac{k_0^{2/3}}{2 \pi k_\perp^{8/3}} v_0^2 \delta\left(k_\parallel - 
 k_\perp^{2/3} k_0^{1/3} \frac{v_0}{v_A}\right).
\end{equation}
Here the $\delta$-function enforces the critical balance constraint,
maintaining turbulence that is two-dimensional on the surface of the
GS cone. Note that this is not what actually occurs in magnetized
turbulence. Within the cone, at $k_\parallel v_A < k_\perp v_\perp$,
turbulence can exist; in this case, the timescale for nonlinear
transfer of energy to higher $k_\perp$ is faster than the linear
timescale, so the cascade progresses in a manner similar to
hydrodynamic turbulence as if there were no characteristic linear
frequency in the medium.  But, for the moment I will keep the
$\delta$-function because it simplifies this argument and I like it.

Summarizing the results, we find 
\begin{equation}
\begin{array}{lll}
\mbox{1-D} &  E^{(1)}= E(k_\perp) = \frac{k_0^{2/3}}{k_\perp^{5/3}} v_0^2 \\
\mbox{2-D} &  E^{(2)} =E(k_\perp,\theta)= \frac{k_0^{2/3}}{2 \pi k_\perp^{8/3}} v_0^2 \\
\mbox{3-D} &  E^{(3)} =E(k_\parallel,k_\perp,\theta)= 
\frac{k_0^{2/3}}{2 \pi k_\perp^{8/3}} v_0^2 \delta\left(k_\parallel - 
 k_\perp^{2/3} k_0^{1/3} \frac{v_0}{v_A}\right).
\end{array}
\end{equation}
%------------------------------------------------------------------------------
\subsection{Connection to \T{GS2} Diagnostics}
The described above in \secref{sec:gs2spec} produces the
angle-integrated energy spectrum---this is equivalent to the
1-dimensional energy spectrum.  Hence, the slopes of the output from
the \T{GS2} polar spectrum diagnostics should be directly comparable
to the 1-dimensional energy spectra predicted by the Golreich-Sridhar
theory.


%------------------------------------------------------------------------------
\subsection{\T{.kspec\_raw} and \T{.kspec\_avg}}
If \T{write\_ascii} is true (the default), then if
\T{write\_Epolar} is true, the polar spectrum vs. $k_\perp$ is output
to \T{runname.kspec\_raw} and \T{runname.kspec\_avg}.  The output
columns are:

\begin{center}
\begin{tabular}{c|c}
Column & Value \\ \hline
2 &$t$ \\
4 & $k_\perp$ \\
6 & $E_{tot}$ \\
8 & $E_{A_\parallel}$ \\
10 & $E_{B_\parallel}$ \\
12 & $E_{\phi_i^2}$ \\
14 & $E_{h_i^2}$ \\
16 & $E_{\delta f_i^2}$ \\
18 & $E_{\phi_e^2}$ \\
20 & $E_{h_e^2}$ \\
22 & $E_{\delta f_e^2}$
\end{tabular}
\end{center}
%==============================================================================
\newpage
\section{Discrete Fourier Mode Energy Calculation}
There is some subtlety involved in the calculation of the turbulent
energy and it's connection to analytical theory. In this section, I
will attempt to describe precisely the calculation of the energy and
make this connection to theory clear.

The energy density in gyrokinetic theory can be expressed as
\begin{equation}
E=\int \frac{d^3\V{r}}{V} \left[ \frac{|\delta\V{B}|^2}{8 \pi} + 
\int d^3\V{v} \sum_s \frac{T_{0s}}{2F_{0s}}  
\left(\langle h_s \rangle_{\V{r}} - \frac{q_s \phi}{T_s} F_{0s} \right)^2
\right]
\end{equation}
For the moment, we'll focus on the energy in the perpendicular
magnetic field perturbation $\delta B_\perp(\V{r},t)$.  Suppressing
the dependence on time for notational simplicity, the perpendicular
magnetic field perturbation in a periodic box of size $(L_x,L_y,L_z)$
can be expressed as the Fourier series
\begin{equation}
\delta B_\perp(\V{r}) = \sum_{i=- \infty}^{\infty} \sum_{j=- \infty}^{\infty} 
\delta B_{\perp ij}(z) e^{i(k_{xi} x + k_{yj} y)}
\end{equation}
where $k_{xi}=2 \pi i/L_x$ and $k_{yj}=2 \pi j/L_y$.  We may then express
\begin{equation}
|\delta B_\perp(\V{r})|^2 = \delta B_\perp(\V{r})\delta B_\perp^*(\V{r}) =
 \sum_{i=- \infty}^{\infty} \sum_{j=- \infty}^{\infty} 
 \sum_{i'=- \infty}^{\infty} \sum_{j'=- \infty}^{\infty} 
\delta B_{\perp ij}(z)\delta B_{\perp i'j'}^*(z) e^{i(k_{xi}-k_{xi'}) x }
e^{i(k_{yj}-k_{yj'}) y }
\end{equation}
We can thus write the energy density as
\begin{equation}
E_{B_\perp}= \frac{1}{8 \pi}\sum_{i=- \infty}^{\infty} \sum_{j=- \infty}^{\infty} 
 \sum_{i'=- \infty}^{\infty} \sum_{j'=- \infty}^{\infty} 
\int \frac{dz}{L_z} \delta B_{\perp ij}(z)\delta B_{\perp i'j'}^*(z)
\int \frac{dx}{L_x}  e^{i2 \pi (i-i') x/L_x }
\int \frac{dy}{L_y}  e^{i2 \pi (j-j') y/L_y }.
\end{equation}
Now, we make use of the identity
\begin{equation}
\frac{1}{L}\int^{L/2}_{L/2} dx  e^{i2 \pi (n-n') x/L } = \delta_{nn'}
\end{equation}
where $\delta_{nn'}$ is the Kronecker delta.  Thus, the energy simplifies to
\begin{equation}
E_{B_\perp}= \frac{1}{8 \pi}\sum_{i=- \infty}^{\infty} \sum_{j=- \infty}^{\infty} 
 \sum_{i'=- \infty}^{\infty} \sum_{j'=- \infty}^{\infty} 
\int \frac{dz}{L_z} \delta B_{\perp ij}(z)\delta B_{\perp i'j'}^*(z)
\delta_{ii'} \delta_{jj'} 
\end{equation}
and we obtain the final result
\begin{equation}
E_{B_\perp}= \sum_{i=- \infty}^{\infty} \sum_{j=- \infty}^{\infty} 
\int \frac{dz}{L_z} \frac{|\delta B_{\perp ij}(z)|^2}{8 \pi}.
\end{equation}

%==============================================================================
\section{Ascii output files}

Everything turned on by the flag $\gsvar{write\_ascii}$.

\paragraph{\gsvar{(runname).moments}}
Controlled by $\gsvar{write\_final\_moments}$ and each column means
\begin{center}\begin{tabular}{cccccccccc}
    $\theta$ & $k_y$ & $k_x$ & $\gsvar{ntot}$ & $\gsvar{dens}$ & $u_{\parallel}$ & $T_{\parallel}$ & $T_{\perp}$ & $\theta - \theta_0$ & $\gsvar{is}$
\end{tabular}\end{center}
middle 5 normalized by $\gsvar{phi0}$

\paragraph{\gsvar{(runname).mom2}}
Controlled by $\gsvar{write\_final\_moments}$ and each column means
\begin{center}\begin{tabular}{cccccccccc}
    $\theta$ & $k_y$ & $k_x$ & $\gsvar{ntot}$ & $\gsvar{dens}$ & $u_{\parallel}$ & $T_{\parallel}$ & $T_{\perp}$ & $\theta - \theta_0$ & $\gsvar{is}$
\end{tabular}\end{center}

\paragraph{\gsvar{(runname).fields}}
Controlled by $\gsvar{write\_final\_fields}$ and each column means
\begin{center}\begin{tabular}{ccccccccccc}
    $\theta$ & $k_y$ & $k_x$ & $\phi_{\rm r}$ & $\phi_{\rm i}$ & $A_{\parallel,{\rm r}}$ & $A_{\parallel,{\rm i}}$ & $A_{\perp,{\rm r}}$ & $A_{\perp,{\rm i}}$ & $\theta - \theta_0$ & $|\phi|$
\end{tabular}\end{center}
%==============================================================================
\section{\T{.fields}}
If \T{write\_ascii} is true (the default), then if
\T{write\_final\_fields} is true, the fields as functions the parallel
coordinate (\T{theta}, or \T{ig}) are output into the file
\T{runname.fields} at the end of the run.  The output columns are:

\begin{center}
\begin{tabular}{c|l}
Column & Value \\ \hline
1 &\T{theta(ig)} \\
2 & \T{aky\_out(ik)} \\
3 & \T{akx\_out(it)} \\
4 & $\Re$(\T{phi}) \\
5 & $\Im$(\T{phi}) \\
6 & $\Re$(\T{apar}) \\
7 & $\Im$(\T{apar}) \\
8 & $\Re$(\T{bpar}) \\
9 & $\Im$(\T{bpar}) \\
10 & \T{theta(ig)}$-$\T{theta0(it,ik)} \\
11 & $|\phi|$?
\end{tabular}
\end{center}

