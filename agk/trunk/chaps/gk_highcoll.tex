%==============================================================================
% Collisions in Gyrokinetics
%==============================================================================
\chapter{Highly Collisional Limit of Gyrokinetics}

%==============================================================================
\section{Strong Electron Collision Limit}
Consider the linearized electron gyrokinetic equation
\begin{equation}	
\frac{\partial h_{e}}{\partial t} 
+v_\parallel  \frac{\partial h_{e}}{\partial z} 
- \left\langle\C(h_e)\right\rangle_{\V{R}_e} =  \frac{q_eF_{0e}}{T_e} 
\frac{\partial \langle \chi \rangle_{\V{R}_e}}{\partial t}
\end{equation}
 where 
\begin{equation}
\langle \chi \rangle_{\V{R}_e}=\sum_{\V{k}} e^{i \V{k}\cdot \V{R}_e}
\left[J_0\left(\frac{k_\perp v_\perp}{\Omega_e}\right) 
\left(\hat{\phi}_\V{k} - \frac{v_\parallel \hat{A}_{\parallel\V{k}} }{c}\right) 
+ \frac{J_1\left(\frac{k_\perp v_\perp}{\Omega_e}\right)}
{\frac{k_\perp v_\perp}{\Omega_e}} \frac{2 v^2_\perp}{v_{te}^2}
\frac{T_e}{q_e}\frac{\delta \hat{B}_{\parallel \V{k}}}{B_0} \right]
\end{equation}
and
\begin{equation}
\langle \C(h_e) \rangle_{\V{R}_e}=\sum_{\V{k}} e^{i \V{k}\cdot \V{R}_e} 
\frac{\nu_{ee}(v)}{2} \left\{\frac{\partial}{\partial \xi} \left[ (1-\xi^2)  
\frac{\partial h_{\V{k}e}}{\partial \xi} \right]
-\frac{k_\perp^2 \rho_e^2}{2} \frac{v^2}{v_{te}^2}  (1+\xi^2) 
h_{\V{k}e} \right\}.
\end{equation} 
The pitch angle is defined by $\xi \equiv v_\parallel/v$ and the coefficient 
$\nu_{ee}$ is given by the formula
\begin{equation}
\nu_{ee}(v) = \frac{\sqrt{2} \pi n_e q_e^4 \ln \Lambda_{ee}}{m_e^{1/2}  T_e^{3/2}}
 \left(\frac{v_{te}}{v}\right)^3 \left[  \left( 1- \frac{1}{2}\frac{v_{te}^2}{v^2} \right) 
\frac{2}{\sqrt{\pi}} \int_0^{v/v_{te}}dx e^{-x^2} + \frac{1}{\sqrt{\pi}} 
\frac{v_{te}}{v}e^{-v^2/v_{te}^2} \right]
\end{equation}
Note that here we are considering only electron-electron collisions
and are not including electron-ion collisions.  As shorthand, we
denote $J_{ns}=J_n (\gamma_s)$ where $\gamma_s= \frac{k_\perp
v_\perp}{\Omega_s}$. Also, we take $\nu_e=\nu_{ee}(v)$ to be simply a
constant of our choice, independent of $v$.
The Fourier-transformed electron gyrokinetic equation then becomes
\begin{equation}	
\omega h_{\V{k}e}
- k_\parallel v_\parallel   h_{\V{k}e}
- \frac{i \nu_e}{2} \frac{\partial}{\partial \xi} \left[ (1-\xi^2)  
\frac{\partial h_{\V{k}e}}{\partial \xi} \right]
+ \frac{i \nu_e}{2} \frac{k_\perp^2 \rho_e^2}{2} \frac{v^2}{v_{te}^2}  (1+\xi^2) 
h_{\V{k}e} 
=  \omega \frac{q_eF_{0e}}{T_e} \left[J_{0e} \phi_\V{k} 
- J_{0e}\frac{v_\parallel A_{\parallel\V{k}} }{c} 
+ \frac{J_{1e}}{\gamma_e}\frac{2 v^2_\perp}{v_{te}^2}
\frac{T_e}{q_e}\frac{\delta B_{\parallel \V{k}}}{B_0} \right]
\end{equation}

We are interested in the limit that $k_\perp \rho_e \sim \epsilon$.
In this large-wavelength limit (compared to the electron Larmor radius
$\rho_e$), we can take the small argument limits of the Bessel functions,
\begin{eqnarray} 
&& \lim_{z \rightarrow 1} J_0(z)=1, 
\quad  \lim_{z \rightarrow 1} \frac{J_1(z)}{z}=\frac{1}{2}.
\nonumber 
\end{eqnarray}

The following subsidiary ordering, in addition to the usual
gyrokinetic ordering, is imposed:
\begin{equation}
\begin{array}{ccc}
k_\perp \rho_e & \sim & \epsilon \\
\frac{\omega}{k_\parallel v_{te}} & \sim & \epsilon^{1/2} \\
\frac{\nu_e}{\omega} & \sim & \epsilon^{-1} \\
\end{array}
\end{equation}
The balance of terms, as we shall see, occurs in this ordering for
\begin{equation}
\frac{\nu_e}{\omega} \sim \left(\frac{k_\parallel v_{te}}{\omega} \right)^2
\end{equation} 
The order of each term in the gyrokinetic equation thus becomes
\begin{equation}	
\underorder{1}{\omega h_{\V{k}e}}
- \underorder{\epsilon^{-1/2}}{k_\parallel v_\parallel   h_{\V{k}e}}
- \underorder{\epsilon^{-1}}{\frac{i \nu_e}{2} \frac{\partial}{\partial \xi} \left[ (1-\xi^2)  
\frac{\partial h_{\V{k}e}}{\partial \xi} \right]}
+ \underorder{\epsilon}{\frac{i \nu_e}{2} \frac{k_\perp^2 \rho_e^2}{2} \frac{v^2}{v_{te}^2}  (1+\xi^2) 
h_{\V{k}e} }
=  \omega \frac{q_eF_{0e}}{T_e} 
\underorder{1}{\left[ \phi_\V{k} \right.}
- \underorder{\epsilon^{-1/2}}{\frac{v_\parallel A_{\parallel\V{k}} }{c} }
+\underorder{1}{\left.\frac{ v^2_\perp}{v_{te}^2}
\frac{T_e}{q_e}\frac{\delta B_{\parallel \V{k}}}{B_0}\right]} 
\end{equation}
Expanding the electron gyrokinetic distribution function $h_{\V{k}e}$ in terms 
powers of $ \epsilon^{1/2}$,
\begin{equation}
h_{\V{k}e} = h_{\V{k}e}^{(0)} + \epsilon^{1/2}h_{\V{k}e}^{(1)} +
\epsilon h_{\V{k}e}^{(2)} + \cdots
\end{equation} 
we can solve for the distribution function order by order

%-------------------------------------------------------------------------------
\subsection{Order $\epsilon^{-1}$}
At this order, only the pitch-angle scattering term appears,
\begin{equation}	
 \frac{i \nu_e}{2} \frac{\partial}{\partial \xi} \left[ (1-\xi^2)  
\frac{\partial h_{\V{k}e}^{(0)}}{\partial \xi} \right]=0.
\end{equation}
Simple integration yields the solution 
\begin{equation}	
h_{\V{k}e}^{(0)} = \int d\xi \frac{A}{1-\xi^2},
\end{equation}
which is infinite unless the constant of integration $A=0$.  Thus,
at this order we discover that $h_{\V{k}e}^{(0)}$ has no pitch angle dependence,
\begin{equation}	
h_{\V{k}e}^{(0)} = h_{\V{k}e}^{(0)} (v)
\end{equation}
%-------------------------------------------------------------------------------
\subsection{Order $\epsilon^{-1/2}$}
At this order, we find
\begin{equation}	
 k_\parallel v_\parallel    h_{\V{k}e}^{(0)}
+ \frac{i \nu_e}{2} \frac{\partial}{\partial \xi} \left[ (1-\xi^2)  
\frac{\partial  h_{\V{k}e}^{(1)}}{\partial \xi} \right]
= \frac{q_eF_{0e}}{T_e}k_\parallel  v_\parallel  
\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} 
\end{equation}
We want to solve for $ h_{\V{k}e}^{(1)}$, so we obtain
\begin{equation}	
 \frac{\partial}{\partial \xi} \left[ (1-\xi^2)  
\frac{\partial  h_{\V{k}e}^{(1)}}{\partial \xi} \right]
= - \frac{i 2k_\parallel  v   }{\nu_e}\left( \frac{q_eF_{0e}}{T_e}
\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} -
h_{\V{k}e}^{(0)} \right)\xi
\end{equation}
Defining a variable
\begin{equation}	
\overline{A}=- \frac{i 2k_\parallel  v   }{\nu_e}\left[ \frac{q_eF_{0e}}{T_e}
\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} -
h_{\V{k}e}^{(0)} \right],
\end{equation}
we can solve for $ h_{\V{k}e}^{(1)}$ in two ways: by direct integration of the 
equation
\begin{equation}	
 \frac{\partial}{\partial \xi} \left[ (1-\xi^2)  
\frac{\partial  h_{\V{k}e}^{(1)}}{\partial \xi} \right]
= \overline{A} \xi
\end{equation}
or by noticing that the solutions of this equation are the Legendre
Polynomials. The solution is $ h_{\V{k}e}^{(1)} = - \overline{A} \xi /2$,
or
\begin{equation}	
 h_{\V{k}e}^{(1)} =\frac{i k_\parallel  v_\parallel   }{\nu_e}\left[ \frac{q_eF_{0e}}{T_e}
\left(\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} \right)-
h_{\V{k}e}^{(0)} \right].
\end{equation}
%-------------------------------------------------------------------------------
\subsection{Order $1$}
At this order, we find
\begin{equation}	
\omega h_{\V{k}e}^{(0)}
- k_\parallel v_\parallel   h_{\V{k}e}^{(1)}
- \frac{i \nu_e}{2} \frac{\partial}{\partial \xi} \left[ (1-\xi^2)  
\frac{\partial h_{\V{k}e}^{(2)}}{\partial \xi} \right]
=  \omega \frac{q_eF_{0e}}{T_e} \left[ \phi_\V{k} 
+ \frac{v^2_\perp}{v_{te}^2}
\left(\frac{T_e}{q_e}\frac{\delta B_{\parallel \V{k}}}{B_0}\right) \right]
\end{equation}
We integrate this equation over $\int_{-1}^{1} d \xi$ to annihilate
the $ h_{\V{k}e}^{(2)}$ term, leaving us, after substituting for $
h_{\V{k}e}^{(1)}$ from the Order $\epsilon^{-1/2}$ solution, with
\begin{equation}	
\omega h_{\V{k}e}^{(0)}
-\frac{i k_\parallel^2  v^2   }{3\nu_e}\left[ \frac{q_eF_{0e}}{T_e}
\left(\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} \right)-
h_{\V{k}e}^{(0)} \right]
=  \omega \frac{q_eF_{0e}}{T_e} \left[ \phi_\V{k} 
+ \frac{2}{3}\frac{v^2}{v_{te}^2}
\left(\frac{T_e}{q_e}\frac{\delta B_{\parallel \V{k}}}{B_0} \right)\right]
\end{equation}
Factoring, we find
\begin{equation}	
\omega \left\{ h_{\V{k}e}^{(0)} - \frac{q_eF_{0e}}{T_e} \left[ \phi_\V{k} 
+ \frac{2}{3}\frac{v^2}{v_{te}^2}
\left(\frac{T_e}{q_e}\frac{\delta B_{\parallel \V{k}}}{B_0} \right)\right]\right\}
-\frac{i k_\parallel^2  v^2   }{3\nu_e}\left[ \frac{q_eF_{0e}}{T_e}
\left(\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} \right)-
h_{\V{k}e}^{(0)} \right]= 0
\label{eq:hcdist_balance}
\end{equation}
Solving for $ h_{\V{k}e}^{(0)}$, we obtain the final result
\begin{equation}	
h_{\V{k}e}^{(0)}=F_{0e}
\left\{ \frac{1}{1+\frac{i k_\parallel^2 v^2}{3\nu_e \omega}}
\left[ \left(\frac{q_e\phi_\V{k}}{T_e}\right) +  \frac{2}{3}\frac{v^2}{v_{te}^2}
\left(\frac{\delta B_{\parallel \V{k}}}{B_0} \right) \right]
+ \frac{\frac{i k_\parallel^2 v^2}{3\nu_e \omega}}
{1+\frac{i k_\parallel^2 v^2}{3\nu_e \omega}} 
\left(\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} \right) \right\}
\end{equation}


%-------------------------------------------------------------------------------
\subsection{Summary}
We define a new set of dimensionless basis functions
\begin{equation}	
\tilde{E}_\parallel \equiv \frac{q_e}{T_e} \left(\phi_\V{k} -
\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} \right)
\label{eq:defepar}
\end{equation}
\begin{equation}	
\tilde{B}_\perp \equiv \frac{q_e}{T_e} 
\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} 
\label{eq:defbperp}
\end{equation}
\begin{equation}	
\tilde{B}_\parallel \equiv \frac{\delta B_{\parallel \V{k}}}{B_0}.
\label{eq:defbpar}
\end{equation}
We also express the solution in terms of normalized velocity 
\begin{equation}	
x \equiv \frac{v}{v_{te}}
\end{equation}
 and pitch angle
\begin{equation}	
\xi \equiv \frac{v_\parallel}{v}.
\end{equation}
We normalize the electron collisional coefficient using
\begin{equation}	
\hat{\nu}_e \equiv \frac{\nu_e }{k_\parallel v_{te}}.
\end{equation}
and define the dimensionless parameter $y$ which measures the 
strength of the collisionality
\begin{equation}	
y \equiv \frac{-i 3 \nu_e \omega }{k_\parallel^2 v_{te}^2}.
\end{equation}
The solution is given by
\begin{equation}	
h_{\V{k}e}= \left( 1- \frac{i \xi x}{\hat{\nu}_e}\right)h_{\V{k}e}^{(0)} (x)
+ \frac{i \xi x}{\hat{\nu}_e} F_{0e} \tilde{B}_\perp 
\label{eq:hc_dist}
\end{equation}
with 
\begin{equation}	
h_{\V{k}e}^{(0)} (x)= \frac{1}{1+\frac{x^2}{y}} F_{0e} 
\left( \tilde{E}_\parallel +  \frac{2}{3} x^2 \tilde{B}_\parallel \right)
+  F_{0e}\tilde{B}_\perp 
\label{eq:hc_dist0}
\end{equation}

%-------------------------------------------------------------------------------
\subsection{Simple Limit Comparing $\nu_e/\omega$ to $k_\parallel^2 v_{te}^2 /\omega^2$}
From \eqref{eq:hcdist_balance} we can discern two simple limits: semi-collisionality 
and high collisionality.

In the semi-collisional limit of $\nu_e/\omega \ll k_\parallel^2
v_{te}^2 /\omega^2$, the solution is simply
\begin{equation}	
h_{\V{k}e}^{(0)}  = \frac{q_eF_{0e}}{T_e}
\left(\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} \right).
\end{equation}
The perturbed distribution function is given by
\begin{equation}	
\delta f_e = -\frac{q_eF_{0e}}{T_e} \phi_\V{k} + h_{\V{k}e}^{(0)}+ h_{\V{k}e}^{(1)},
\end{equation}
so we find in this limit
\begin{equation}	
\delta f_e = -\frac{q_eF_{0e}}{T_e} \left[ \phi_\V{k} - \left(\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} \right) \right]
\end{equation}
Hence, using the definitions \eqref{eq:defepar}--\eqref{eq:defbpar}, this becomes
\begin{equation}	
\frac{\delta f_e}{F_{0e}} = -\tilde{E}_\parallel.
\end{equation}
The distribution function is driven by $E_\parallel$.

In the highly collisional limit of $\nu_e/\omega \gg k_\parallel^2
v_{te}^2 /\omega^2$, the solution becomes
\begin{equation}	
h_{\V{k}e}^{(0)}  =\frac{q_eF_{0e}}{T_e} \left[ \phi_\V{k} 
+ \frac{2}{3}\frac{v^2}{v_{te}^2}
\left(\frac{T_e}{q_e}\frac{\delta B_{\parallel \V{k}}}{B_0} \right)\right].
\end{equation}
Hence, the perturbed distribution function becomes
\begin{equation}	
\frac{\delta f_e}{F_{0e}} = -\frac{i k_\parallel  v_\parallel   }{\nu_e} 
\tilde{E}_\parallel + \left( 1 -\frac{i k_\parallel  v_\parallel   }{\nu_e} \right)
\frac{2}{3}\frac{v^2}{v_{te}^2} \tilde{B}_\parallel.
\end{equation}
The perturbed distribution function damps $E_\parallel$, effectively
providing a resistance to the parallel current.  Hence, we expect strong damping
in this limit of high collisionality.  

Note, however, that the necessary collision rate to be in the highly
collisional regime is very high. For kinetic \Alfven waves, the frequency is
given by
\begin{equation}	
\overline{\omega}^2 = \frac{2 \alpha_i}{\beta_i + \frac{2 \tau}{1+\tau}}
\end{equation}
where the bar denotes normalization to $k_\parallel v_A$; so we have
\begin{equation}	
\overline{\nu}_e \gg \frac{\mu \beta_i}{\overline{\omega} \tau} = 
 \frac{\mu \beta_i \sqrt{\beta_i + \frac{2 \tau}{1+\tau}}}
{\sqrt{2} \alpha_i^{1/2} \tau} 
\end{equation}
For $\beta_i \sim 1$ and $\tau \sim 1$ and $k_\perp \rho_i \sim 10$, this means
$\overline{\nu}_e \gg 260$, or $\overline{\nu}_e = \Order(m_i/m_e)$.

%==============================================================================
\newpage
\section{Define the Collisional Dispersion Function}
Here we define the Collisional Dispersion Function by
\begin{equation}	
\chi_n(y)=\int_0^\infty \frac{dx}{\sqrt{\pi}} \frac{x^n e^{-x^2}}{1+\frac{x^2}{y}}
\label{eq:coldispfunc}
\end{equation}
for a complex argument $y$.
This function has poles at 
\begin{equation}	
x=|y|^{1/2} \left(\mp \sin \frac{\theta}{2} \pm i \cos \frac{\theta}{2} \right),
\end{equation}
where the complex argument is 
given by
\begin{equation}	
y= y_r +i y_i = |y| e^{i\theta}
\end{equation}
and 
\begin{equation}	
\theta = \tan^{-1} \frac{y_i}{y_r}.
\end{equation}
These poles fall along the real axis, on the path of integration, at
$x=\pm |y|^{1/2}$ when $y_i=0$ and $y_r<0$ (or $\theta = \pm \pi$).
In the course of solving for the dispersion relation, we will need
$\chi_n$ for values $n=2,4,6$. Contour plots of the real and imaginary
components of $\chi_n(y)$ over complex $y$-space for values $n=2,4,6$
are presented in \figref{fig:chin}.
\begin{figure}
\hbox to \hsize{ \hfill \epsfxsize7cm \epsffile{figs/chi2r.ps} \hfill\epsfxsize7cm \epsffile{figs/chi2i.ps} \hfill}
\hbox to \hsize{ \hfill \epsfxsize7cm \epsffile{figs/chi4r.ps} \hfill\epsfxsize7cm \epsffile{figs/chi4i.ps} \hfill}
\hbox to \hsize{ \hfill \epsfxsize7cm \epsffile{figs/chi6r.ps} \hfill\epsfxsize7cm \epsffile{figs/chi6i.ps} \hfill}
\caption{Contour plots of the real and imaginary components of $\chi_n(y)$ over complex
$y$-space for values $n=2,4,6$. Positive values have red contours,
negative have blue contours, and zero is a black contour.}
\label{fig:chin}
\end{figure}

%-------------------------------------------------------------------------------
\subsection{Small Argument Limit of the Collisional Dispersion Function}
For $|y| \ll x$ or $|y| \ll 1$, 
\begin{equation}	
\chi_n(y) \simeq y \int_0^\infty \frac{dx}{\sqrt{\pi}} x^{n-2} e^{-x^2}.
\end{equation}
For values  $n=2,4,6$, this limit gives
\begin{equation}
\begin{array}{ccc}
\chi_2(y) &\simeq& \frac{y}{2}\\
\chi_4(y) &\simeq& \frac{y}{4}\\
\chi_6(y) &\simeq& \frac{3y}{8}
\end{array}.
\end{equation}

%-------------------------------------------------------------------------------
\subsection{Large Argument Limit of the Collisional Dispersion Function}
For $|y| \gg x$ or $|y| \gg 1$,
\begin{equation}	
\chi_n(y) \simeq \int_0^\infty \frac{dx}{\sqrt{\pi}}\left(
x^n - x^{n+2} \frac{y_r}{|y|^2} + i x^{n+2} \frac{y_i}{|y|^2} \right) e^{-x^2}.
\end{equation}
For values  $n=2,4,6$, this limit gives
\begin{equation}
\begin{array}{ccc}
\chi_2(y) &\simeq& \frac{1}{4} \left(1- \frac{3}{2y}\right)\\
\chi_4(y) &\simeq& \frac{3}{8} \left(1- \frac{5}{2y}\right)\\
\chi_6(y) &\simeq& \frac{15}{16} \left(1- \frac{7}{2y}\right)
\end{array}.
\end{equation}

%==============================================================================
\newpage
\section{Dispersion Relation for Strong Electron Collision Limit}
To determine the dispersion relation for the strong electron collision
limit of gyrokinetics, we must substitute the solution for the
distribution function given by \eqref{eq:hc_dist} and
\eqref{eq:hc_dist0} into Maxwells equations and determine the 
solubility condition. 

We will solve for the dispersion relation in the kinetic Alfven wave
limit given by the ordering
\begin{equation}
\begin{array}{ccc}
\alpha_e & \sim & \epsilon \\
\alpha_i & \sim & \epsilon^{-1} \\
\frac{\omega}{k_\parallel v_{te}} & \sim & \epsilon^{1/2} \\
\frac{\nu_e}{\omega} & \sim & \epsilon^{-1} \\
\end{array}.
\end{equation}
In this limit, 
\begin{eqnarray} 
&& J_{0i}=0, \quad J_{0e}=1,
\quad \frac{J_{1i}}{\gamma_i}=0, \quad \frac{J_{1e}}{\gamma_e}=\frac{1}{2}.
\nonumber 
\end{eqnarray}
In this limit, the ion response is strictly Boltzmann, so $ h_{\V{k}i}=0$.

Velocity integration is done over dimensionless variables $x= v/v_{te}$ and 
$\xi=v_\parallel /v$,
\begin{equation}	
\int d^3\V{v} = 2 \pi v_{te}^3 \int_0^\infty x^2 dx \int_{-1}^1 d\xi.
\end{equation}

%-------------------------------------------------------------------------------
\subsection{Poisson's Equation, or the Quasineutrality Condition}
The quasineutrality condition is given by 
\begin{equation}
\sum_s \frac{q_s^2 n_s}{T_{s}} \phi - \sum_s q_s \int d^3\V{v}
 \langle h_s \rangle_\V{r} = 0 .
\end{equation}
Fourier transforming and noting that $h_{\V{k}i}=0$, this becomes
\begin{equation}
\left(\frac{q_i^2 n_i}{T_{i}} +\frac{q_e^2 n_e}{T_{e}}\right)\phi_\V{k} - q_e 
\int d^3\V{v} J_{0e} h_{\V{k}e} = 0 .
\end{equation}
Noting that the terms proportional to $\xi$ in \eqref{eq:hc_dist} are odd in  $\xi$
and so contribute nothing, substituting $ J_{0e} =1$, and using $F_{0e} = \frac{n_e}{\pi^{3/2}v_{te}^3}
\exp(-v^2/v_{te}^2)$, the integral becomes
\begin{equation}
q_e  \int d^3\V{v} h_{\V{k}e} = \frac{2 \pi q_e v_{te}^3 n_e}{\pi v_{te}^3}
\int_0^\infty \frac{x^2 dx}{\sqrt{\pi}} \int_{-1}^1 d\xi 
\left[\frac{1}{1+\frac{x^2}{y}}
\left( \tilde{E}_\parallel +  \frac{2}{3} x^2 \tilde{B}_\parallel \right)
+ \tilde{B}_\perp \right] e^{-x^2}
\end{equation}
Noting $\int_{-1}^1 d\xi=2$ and simplifying, we end up with
\begin{equation}
= 4  q_e  n_e \left[\tilde{E}_\parallel  \int_0^\infty \frac{dx}{\sqrt{\pi}} 
\frac{x^2 e^{-x^2} }{1+\frac{x^2}{y}} 
+\frac{2}{3}  \tilde{B}_\parallel  \int_0^\infty \frac{dx}{\sqrt{\pi}} 
\frac{x^4 e^{-x^2} }{1+\frac{x^2}{y}} 
+ \tilde{B}_\perp \int_0^\infty \frac{dx}{\sqrt{\pi}} x^2  e^{-x^2}\right]
\end{equation}
Using the definition of the Collisional Dispersion Function $\chi_n$,
given by \eqref{eq:coldispfunc}, we find the quasineutrality condition
becomes
\begin{equation}
q_e  n_e \left( \frac{1}{\tau} +1 \right) 
\left(\tilde{E}_\parallel+\tilde{B}_\perp\right)-
q_e  n_e \left[ 4 \chi_2 \tilde{E}_\parallel
+\frac{8}{3}\chi_4  \tilde{B}_\parallel  
+ \tilde{B}_\perp \right]=0
\end{equation}
where we have used the shorthand for the temperature ratio $\tau=T_i/T_e$.
Simplifying and collecting terms, this results in
\begin{equation}
 \left( 1+ \frac{1}{\tau}  -  4 \chi_2  \right) \tilde{E}_\parallel
+ \frac{1}{\tau}\tilde{B}_\perp 
-\frac{8}{3}\chi_4  \tilde{B}_\parallel  =0
\end{equation}
%-------------------------------------------------------------------------------
\subsection{Parallel Ampere's Law}
The parallel component of Ampere's Law is given by 
\begin{equation}
-\nabla_\perp^2 A_\parallel= \sum_s \frac{ 4 \pi}{c} q_s
\int d^3 \V{v}  v_\parallel \langle h_s \rangle_\V{r} 
\end{equation}
Fourier transforming, noting that $h_{\V{k}i}=0$, and simplifying, this becomes
\begin{equation}
\frac{c k_\perp^2}{ 4 \pi} A_{\parallel \V{k}}= q_e
\int d^3 \V{v}  v_\parallel  J_{0e} h_{\V{k}e} 
\end{equation}
Noting that the terms even in $\xi$ in \eqref{eq:hc_dist} are odd in
$\xi$ in this integral and so contribute nothing, substituting $
J_{0e} =1$, and using $F_{0e} =\frac{n_e}{\pi^{3/2}v_{te}^3}
\exp(-v^2/v_{te}^2)$, the integral becomes
\begin{equation}
q_e \int d^3 \V{v}  v_\parallel  h_{\V{k}e} =
 \frac{2 \pi q_e v_{te}^4 n_e}{\pi v_{te}^3}
\int_0^\infty \frac{x^2 dx}{\sqrt{\pi}} \int_{-1}^1 d\xi 
\left[ \left(-i \frac{\xi^2 x^2}{\hat{\nu}_e} \right) h_{\V{k}e}^{(0)} (x)
+ i \frac{\xi^2 x^2}{\hat{\nu}_e}\tilde{B}_\perp \right]e^{-x^2}
\end{equation}
where the collision frequency has been normalized to the electron
thermal velocity $\hat{\nu}_e = \nu_e /(k_\parallel v_A)$.  Noting
that the $\tilde{B}_\perp$ terms cancel, using $\int_{-1}^1 d\xi \xi^2
=2/3$, and simplifying, the integral becomes
\begin{equation}
= \frac{4}{3} q_e n_e v_{te}\frac{i}{\hat{\nu}_e} \left[ 
- \tilde{E}_\parallel  \int_0^\infty \frac{dx}{\sqrt{\pi}} 
\frac{x^4 e^{-x^2} }{1+\frac{x^2}{y}} 
-\frac{2}{3}  \tilde{B}_\parallel \int_0^\infty \frac{dx}{\sqrt{\pi}} 
\frac{x^6 e^{-x^2} }{1+\frac{x^2}{y}} \right]
\end{equation}
Putting everything together, we find
\begin{equation}
 \frac{T_e}{q_e} \frac{c^2 k_\perp^2 k_\parallel }{ 4 \pi \omega } \frac{q_e}{T_e} 
\frac{\omega A_{\parallel \V{k}}}{k_\parallel c}=
 \frac{4}{3} q_e n_e v_{te}\frac{i}{\hat{\nu}_e} \left[ 
- \chi_4 \tilde{E}_\parallel 
-\frac{2}{3}  \chi_6  \tilde{B}_\parallel\right]
\end{equation}
Further simplification yields
\begin{equation}
-i \frac{T_e}{q_e} \frac{c^2 k_\perp^2 \nu_e}{4\pi q_e n_e v_{te}^2\omega} 
\tilde{B}_\perp =
- \frac{4}{3}  \chi_4 \tilde{E}_\parallel 
-\frac{8}{9}  \chi_6  \tilde{B}_\parallel
\end{equation}
The constants on the left-hand side simplify to
\begin{equation}
-i \frac{T_e}{q_e} \frac{c^2 k_\perp^2 \nu_e}{4\pi q_e n_e v_{te}^2\omega} 
= -i \frac{\nu_e}{\omega} \frac{\alpha_i}{\beta_i \mu}
\end{equation}
where the mass ratio is given by $\mu=m_i/m_e$ and $\alpha_i=
k_\perp^2 \rho_i^2 /2$.
Thus, 
our final result is
\begin{equation}
\frac{4}{3}  \chi_4 \tilde{E}_\parallel 
 -i \frac{\nu_e}{\omega} \frac{\alpha_i}{\mu \beta_i } \tilde{B}_\perp 
+\frac{8}{9}  \chi_6  \tilde{B}_\parallel=0.
\end{equation}
%-------------------------------------------------------------------------------
\subsection{Perpendicular Ampere's Law}
The perpendicular component of Ampere's Law is given by 
\begin{equation}
\nabla_\perp \delta B_\parallel = \sum_s \frac{ 4 \pi}{c} q_s
\int d^3 \V{v} \langle\zhat \times \V{v}_\perp h_s \rangle_\V{r}.
\end{equation}
Fourier transforming, noting that $h_{\V{k}i}=0$, and simplifying, this becomes
\begin{equation}
-\frac{c}{ 4 \pi} \delta B_{\parallel \V{k}}= \frac{q_e}{\Omega_e}
\int d^3 \V{v} \frac{J_{1e}}{\frac{k_\perp v_\perp}{\Omega_e}} 
v_\perp^2 h_{\V{k}e} 
\end{equation}
Noting that the terms proportional to $\xi$ in \eqref{eq:hc_dist} are
odd in $\xi$ and so contribute nothing, substituting
$\frac{J_{1e}}{\gamma_e}=\frac{1}{2}$, replacing $v_\perp^2 = x^2(1-\xi^2)$,
 and using $F_{0e} =
\frac{n_e}{\pi^{3/2}v_{te}^3} \exp(-v^2/v_{te}^2)$, the integral becomes
\begin{equation}
 \frac{q_e}{\Omega_e} \int d^3 \V{v} 
\frac{J_{1e}}{\frac{k_\perp v_\perp}{\Omega_e}}  v_\perp^2 h_{\V{k}e} 
= \frac{2 \pi q_e v_{te}^5 n_e}{2 \pi \Omega_e v_{te}^3}
\int_0^\infty \frac{x^2 dx}{\sqrt{\pi}} \int_{-1}^1 d\xi 
 x^2(1-\xi^2)\left[\frac{1}{1+\frac{x^2}{y}}
\left( \tilde{E}_\parallel +  \frac{2}{3} x^2 \tilde{B}_\parallel \right)
+ \tilde{B}_\perp \right] e^{-x^2}
\end{equation}
Using $\int_{-1}^1 d\xi(1- \xi^2) =4/3$, and simplifying, the integral
becomes
\begin{equation}
= \frac{4q_e n_e v_{te}^2 }{3 \Omega_e} \left[
\tilde{E}_\parallel  \int_0^\infty \frac{dx}{\sqrt{\pi}} 
\frac{x^4 e^{-x^2} }{1+\frac{x^2}{y}} 
+\frac{2}{3} \tilde{B}_\parallel \int_0^\infty \frac{dx}{\sqrt{\pi}}
\frac{x^6 e^{-x^2} }{1+\frac{x^2}{y}}
+ \tilde{B}_\perp  \int_0^\infty \frac{dx}{\sqrt{\pi}}  x^4  e^{-x^2} \right]
\end{equation}
Putting everything together, we find
\begin{equation}
-\frac{c \Omega_e B_0 }{ 4 \pi q_e n_e v_{te}^2}\tilde{B}_\parallel  
=  \frac{4}{3}\chi_4 \tilde{E}_\parallel 
+\frac{8}{9}\chi_6  \tilde{B}_\parallel
+ \frac{1}{2}\tilde{B}_\perp  
\end{equation}
The constants on the left-hand side simplify to
\begin{equation}
-\frac{c \Omega_e B_0 }{ 4 \pi q_e n_e v_{te}^2}
=  -\frac{\tau}{\beta_i},
\end{equation}
so we end up with
\begin{equation}
\frac{8}{3}\chi_4 \tilde{E}_\parallel  + \tilde{B}_\perp  
+\left(\frac{2\tau}{\beta_i} +\frac{16}{9}\chi_6 \right) \tilde{B}_\parallel =0
\end{equation}
%-------------------------------------------------------------------------------
\subsection{Solving for the Dispersion Relation}
Defining the quantities
\begin{equation}
A=1 + \frac{1}{\tau} - 4 \chi_2 
\end{equation}
\begin{equation}
B=\frac{4}{3} \chi_4
\end{equation}
\begin{equation}
C=\frac{8}{9} \chi_6,
\end{equation}
we obtain the matrix equation
\begin{equation}
\left( \begin{array}{ccc}
A & \frac{1}{\tau} & -2B \\
B &  -i \frac{\nu_e}{\omega} \frac{\alpha_i}{ \mu \beta_i} & C \\
2B &1 & \frac{2 \tau}{\beta_i} +2C
\end{array} \right) 
\left( \begin{array}{c}
\tilde{E}_\parallel \\
\tilde{B}_\perp  \\
\tilde{B}_\parallel
\end{array} \right)=0
\end{equation}
This yields the dispersion relation
\begin{equation}
\frac{i \nu_e}{\omega} \frac{2 \alpha_i}{ \mu \beta_i} \left( 
\frac{A \tau}{\beta_i} + 2B^2 + AC \right) + \frac{2B}{\beta_i} + 2B^2 + AC=0
\label{eq:hc_disp}
\end{equation}
%-------------------------------------------------------------------------------
\section{Limits of the Strong Electron Collision Dispersion Relation}
Analytical solutions of the dispersion relation can be derived in the 
limits $|y| \ll 1$ and $|y| \gg 1$.  Here we describe the assumed ordering 
in detail and derive the solutions for both of these limits.

%-------------------------------------------------------------------------------
\subsection{Semi-collisional Limit, $|y| \ll 1$}
For the semi-collisional limit, we assume the following ordering of parameters:
\begin{equation}
\begin{array}{ccc}
\mu & \sim & \epsilon^{-2} \\
\tau & \sim & 1 \\
\beta_i & \sim & 1 \\
\alpha_i & \sim & \epsilon^{-1} \\
\nu_e/\omega & \sim & \epsilon^{-1/2} 
\end{array}.
\end{equation}
In this semi-collisional limit, we expect to reproduce the basic
dynamics of the kinetic \Alfven wave, so we can use this formula to
estimate the ordering of the frequency.  The ordering imposed above
gives the following order for secondary parameters:
\begin{equation}
\begin{array}{ccc}
\overline{\omega} = \left( \frac{\alpha_i}{\beta_i/2 + 2\tau/(1+\tau)}\right)^{1/2} & \sim & \epsilon^{-1/2} \\
\frac{\omega}{k_\parallel v_{te}} = \frac{\overline{\omega} \tau^{1/2}}{\mu^{1/2} \beta_i^{1/2}}  & \sim & \epsilon^{1/2} \\
\alpha_e = \frac{\alpha_i}{\mu \tau}  & \sim & \epsilon \\
y= -3 \frac{i \nu_e}{\omega} \frac{\overline{\omega}^2 \tau}{\mu \beta_i}  & \sim & \epsilon^{1/2} 
\end{array}.
\end{equation}
In this limit, the coefficients in the dispersion relation simplify to 
\begin{equation}
\begin{array}{ccc}
A & \simeq & 1+ 1/\tau - 2y\\
B & \simeq & y/3 \\
C & \simeq & y/3
\end{array}.
\end{equation}
Hence, the order of the terms in the dispersion relation is given by
\begin{equation}
\frac{i \nu_e}{\omega} \frac{2 \alpha_i}{ \mu \beta_i} \left( 
\underorder{(\epsilon^{1/2},\epsilon)}{\frac{A \tau}{\beta_i}} 
+ \underorder{\epsilon^{3/2}}{2B^2} 
+ \underorder{(\epsilon,\epsilon^{3/2})}{AC} \right) 
+ \underorder{\epsilon^{1/2}}{\frac{2B}{\beta_i}}
+ \underorder{\epsilon}{2B^2} 
+ \underorder{(\epsilon^{1/2},\epsilon)}{AC}=0
\end{equation}

Keeping all terms to order $\epsilon$ except for the  order $\epsilon$ part of the 
first term and simplifying produces the result
\begin{equation}
\alpha_i - \overline{\omega}^2 \left( \frac{1}{A'} + \frac{\beta_i}{2}\right) + 
-\frac{i \nu_e}{\mu \omega}\left( \alpha_i \overline{\omega}^2 + 
\frac{2 \tau}{A'}\overline{\omega}^4\right) = 0.
\end{equation}
where we have used $A' = 1+ 1/\tau $.

Assuming $|\gamma| \ll |\omega_r|$, we can expand about $\omega=\omega_r$,
where $D_r(\omega_r)=0$, and solve for the damping rate using
\begin{equation}
\gamma= - \frac{D_i(\omega_r)}
{\frac{\partial D_r(\omega_r)}{\partial \omega_r} }.
\end{equation}
The resulting solution is 
\begin{equation}
\overline{\omega}_r^2 = \frac{2 \alpha_i}{\beta_i + \frac{2 \tau}{1+\tau}}
\end{equation}
\begin{equation}
\overline{\gamma}= -\frac{\overline{\nu}_e \alpha_i}{\mu\beta_i} \frac{\left[\frac{\tau^2 \beta_i}{1+\tau}
+ \frac{\beta_i }{2} \left(\frac{\tau}{1+\tau}+ \frac{\beta_i }{2}\right)\right]}
{\left(\frac{\tau}{1+\tau}+ \frac{\beta_i }{2}\right)^2}
\end{equation}
Comparison of this limit with the numerical solution to
\eqref{eq:hc_disp} is shown in \figref{fig:yll1}.

\begin{figure}
\hbox to \hsize{ \hfill \epsfxsize7cm \epsffile{figs/yll1_bi_w.ps} \hfill\epsfxsize7cm \epsffile{figs/yll1_bi_g.ps} \hfill}
\hbox to \hsize{ \hfill \epsfxsize7cm \epsffile{figs/yll1_tite_w.ps} \hfill\epsfxsize7cm \epsffile{figs/yll1_tite_g.ps} \hfill}
\caption{Semi-collisional, $|y| \ll 1$, limit of the highly collisional gyrokinetic dispersion relation.
Values of parameters not plotted are $\beta_i=1$, $T_i/T_e=1$, and
$k_\perp \rho_i =10$.}
\label{fig:yll1}
\end{figure}

%-------------------------------------------------------------------------------
\subsection{Highly Collisional Limit, $|y| \gg 1$}
For the highly collisional limit, we assume the following ordering of parameters:
\begin{equation}
\begin{array}{ccc}
\mu & \sim & \epsilon^{-2} \\
\tau & \sim & 1 \\
\beta_i & \sim & 1 \\
\alpha_i & \sim & \epsilon^{-1} \\
\nu_e/\omega & \sim & \epsilon^{-3/2} 
\end{array}.
\end{equation}
Using once again the kinetic \Alfven wave solution  to
estimate the ordering of the frequency, the secondary parameters
assume the following ordering:
\begin{equation}
\begin{array}{ccc}
\overline{\omega}=\left(\frac{\alpha_i}{\beta_i/2 + 2\tau/(1+\tau)}\right)^{1/2} 
& \sim & \epsilon^{-1/2} \\
\frac{\omega}{k_\parallel v_{te}} = \frac{\overline{\omega} \tau^{1/2}}
{\mu^{1/2} \beta_i^{1/2}}  & \sim & \epsilon^{1/2} \\
\alpha_e = \frac{\alpha_i}{\mu \tau}  & \sim & \epsilon \\
y= -3 \frac{i \nu_e}{\omega} \frac{\overline{\omega}^2 \tau}{\mu \beta_i}  
& \sim & \epsilon^{-1/2} 
\end{array}.
\end{equation}
In this limit, the coefficients in the dispersion relation simplify to 
\begin{equation}
\begin{array}{ccc}
A & \simeq & \frac{1}{\tau} + \frac{3}{2y}\\
B & \simeq &  \frac{1}{2} - \frac{5}{4y} \\
C & \simeq & \frac{5}{6} - \frac{35}{12y} 
\end{array}.
\end{equation}
Hence, the order of the terms in the dispersion relation is given by
\begin{equation}
\frac{i \nu_e}{\omega} \frac{2 \alpha_i}{ \mu \beta_i} \left( 
\underorder{(\epsilon^{-1/2},1)}{\frac{A \tau}{\beta_i}} 
+ \underorder{(\epsilon^{-1/2},1)}{2B^2} 
+ \underorder{(\epsilon^{-1/2},1)}{AC} \right) 
+ \underorder{(1,\epsilon^{1/2})}{\frac{2B}{\beta_i}}
+ \underorder{(1,\epsilon^{1/2})}{2B^2} 
+ \underorder{(1,\epsilon^{1/2})}{AC}=0
\end{equation}

Keeping  all terms to order $1$, we obtain
\begin{equation}
\frac{1}{2} \overline{\omega}^2 + \frac{i \overline{\nu}_e \alpha_i}{\mu\beta_i} 
\overline{\omega} - \frac{\alpha_i}{3 \tau} 
\frac{\left(\frac{3 \tau}{2\beta_i}-  \frac{5}{4}- \frac{35}{12\tau} \right)}
{\left(\frac{1}{\beta_i}+ \frac{1}{2} + \frac{5}{6\tau} \right)}=0
\end{equation}
The solution is
\begin{equation}
\overline{\omega}= -i\frac{\overline{\nu}_e \alpha_i}{\mu\beta_i}
\pm \left[ \frac{2\alpha_i}{3 \tau}
\frac{\left(\frac{3 \tau}{2\beta_i}-  \frac{5}{4}- \frac{35}{12\tau} \right)}
{\left(\frac{1}{\beta_i}+ \frac{1}{2} + \frac{5}{6\tau} \right)} -
\left(\frac{ \overline{\nu}_e \alpha_i}{\mu\beta_i} \right)^2 \right]^{1/2}
\end{equation}
Comparison of this limit with the numerical solution to
\eqref{eq:hc_disp} is shown in \figref{fig:ygg1}.

\begin{figure}
\hbox to \hsize{ \hfill \epsfxsize7cm \epsffile{figs/ygg1_nue_w.ps} \hfill\epsfxsize7cm \epsffile{figs/ygg1_nue_g.ps} \hfill}
\hbox to \hsize{ \hfill \epsfxsize7cm \epsffile{figs/ygg1_bi_w.ps} \hfill\epsfxsize7cm \epsffile{figs/ygg1_bi_g.ps} \hfill}
\hbox to \hsize{ \hfill \epsfxsize7cm \epsffile{figs/ygg1_tite_w.ps} \hfill\epsfxsize7cm \epsffile{figs/ygg1_tite_g.ps} \hfill}
\caption{Highly collisional, $|y| \gg 1$, limit of the highly collisional gyrokinetic dispersion relation.
Values of parameters not plotted are $\beta_i=1$, $T_i/T_e=1$, and
$k_\perp \rho_i =10$.}
\label{fig:ygg1}
\end{figure}


%==============================================================================
\newpage
\section{Strong Electron Collision Dispersion Relation for $\alpha_i \sim 1$}
The previous dispersion relation assumed the kinetic \Alfven wave
regime was appropriate, which requires $\alpha_i \gg 1$, severely
restricting the applicability of the result.  Here we combine the
standard gyrokinetic distribution function for ions with the strong
electron collisional distribution function for electrons to find a
dispersion relation valid for finite $\alpha_i$.

Note that although this dispersion relation is valid for a much
greater range of $k_\perp \rho_i$, it still assumes collisions
dominate the evolution of the electron distribution function, so it
can never recover the correct collisionless damping by electrons,
although it does recover the ion collisionless damping.

%-------------------------------------------------------------------------------
\subsection{Solving for the Dispersion Relation}
We define a slightly different set of dimensionless basis functions
(normalized to ion quantities)
\begin{equation}	
\tilde{E}_{\parallel i} \equiv \frac{q_i}{T_i} \left(\phi_\V{k} -
\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} \right)
\label{eq:defepari}
\end{equation}
\begin{equation}	
\tilde{B}_{\perp i} \equiv \frac{q_i}{T_i} 
\frac{ \omega A_{\parallel\V{k}} }{k_\parallel c} 
\label{eq:defbperpi}
\end{equation}
\begin{equation}	
\tilde{B}_\parallel \equiv \frac{\delta B_{\parallel \V{k}}}{B_0}.
\label{eq:defbpari}
\end{equation}
Defining the quantities
\begin{equation}
A= 1 + \Gamma_{0i} \xi_i Z(\xi_i)
\end{equation}
\begin{equation}
B= 1 - \Gamma_{0i}
\end{equation}
\begin{equation}
C=\Gamma_{1i} \xi_i Z(\xi_i)
\end{equation}
\begin{equation}
D= \Gamma_{2i} \xi_i Z(\xi_i)
\end{equation}
\begin{equation}
E=\Gamma_{1i}
\end{equation}
\begin{equation}
F=\tau - 4 \tau \chi_2 
\end{equation}
\begin{equation}
G=\frac{4}{3} \chi_4
\end{equation}
\begin{equation}
H=\frac{8}{9} \frac{\chi_6}{\tau}
\end{equation}
\begin{equation}
Q=\frac{i \omega}{\nu_e} \frac{\beta_i \mu}{\overline{\omega}^2}
\end{equation}
we obtain the matrix equation
\begin{equation}
\left( \begin{array}{ccc}
A+F & B & C+2G \\
A-B +QG  & \alpha_i/\overline{\omega}^2 & C+E-QH \\
 C+2G &1-E & D-\frac{2}{\beta_i} -2H
\end{array} \right) 
\left( \begin{array}{c}
\tilde{E}_{\parallel i} \\
\tilde{B}_{\perp i}  \\
\tilde{B}_\parallel
\end{array} \right)=0
\end{equation}
This yields the dispersion relation
\begin{eqnarray}
& & \left[ \frac{\alpha_i (A+F)}{\overline{\omega}^2} - B(A-B+QG) \right]
\left[\left(D -\frac{2}{\beta_i} - 2H \right)(A+F) - (C+2G)^2 \right] \nonumber \\
&& - \left[(A+F)(C+E-QH) - (A-B+QG)(C+2G)\right]
 \left[(A+F)(1-E)-B(C+2G)\right]=0
\end{eqnarray}
